#include <stdio.h>
#include <stdlib.h>

#include "../src/graph/graph_node.h"

#define NUM_NODES 9

void print_g_node(const graph_node* g_node)
{
  if (g_node->distance_from_previous)
  {
    printf("ID = %d; Idx in heap = %ld; Distance = %d\n",
      g_node->node_id,
      g_node->idx_in_heap,
      *g_node->distance_from_previous
    );
  }
  else
  {
    printf("ID = %d; Idx in heap = %ld; Distance = %s\n",
      g_node->node_id,
      g_node->idx_in_heap,
      "inf"
    );
  }
}

void print_mn_q_of_g_nodes(min_prior_queue* mn_q)
{
  for (unsigned int i = 0; i < mn_q->heap_size; ++i)
  {
    print_g_node(VOID_PTR_TO_G_NODE_PTR(get_by_index_from_min_heap(mn_q, i)));
  }
  putchar('\n');
}

int test_min_queue_of_graph_nodes()
{
  graph_node* nodes[NUM_NODES];

  nodes[0] = constrcut_graph_node(0);
  set_distance(nodes[0], 20);

  nodes[1] = constrcut_graph_node(1);
  set_distance(nodes[1], 14);

  nodes[2] = constrcut_graph_node(2);

  nodes[3] = constrcut_graph_node(3);
  set_distance(nodes[3], 8);

  nodes[4] = constrcut_graph_node(4);
  set_distance(nodes[4], 24);

  nodes[5] = constrcut_graph_node(5);
  set_distance(nodes[5], 15);

  nodes[6] = constrcut_graph_node(6);
  set_distance(nodes[6], 15);

  nodes[7] = constrcut_graph_node(7);
  set_distance(nodes[7], 11);

  nodes[8] = constrcut_graph_node(8);

  min_prior_queue* min_q =
    construct_min_heap(
        nodes,
        NUM_NODES,
        sizeof(graph_node*),
        graph_node_ptr_ptr_abstract_comparator,
        set_distance_for_heap,
        initialize_idx_in_heap
    );

  print_mn_q_of_g_nodes(min_q);

  unsigned int new_distance = 7;
  set_distance_in_prior_q(min_q, 6, &new_distance);
  new_distance = 16;
  set_distance_in_prior_q(min_q, 5, &new_distance);
  set_distance_in_prior_q(min_q, 3, NULL);

  print_mn_q_of_g_nodes(min_q);

  graph_node* min_node = constrcut_graph_node(0);
  while (!is_min_heap_empty(min_q))
  {
    extract_min(min_q, &min_node);

    print_g_node(min_node);
  }
  free_graph_node(min_node);
  printf("heap_size = %d; heap_capacity = %d\n",
    min_q->heap_size,
    min_q->capacity
  );

  free_min_heap(min_q);
  for(unsigned int i = 0; i < NUM_NODES; i++)
  {
    free(nodes[i]);
  }

  return 0;
}
