#include "../src/heap/min_heap.h"
#include <stdlib.h>
#include <stdio.h>

int char_comparator(void* left, void* right)
{
  return *((char*)left) - *((char*)right);
}

int set_int_for_heap(void* int_ptr, void* other_int_ptr)
{
  *(int*)int_ptr = *(int*)other_int_ptr;

  return 0;
}

int test_min_heap()
{
  char arr[] = {10, 9, 8, 7, 6, 8};
  // char arr[] = {7, 8, 8, 9, 6, 10};

  min_heap* mn_heap =
    construct_min_heap(
      arr,
      6,
      sizeof(char),
      char_comparator,
      set_int_for_heap,
      NULL
    );

  for (unsigned int i = 0; i < mn_heap->heap_size; ++i)
  {
    printf("%d ", *((char*) get_by_index_from_min_heap(mn_heap, i)));
  }
  printf("\n");

  free(mn_heap->array);
  free(mn_heap);
  return 0;
}
