#include "../src/graph/input/input_adj_matrix_for_finding_path.h"

void print_path_finding_input(path_finding_input* pfi)
{
  printf(
    "Num vertices = %d, source = %d, destination = %d\n",
    pfi->a_matrix->size,
    pfi->source_node_id,
    pfi->destination_node_id
  );

  for (unsigned int i = 0; i < pfi->a_matrix->size; ++i)
  {
    for (unsigned int j = 0; j < pfi->a_matrix->size; ++j)
    {
      unsigned int* cur_value = pfi->a_matrix->matrix[i][j];
      if (cur_value)
      {
        printf("%d\t", *cur_value);
      }
      else
      {
        printf("*\t");
      }
    }

    putchar('\n');
  }
}
