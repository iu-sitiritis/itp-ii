#include "graph/input/input_adj_matrix_for_finding_path.h"
#include "graph/graph_algorithms.h"
#include "graph/output/output_all_shortest_paths.h"

void find_shortest_paths(
  FILE* input_file_ptr,
  FILE* output_file_ptr
)
{
  path_finding_input* input_pf = construct_path_finding_input();
  input_file_read_status read_status = read_from_file(input_file_ptr, input_pf);

  switch (read_status)
  {
    case OK:
    {
      shortest_paths_graph* spg =
        find_shortest_paths_from_single_source(
          input_pf->a_matrix,
          input_pf->source_node_id
        );

      print_all_shortest_paths_from_node_to_file(
          output_file_ptr,
          spg,
          input_pf->destination_node_id
      );

      free_shortest_paths_graph((shortest_paths_graph*)spg);
      break;
    }

    default:
    {
      print_error(output_file_ptr, read_status);
    }
  }

  // free resources
  free_path_finding_input(input_pf);
}

int main()
{
  FILE* input_file_ptr = fopen("./input.txt", "r");
  FILE* output_file_ptr = fopen("./TymurLysenkoOutput.txt", "w");

  find_shortest_paths(input_file_ptr, output_file_ptr);

  // close files
  fclose(input_file_ptr);
  fclose(output_file_ptr);

  return 0;
}
