#ifndef MIN_PRIORITY_QUEUE
#define MIN_PRIORITY_QUEUE

#include "../heap/min_heap.h"

typedef min_heap min_prior_queue;

// extracts minimal value from a min_prior_queue into min_value
// returns # of elements left in the queue
// 0 means no elements left, hence nothing to extract
// the queue is empty - does not change the value of min_value
unsigned int extract_min(min_prior_queue* q, void* min_value);

// Description:
// updates a value of an element stored in the queue at index idx
// by setting it to new_value and keeps the heap property
// Return value:
// returns the new index of the updated object
// Errors:
// if provided idx is incorrect - returns -1
// if new_key_value is an invalid - returns -2
long update_key(min_prior_queue* q, unsigned int idx, void* new_value);

// Description:
// Checks if new_value is a valid value for object stored in q at index idx.
// For min_prior_queue value is valid <=> value <= q[idx]
// For max_prior_queue value is valid <=> value >= q[idx]
// Return value:
// 1 <=> new_key is valid
// 0 <=> new_key is not valid
static inline int is_new_value_valid(
  min_prior_queue* q,
  unsigned int idx,
  void* new_value
)
{
  return q->comparator(new_value, get_by_index_from_min_heap(q, idx)) <= 0;
}

#endif // MIN_PRIORITY_QUEUE
