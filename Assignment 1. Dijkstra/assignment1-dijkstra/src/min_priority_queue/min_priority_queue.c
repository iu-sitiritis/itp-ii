#include <memory.h>

#include "min_priority_queue.h"

unsigned int extract_min(min_prior_queue* q, void* min_value)
{
  if (!is_min_heap_empty(q))
  {
    memcpy(min_value, q->array, q->type_size);
    memcpy(q->array,
      get_by_index_from_min_heap(q, q->heap_size - 1),
      q->type_size
    );
    --(q->heap_size);

    if(q->initialize_idx)
    {
      q->initialize_idx(min_value, -1);

      if (q->heap_size)
      // if heap has at least 1 entry in it
      {
        q->initialize_idx(q->array, 0);
      }
    }

    min_heapify(q, 0);
  }

  return q->heap_size;
}

long update_key(min_prior_queue* q, unsigned int idx, void* new_value)
{
  // validate idx
  if (!is_index_valid(q, idx))
  {
    return -1;
  }
  else if (!is_new_value_valid(q, idx, &new_value))
  // validate new_key_value
  {
    return -2;
  }

  q->set_value(get_by_index_from_min_heap(q, idx), &new_value);

  // while idx have not reached a root (i. e. idx != 0)
  // compare the q[idx] with its parent,
  // and if it is < its parent - swap them and change idx to parent_idx(idx)
  while (
    idx &&
    (
      q->comparator(
        get_by_index_from_min_heap(q, idx),
        get_by_index_from_min_heap(q, parent_idx(idx))
      ) < 0
    )
  )
  {
    swap_entries_min_heap(q, idx, parent_idx(idx));
    idx = parent_idx(idx);
  }

  return idx;
}
