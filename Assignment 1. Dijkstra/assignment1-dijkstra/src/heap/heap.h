#ifndef HEAP
#define HEAP

// get parent's index in a heap
static inline unsigned int parent_idx(unsigned int idx)
{
  return (idx - 1) / 2;
}

// get left's index in a heap
static inline unsigned int left_idx(unsigned int idx)
{
  return (2 * idx) + 1;
}

// get right's index in a heap
static inline unsigned int right_idx(unsigned int idx)
{
  return (2 * idx) + 2;
}

#endif
