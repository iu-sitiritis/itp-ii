#ifndef MIN_HEAP
#define MIN_HEAP

#include "heap.h"

typedef
  struct min_heap
  {
    void* array;

    unsigned int type_size;

    unsigned int capacity;
    unsigned int heap_size;

    // Description:
    // function used to compare 2 objects stored in the heap
    // negative int <=> left < right
    // Return value:
    // 0 <=> left == right
    // positive int <=> left > right
    // can be reversed, so the heap becomes a max_heap
    int (*comparator)(void* left, void* right);
    // Description:
    // Function used to change the value of a stored object obj to new_value
    // Return value:
    // 0 <=> no errors assigning new_value
    // !0 <=> an error occurred
    int (*set_value)(void* obj, void* new_value);
    // Description:
    // Function used to initialize obj with its index idx in the heap
    // -1 - obj is not in the heap
    void (*initialize_idx)(void* obj, long idx);
  }
min_heap;

// Description:
// construct a new min_heap using:
// an input array
// array element's size
// a comparator for the type
// Return value:
// returns a pointer to the newly created min_heap
min_heap* construct_min_heap(
  const void* array,
  unsigned int array_size,
  unsigned int type_size,
  int (*comparator)(void* left, void* right),
  int (*set_value)(void* qd_obj, void* new_key_value),
  void (*initialize_idx)(void* obj, long idx)
);

// free a min_heap
void free_min_heap(min_heap* in_heap);

// keep the property of a min_heap starting at index idx, namely:
// each parent is <= to its left & right
void min_heapify(min_heap* in_heap, unsigned int idx);

// swap entries in a min_heap at indexes first_idx & last_idx
void swap_entries_min_heap(
  min_heap* in_heap,
  unsigned int first_idx,
  unsigned int last_idx
);

// !0 <=> heap is empty
// 0 <=> not empty
static inline unsigned int is_min_heap_empty(const min_heap* in_heap)
{
  return in_heap->heap_size == 0;
}

// 1 <=> idx is valid for in_heap
// 0 <=> idx is not valid for in_heap
static inline int is_index_valid(const min_heap* in_heap, long idx)
{
  return idx < in_heap->heap_size && idx >= 0;
}

// return element from min_heap at index idx
static inline void* get_by_index_from_min_heap(
  const min_heap* in_heap,
  unsigned int idx
)
{
  return (void *)(((char *)(in_heap->array)) + (in_heap->type_size * idx));
  // cast is essential here according to C standard
}

// return parent's element from min_heap of an element at index idx
static inline void* get_parent_from_min_heap(
  const min_heap* in_heap,
  unsigned int idx
)
{
  return get_by_index_from_min_heap(in_heap, parent_idx(idx));
}

// return left's element from min_heap of an element at index idx
static inline void* get_left_from_min_heap(
    const min_heap* in_heap,
    unsigned int idx
)
{
  return get_by_index_from_min_heap(in_heap, left_idx(idx));
}

// return right's element from min_heap of an element at index idx
static inline void* get_right_from_min_heap(
    const min_heap* in_heap,
    unsigned int idx
)
{
  return get_by_index_from_min_heap(in_heap, right_idx(idx));
}

#endif // MIN_HEAP
