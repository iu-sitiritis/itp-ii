#include "min_heap.h"
#include <memory.h>
#include <stdlib.h>

min_heap* construct_min_heap(
  const void* array,
  unsigned int array_size,
  unsigned int type_size,
  int (*comparator)(void* left, void* right),
  int (*set_value)(void* qd_obj, void* new_value),
  void (*initialize_idx)(void* obj, long idx)
)
{
  min_heap* result = malloc(sizeof(min_heap));

  result->array = calloc(array_size, type_size);
  memcpy(result->array, array, array_size * type_size);

  result->heap_size = array_size;
  result->capacity = array_size;
  result->type_size = type_size;

  result->comparator = comparator;
  result->set_value = set_value;
  result->initialize_idx = initialize_idx;

  if (initialize_idx)
  {
    for (long i = 0; i < result->heap_size; ++i)
    {
      initialize_idx(
        get_by_index_from_min_heap(result, (unsigned int)i),
        i
      );
    }
  }

  for (long i = ((array_size - 1) / 2); i >= 0; --i)
  {
    min_heapify(result, (unsigned int)i);
  }

  return result;
}

void free_min_heap(min_heap* in_heap)
{
  free(in_heap->array);
  free(in_heap);
}

void min_heapify(min_heap* in_heap, unsigned int idx)
{
  unsigned int left_i = left_idx(idx);
  unsigned int right_i = right_idx(idx);

  unsigned int smallest_idx = idx;

  if (
       (left_i < in_heap->heap_size) &&
       (
         in_heap->comparator(
           get_by_index_from_min_heap(in_heap, left_i),
           get_by_index_from_min_heap(in_heap, idx)
         ) < 0
       )
  )
  {
    smallest_idx = left_i;
  }

  if (
    (right_i < in_heap->heap_size) &&
    (
      in_heap->comparator(
        get_by_index_from_min_heap(in_heap, right_i),
        get_by_index_from_min_heap(in_heap, smallest_idx)
      ) < 0
    )
  )
  {
    smallest_idx = right_i;
  }

  if (smallest_idx != idx)
  {
    swap_entries_min_heap(in_heap, idx, smallest_idx);
    min_heapify(in_heap, smallest_idx);
  }
}

void swap_entries_min_heap(
  min_heap* in_heap,
  unsigned int first_idx,
  unsigned int second_idx
)
{
  void* temp = malloc(in_heap->type_size);
  void* first_ptr = get_by_index_from_min_heap(in_heap, first_idx);
  void* second_ptr = get_by_index_from_min_heap(in_heap, second_idx);

  memcpy(temp, first_ptr, in_heap->type_size);
  memcpy(first_ptr, second_ptr, in_heap->type_size);
  memcpy(second_ptr, temp, in_heap->type_size);

  free(temp);

  if(in_heap->initialize_idx)
  {
    in_heap->initialize_idx(
      get_by_index_from_min_heap(in_heap, first_idx),
      first_idx
    );

    in_heap->initialize_idx(
      get_by_index_from_min_heap(in_heap, second_idx),
      second_idx
    );
  }
}
