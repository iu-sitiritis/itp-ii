#include "output_all_shortest_paths.h"

void print_all_shortest_paths_from_node_to_file(
  FILE* file_ptr,
  const shortest_paths_graph* spg,
  unsigned int destination_node_id
)
{
  graph_node* destination_node = get_node_by_id_from_spg(
    spg,
    destination_node_id
  );

  if (destination_node->num_paths)
  {
    fprintf(
      file_ptr,
      "The shortest path is %u.\n",
      *destination_node->distance_from_previous
    );
    fprintf(
      file_ptr,
      "The number of shortest paths is %llu:\n",
      destination_node->num_paths
    );
  }
  else
  {
    fprintf(file_ptr,
      "Initial and destination cities are not connected\n"
    );
  }

  unsigned int num_paths = 0;
  graph_node* print_list = constrcut_graph_node(0);
  recursively_print_all_shortest_paths_from_node_to_file(
    file_ptr,
    get_node_by_id_from_spg(
      spg,
      destination_node_id
    ),
    &num_paths,
    print_list
  );
  free_graph_node(print_list);
}

int recursively_print_all_shortest_paths_from_node_to_file(
  FILE* file_ptr,
  graph_node* cur_node,
  unsigned int* paths_counter,
  graph_node* print_list
)
{
  int num_chars_written = 0;

  if (cur_node->distance_from_previous)
  // distance is not infinite
  {
    prepend_previous_node(print_list, cur_node);

    if (*(cur_node->distance_from_previous))
    // if the current node is not a source
    {
      linked_list_node_graph_node* cur_list_node =
        cur_node->previous_nodes;

      while (cur_list_node)
      // for all previous nodes
      {
        num_chars_written +=
          recursively_print_all_shortest_paths_from_node_to_file(
            file_ptr,
            cur_list_node->cur_graph_node,
            paths_counter,
            print_list
          );

        cur_list_node = cur_list_node->previous_list_node;
      }
    }
    else
    {
      num_chars_written +=
        print_shortest_path_from_list(
          file_ptr,
          print_list->previous_nodes,
          ++(*paths_counter)
        );
    }

    // backtrack - remove the current node and free memory
    free(
      remove_first_node_from_previous_nodes(print_list)
    );
  }

  return num_chars_written;
}

int print_shortest_path_from_list(
  FILE* file_ptr,
  linked_list_node_graph_node* list_node,
  unsigned int path_number
)
{
  int num_chars_written = 0;

  num_chars_written += fprintf(file_ptr, "%u. ", path_number);

  while (list_node)
  {
    unsigned int cur_node_id = list_node->cur_graph_node->node_id;
    list_node = list_node->previous_list_node;

    if (list_node)
    // if current node to print is not the last in the path
    {
      num_chars_written += fprintf(file_ptr, "%u -> ", cur_node_id);
    }
    else
    // if the node is the last to print
    {
      num_chars_written += fprintf(file_ptr, "%u", cur_node_id);
    }
  }

  putc('\n', file_ptr);
  return num_chars_written + 1;
}

void print_error(
  FILE* file_ptr,
  input_file_read_status error_code
)
{
  switch (error_code)
  {
    case NUMBER_OF_NODES_LESS_MIN:
    case NUMBER_OF_NODES_GREATER_MAX:
    {
      fprintf(file_ptr, "Number of cities is out of range\n");
      break;
    }
    case SOURCE_NODE_OUT_OF_RANGE:
    {
      fprintf(file_ptr, "Chosen initial city does not exist\n");
      break;
    }
    case DESTINATION_NODE_OUT_OF_RANGE:
    {
      fprintf(file_ptr, "Chosen destination city does not exist\n");
      break;
    }
    case MATRIX_SIZE_LESS:
    case MATRIX_SIZE_GREATER:
    {
      fprintf(file_ptr, "Matrix size does not suit to the number of cities\n");
      break;
    }
    case WEIGHT_LESS_MIN:
    case WEIGHT_GREATER_MAX:
    {
      fprintf(file_ptr, "The distance between some cities is out of range\n");
      break;
    }
    case INVALID_FILE_STRUCTURE:
    {
      fprintf(file_ptr, "Structure of the input is invalid\n");
      break;
    }

    default:
    {
      fprintf(file_ptr, "An unknown error has occurred\n");
      break;
    }
  }
}
