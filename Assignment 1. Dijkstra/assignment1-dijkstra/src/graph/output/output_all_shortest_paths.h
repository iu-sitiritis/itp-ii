#ifndef OUTPUT_ALL_SHORTEST_PATHS_H
#define OUTPUT_ALL_SHORTEST_PATHS_H

#include "../shortest_paths_graph.h"
#include "../input/input_adj_matrix_for_finding_path.h"

// *Description:
// Having a shortest_paths_graph prints all (shortes) paths
// from destination_node to the source_node into a given file.
// If destination_node is unreachable from the source_node, prints:
// "Initial and destination cities are not connected".
// In case the destination_node is reachable from the source_node prints
// in the following format:
// The shortest path is <shortest_paths_length>.
// Number of shortest paths is <num_shortest_paths>:
// <shortest_path_number>. <path>
// E. g.:
// The shortest path is 9.
// Number of shortest paths is 2:
// 1. 3 -> 0 -> 4
// 2. 3 -> 1 -> 4
void print_all_shortest_paths_from_node_to_file(
  FILE* file_ptr,
  const shortest_paths_graph* spg,
  unsigned int destination_node_id
);

// *Return value
// Returns number of characters written to the file
int recursively_print_all_shortest_paths_from_node_to_file(
    FILE* file_ptr,
    graph_node* cur_node,
    unsigned int* paths_counter,
    graph_node* print_list
);

// *Return value
// Returns number of characters written to the file
int print_shortest_path_from_list(
  FILE* file_ptr,
  linked_list_node_graph_node* list_node,
  unsigned int path_number
);

// *Description:
// Prints an error message corresponding to error_code into a given file.
void print_error(
  FILE* file_ptr,
  input_file_read_status error_code
);

#endif // OUTPUT_ALL_SHORTEST_PATHS_H
