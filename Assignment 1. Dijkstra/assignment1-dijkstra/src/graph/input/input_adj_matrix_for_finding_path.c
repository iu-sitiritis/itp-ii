#include "input_adj_matrix_for_finding_path.h"
#include <errno.h>

path_finding_input* construct_path_finding_input()
{
  path_finding_input* result = malloc(sizeof(path_finding_input));
  result->a_matrix = NULL;
  result->source_node_id = 0;
  result->destination_node_id = 0;

  return result;
}

void free_path_finding_input(path_finding_input* input)
{
  if (input->a_matrix)
  {
    free_adj_matrix(input->a_matrix);
  }

  free(input);
}

input_file_read_status read_from_file(
  FILE* file_ptr,
  path_finding_input* result
)
{
  input_file_read_status read_status_result = OK;
  input_file_read_status read_status;
  char last_symbol;

  // read number of nodes
  unsigned int num_nodes;

  read_status =
    read_next_u_integer_from_file(
      file_ptr,
      &num_nodes,
      &last_symbol
    );

  if (read_status == INVALID_FILE_STRUCTURE)
  {
    read_status_result = INVALID_FILE_STRUCTURE;
  }
  else if (read_status == NO_EDGE)
  {
    read_status_result = INVALID_FILE_STRUCTURE;
  }
  else if (read_status == NEGATIVE_VALUE)
  {
    read_status_result = NUMBER_OF_NODES_LESS_MIN;
  }
  else if (
    read_status == GREATER_THAN_MAX ||
    // at this point it is known that num_nodes is assigned to a valid
    // unsigned int value
    num_nodes > MAX_NUM_NODES
  )
  {
    read_status_result = NUMBER_OF_NODES_GREATER_MAX;
  }
  else if (num_nodes < MIN_NUM_NODES)
  {
    read_status_result = NUMBER_OF_NODES_LESS_MIN;
  }
  else if (last_symbol != ' ')
  {
    read_status_result = INVALID_FILE_STRUCTURE;
  }
  else
  // num_nodes has a valid value
  // (i. e. MIN_NUM_NODES <= num_nodes <= MAX_NUM_NODES)
  // read source node id
  {
    unsigned int source_node_id;

    read_status =
      read_next_u_integer_from_file(
        file_ptr,
        &source_node_id,
        &last_symbol
      );

    if (read_status == INVALID_FILE_STRUCTURE)
    {
      read_status_result = INVALID_FILE_STRUCTURE;
    }
    else if (read_status == NO_EDGE)
    {
      read_status_result = INVALID_FILE_STRUCTURE;
    }
    else if (read_status == NEGATIVE_VALUE)
    {
      read_status_result = SOURCE_NODE_OUT_OF_RANGE;
    }
    else if (
      read_status == GREATER_THAN_MAX ||
      // at this point it is known that num_nodes is assigned to a valid
      // unsigned int value
      source_node_id >= num_nodes
    )
    {
      read_status_result = SOURCE_NODE_OUT_OF_RANGE;
    }
    else if (last_symbol != ' ')
    {
      read_status_result = INVALID_FILE_STRUCTURE;
    }
    else
    // source_node_id has a valid value
    // read destination node id
    {
      unsigned int destination_node_id;

      read_status =
        read_next_u_integer_from_file(
          file_ptr,
          &destination_node_id,
          &last_symbol
        );

      if (read_status == INVALID_FILE_STRUCTURE)
      {
        read_status_result = INVALID_FILE_STRUCTURE;
      }
      else if (read_status == NO_EDGE)
      {
        read_status_result = INVALID_FILE_STRUCTURE;
      }
      else if (read_status == NEGATIVE_VALUE)
      {
        read_status_result = DESTINATION_NODE_OUT_OF_RANGE;
      }
      else if (
        read_status == GREATER_THAN_MAX ||
        // at this point it is known that num_nodes is assigned to a valid
        // unsigned int value
        destination_node_id >= num_nodes
      )
      {
        read_status_result = DESTINATION_NODE_OUT_OF_RANGE;
      }
      else if (last_symbol != '\n')
      {
        read_status_result = INVALID_FILE_STRUCTURE;
      }
      else
      // destination_node_id has a valid value
      {
        if ((last_symbol = (char)getc(file_ptr)) != '\n')
        {
          read_status_result = INVALID_FILE_STRUCTURE;
        }
        else
        // read matrix
        {

          adj_matrix* a_matrix = construct_adj_matrix(num_nodes);

          unsigned int i = 0, j = 0;
          unsigned int next_value;

          while ((i < (num_nodes)) && (j < (num_nodes)))
          {

            read_status =
              read_next_u_integer_from_file(
                file_ptr,
                &next_value,
                &last_symbol
              );

            if (read_status == INVALID_FILE_STRUCTURE)
            {
              read_status_result = INVALID_FILE_STRUCTURE;
              break;
            }
            else if (read_status == GREATER_THAN_MAX ||
              (
                (read_status == NO_EDGE) && (i == j)
              )
            )
            {
              read_status_result = WEIGHT_GREATER_MAX;
              break;
            }
            else if (read_status == NEGATIVE_VALUE)
            {
              read_status_result = WEIGHT_LESS_MIN;
              break;
            }
            // at this point input is either a number or a '*'
            else if (read_status == U_INT || read_status == NO_EDGE)
            {
              if ((i == j) && (i == (num_nodes - 1)) && (last_symbol != EOF))
              // the last entry does not ends with a '\n' (new line)
              {
                read_status_result = INVALID_FILE_STRUCTURE;
                break;
              }
              else if (read_status == U_INT)
              {
                if ((i == j) && (next_value > 0))
                {
                  read_status_result = WEIGHT_GREATER_MAX;
                  break;
                }
                else if (
                  next_value < MIN_EDGE_WEIGHT &&
                  // ! (next_value == 0 => i == j)
                  // that is the next_value == 0
                  // does not imply that i == j
                  !(!(next_value == 0) || (i == j))
                )
                {
                  read_status_result = WEIGHT_LESS_MIN;
                  break;
                }
                else if (next_value > MAX_EDGE_WEIGHT)
                {
                  read_status_result = WEIGHT_GREATER_MAX;
                  break;
                }
                else
                // finally, everything is okay and we are able to
                // put this stupid value into the a_matrix
                {
                  a_matrix->matrix[i][j] = malloc(sizeof(unsigned int));
                  *(a_matrix->matrix[i][j]) = next_value;
                }
              }
              // nothing to do for read_status == NO_EDGE, as it is already initialized to NULL

              if (last_symbol == ' ')
              {
                if (j == (num_nodes - 1))
                // Already a mistake in file.
                // To determine the mistake - read next token
                // and determine whether it was valid or not
                {
                  if (
                    valid_read_from_file(
                        read_status =
                      read_next_u_integer_from_file(
                        file_ptr,
                        &next_value,
                        &last_symbol
                      )
                    )
                  )
                  {
                    read_status_result = MATRIX_SIZE_GREATER;
                  }
                  else
                  {
                    read_status_result = INVALID_FILE_STRUCTURE;
                  }
                  
                  break;
                }
                else
                {
                  ++j;
                }
              }
              else if ((last_symbol == '\n') || (last_symbol == EOF))
              {
                ++i;

                if (j < (num_nodes - 1))
                {
                  read_status_result = MATRIX_SIZE_LESS;
                  break;
                }
                else
                {
                  j = 0;
                }
              }

            }

          } // while

          if (read_status_result == OK)
          {
            result->a_matrix = a_matrix;
            result->source_node_id = source_node_id;
            result->destination_node_id = destination_node_id;
          }
        }
      }
    }
  }

  return read_status_result;
}
