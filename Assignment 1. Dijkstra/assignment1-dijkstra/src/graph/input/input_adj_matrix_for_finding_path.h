﻿#ifndef INPUT_ADJ_MATRIX_FOR_FINDING_PATH_H
#define INPUT_ADJ_MATRIX_FOR_FINDING_PATH_H

#include "../adjacency_matrix.h"
#include <stdio.h>
#include <limits.h>
#include <ctype.h>


#define MAX_DIGITS 20


#define MIN_NUM_NODES 5
#define MAX_NUM_NODES 50

#define MIN_EDGE_WEIGHT 1
#define MAX_EDGE_WEIGHT 20


typedef
  struct path_finding_input
  {
    adj_matrix* a_matrix;
    unsigned int source_node_id;
    unsigned int destination_node_id;
  }
path_finding_input;

typedef
  enum input_file_read_status
  {
    OK = 0,

    U_INT,
    NO_EDGE,

    GREATER_THAN_MAX,
    NEGATIVE_VALUE,

    NUMBER_OF_NODES_LESS_MIN,
    NUMBER_OF_NODES_GREATER_MAX,

    SOURCE_NODE_OUT_OF_RANGE,
    DESTINATION_NODE_OUT_OF_RANGE,

    MATRIX_SIZE_LESS,
    MATRIX_SIZE_GREATER,

    WEIGHT_LESS_MIN,
    WEIGHT_GREATER_MAX,

    INVALID_FILE_STRUCTURE
  }
input_file_read_status;

// *Description:
// Creates an instance of path_finding_input and initializes it with default
// values, namely:
// a_matrix = NULL
// source_node_id = 0
// destination_node_id = 0
// *Return value:
// Returns a pointer to the newly created instance of path_finding_input
path_finding_input* construct_path_finding_input(void);

// Description:
// Frees memory occupied by a path_finding_input*
void free_path_finding_input(path_finding_input* input);

// *Description:
// Reads
// num_nodes source_node_id destination_node_id
// input_adjacency_matrixt_of_size_num_nodes
// from file referenced by file_ptr.
// and sets result to the newly created instance of path_finding_input
// in case input file is valid, otherwise - do not changes its value.
// All values must be unsigned.
// *Return value:
// Returns a validity of a given input file
input_file_read_status read_from_file(
  FILE* file_ptr,
  path_finding_input* result
);

// *Description:
// Tries to read a next unsigned integer from a given file
// and in case of success places the read unsigned int at *result.
// Unsuccessful read does not change *result
// Utilizes getc, so it advances a cursor within a file.
// Cursor of the file must point directly to a digit, otherwise - error
// will be returned
// *Return value:
// Returns validity of a given input file.
// Possible input file statuses are:
// U_INT - read an unsigned integer for the file
// NO_EDGE - read a '*', result is untouched
// GREATER_THAN_MAX - number of digits is grater than maximum
// INVALID_FILE_STRUCTURE - no digits were read
// NEGATIVE_VALUE - value read is negative
// or the input finished with a non-whitespace symbol
// Sets *result to the unsigned int read
// Sets *last_symbol to the last encountered symbol
static inline input_file_read_status read_next_u_integer_from_file(
  FILE* file_ptr,
  unsigned int* result,
  char* last_symbol
)
{
  char read_sequence[MAX_DIGITS + 1];
  short int cur_digit_idx = -1;

  while (
    (
      isdigit(*last_symbol = (char)getc(file_ptr)) ||
      (
        // the first symbol read is a '-' (minus)
        (cur_digit_idx == -1) &&
        (*last_symbol == '-')
      )
    )
    &&
    cur_digit_idx < MAX_DIGITS
  )
  {
    read_sequence[++cur_digit_idx] = *last_symbol;
  }
  read_sequence[++cur_digit_idx] = '\0';

  input_file_read_status read_status;
  if ((read_sequence[0] == '0') && (cur_digit_idx > 1))
  // if something was actually read and there was a leading '0'
  {
    read_status = INVALID_FILE_STRUCTURE;
  }
  else if (read_sequence[0] == '-')
  {
    if (isspace(*last_symbol) && (cur_digit_idx > 0))
    // the first char read is a '-' (minus)
    // and a number was actually read
    {
      read_status = NEGATIVE_VALUE;
    }
    else
    {
      read_status = INVALID_FILE_STRUCTURE;
    }
  }
  else if (cur_digit_idx >= MAX_DIGITS)
  {
    read_status = GREATER_THAN_MAX;
  }
  else if (*last_symbol == '*' && cur_digit_idx == 0)
  {
    *last_symbol = (char)getc(file_ptr);
    read_status = NO_EDGE;
  }
  else if (
    (!isspace(*last_symbol) && (*last_symbol != EOF)) ||
    cur_digit_idx == 0
  )
  // if the last read symbol is not a whitespace or end of file
  // nothing was read to read_sequence, file is invalid
  {
    read_status = INVALID_FILE_STRUCTURE;
  }
  else
  // if errors did not occur and there is a number to convert
  {
    // convert read_sequence to unsigned int base 10 and
    // set *result to its value
    read_status = U_INT;
    *result = (unsigned int)strtol(read_sequence, NULL, 10);
  }

  return read_status;
}

// *Description:
// Maps read statuses with valid and invalid ones
// *Return value:
// 0 <=> status is invalid
// 1 <=> status is valid
static inline short int valid_read_from_file(input_file_read_status read_status)
{
  short int result = 0;

  switch (read_status)
  {
    case OK:


    case U_INT:
    case NO_EDGE:
    {
      result = 1;
      break;
    }

    case GREATER_THAN_MAX:
    case NEGATIVE_VALUE:


    case NUMBER_OF_NODES_LESS_MIN:
    case NUMBER_OF_NODES_GREATER_MAX:

    case SOURCE_NODE_OUT_OF_RANGE:
    case DESTINATION_NODE_OUT_OF_RANGE:

    case MATRIX_SIZE_LESS:
    case MATRIX_SIZE_GREATER:

    case WEIGHT_LESS_MIN:
    case WEIGHT_GREATER_MAX:

    case INVALID_FILE_STRUCTURE:
    {
      result = 0;
      break;
    }
  }

  return result;
}

#endif // INPUT_ADJ_MATRIX_FOR_FINDING_PATH_H
