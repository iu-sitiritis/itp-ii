#include "graph_algorithms.h"

shortest_paths_graph* find_shortest_paths_from_single_source(
  const adj_matrix* a_matrix,
  const unsigned int source_node_id
)
{
  // initialize the resulting graph
  shortest_paths_graph* result_graph =
    construct_shortest_paths_graph(a_matrix->size, source_node_id);

  graph_node* source_node =
    get_node_by_id_from_spg(result_graph, source_node_id);
  set_distance(source_node, 0);
  source_node->num_paths = 1;

  min_prior_queue* mn_q = construct_min_heap(
      result_graph->nodes,
      result_graph->num_nodes,
      sizeof(graph_node*),
      graph_node_ptr_ptr_abstract_comparator,
      set_distance_for_heap,
      initialize_idx_in_heap
    );

  graph_node* cur_node;

  while (!is_min_heap_empty(mn_q))
  // while there are left unprocessed nodes
  {
    extract_min(mn_q, &cur_node);

    if (cur_node->distance_from_previous)
    // and the extracted node is reachable
    {
      for (unsigned int i = 0; i < a_matrix->size; ++i)
      {
        unsigned int* dist_to_adj_node =
          a_matrix->matrix[i][cur_node->node_id];

        if (
          dist_to_adj_node &&
          cur_node->node_id != i
        )
        // if other node is adjacent to the current
        {
          graph_node* adj_node = get_node_by_id_from_spg(result_graph, i);

          // relax
          unsigned int distance_to_dest_node =
            *(cur_node->distance_from_previous) + *dist_to_adj_node;

          int comparison_result =
            graph_node_ptr_value_comparator(
              adj_node,
              distance_to_dest_node
            );

          if (comparison_result > 0)
          // new distance is shorter
          {
            clear_previous(adj_node);
            prepend_previous_node(adj_node, cur_node);
            set_distance_in_prior_q(
              mn_q,
              (unsigned int)adj_node->idx_in_heap,
              &distance_to_dest_node
            );
          }
          else if (comparison_result == 0)
          // new distance is the same
          {
            prepend_previous_node(adj_node, cur_node);
          }
        }
      }
    }
  }

  free_min_heap(mn_q);
  return result_graph;
}
