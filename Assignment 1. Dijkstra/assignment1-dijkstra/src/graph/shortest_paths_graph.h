#ifndef SHORTEST_PATH_GRAPH_H
#define SHORTEST_PATH_GRAPH_H

#include "graph_node.h"

typedef
  struct shortest_paths_graph
  {
    // array of pointer to all nodes of the graph of a size num_nodes
    graph_node** nodes;
    unsigned int num_nodes;
    unsigned int source_node_id;
  }
shortest_paths_graph;

// *Description:
// Constructs a new default instance of shortest_path_graph.
// num_nodes & source_node_id will be set to corresponding arguments,
// nodes will be initialized with num_nodes graph_nodes with their
// IDs set in range [0; num_nodes)
// Return value:
// Pointer to the newly created instance
shortest_paths_graph* construct_shortest_paths_graph(
  unsigned int num_nodes,
  unsigned int source_node_id
);

// *Description:
// Free memory occupied by a shortest_paths_graph
void free_shortest_paths_graph(shortest_paths_graph* graph);

// *Description:
// Returns a graph node with its node_id == node_id
// If node_id is invalid - returns NULL
graph_node* get_node_by_id_from_spg(
  shortest_paths_graph* graph,
  unsigned int node_id
);

#endif // SHORTEST_PATH_GRAPH_H
