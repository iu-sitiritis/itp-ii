#include "graph_node.h"
#include <stdlib.h>

linked_list_node_graph_node* construct_linked_list_node_graph_node(
  linked_list_node_graph_node* previous_list_node,
  graph_node* cur_graph_node
)
{
  linked_list_node_graph_node* new_list_node =
    malloc(sizeof(linked_list_node_graph_node));

  new_list_node->cur_graph_node = cur_graph_node;
  new_list_node->previous_list_node = previous_list_node;

  return new_list_node;
}

graph_node* constrcut_graph_node(unsigned int node_id)
{
  graph_node* result = malloc(sizeof(graph_node));

  result->node_id = node_id;

  result->distance_from_previous = NULL;
  result->previous_nodes = NULL;
  result->num_previous_nodes = 0;
  result->num_paths = 0;

  result->idx_in_heap = -1;

  return result;
}

void free_graph_node(graph_node* node)
{
  clear_previous(node);
  free(node);
}

void prepend_previous_node(graph_node* cur_node, graph_node* node_to_add)
{
  cur_node->previous_nodes = construct_linked_list_node_graph_node(
        cur_node->previous_nodes,
        node_to_add
      );
  ++cur_node->num_previous_nodes;

  cur_node->num_paths += node_to_add->num_paths;
}

linked_list_node_graph_node* remove_first_node_from_previous_nodes(
  graph_node* g_node
)
{
  linked_list_node_graph_node* result_node = g_node->previous_nodes;

  if (result_node)
  {
    if (
      !(g_node->previous_nodes = result_node->previous_list_node)
    )
    // if the next node assigned to g_node->previous_nodes is NULL
    // clear the distance_from_previous
    {
      clear_distance_from_previous(g_node);
    }
    --(g_node->num_previous_nodes);

    // I warned, if the num_paths has changed - the behavior is undefined
    g_node->num_paths -= result_node->cur_graph_node->num_paths;
  }

  return result_node;
}

void clear_distance_from_previous(graph_node* node)
{
  free(node->distance_from_previous);
  node->distance_from_previous = NULL;
}

void clear_previous(graph_node* node)
{
  clear_distance_from_previous(node);
  node->num_previous_nodes = 0;
  node->num_paths = 0;

  while (node->previous_nodes)
  {
    linked_list_node_graph_node* next_to_clear = node->previous_nodes->previous_list_node;
    free(node->previous_nodes);
    node->previous_nodes = next_to_clear;
  }
}

void set_distance(graph_node* cur_node, unsigned int distance_from_previous)
{
  if (!cur_node->distance_from_previous)
  {
    cur_node->distance_from_previous = malloc(sizeof(unsigned int));
  }

  *(cur_node->distance_from_previous) = distance_from_previous;
}

int graph_node_ptr_value_comparator(
  const graph_node* g_node,
  unsigned int distance
)
{
  unsigned int* left_dist_ptr = g_node->distance_from_previous;

  if (left_dist_ptr)
  {
    return (int)*left_dist_ptr - (int)distance;
  }
  else
  {
    return 1;
  }
}

int graph_node_ptr_comparator(
  const graph_node* left_graph_node,
  const graph_node* right_graph_node
)
{
  unsigned int* left_dist_ptr = left_graph_node->distance_from_previous;
  unsigned int* right_dist_ptr = right_graph_node->distance_from_previous;

  // narrowing conversion is fine here, for only the equivalence of the pointers
  // is the only interest here
  int result = (int)(left_dist_ptr - right_dist_ptr);

  if (result)
  // if the distance is different
  {
    if (!left_dist_ptr)
    // if left_dist_ptr is NULL
    {
      return 1;
    }
    else if (!right_dist_ptr)
    // if right_dist_ptr is NULL
    {
      return -1;
    }
    else
    // if they are both not NULL
    {
      if (*left_dist_ptr < *right_dist_ptr)
      {
        return -1;
      }
      else if (*right_dist_ptr < *left_dist_ptr)
      {
        return 1;
      }
      else
      {
        return 0;
      }
    }

  }

  return result;
}

int graph_node_ptr_ptr_abstract_comparator(
  void* left_graph_node,
  void* right_graph_node
)
{
  return
    graph_node_ptr_comparator(
      VOID_PTR_TO_G_NODE_PTR(left_graph_node),
      VOID_PTR_TO_G_NODE_PTR(right_graph_node)
  );
}

int set_distance_for_heap(void* g_node_ptr_ptr, void* other_g_node_ptr_ptr)
{
  graph_node* g_node_ptr = VOID_PTR_TO_G_NODE_PTR(g_node_ptr_ptr);
  graph_node* other_g_node_ptr = VOID_PTR_TO_G_NODE_PTR(other_g_node_ptr_ptr);

  if (other_g_node_ptr->distance_from_previous)
  {
    set_distance(g_node_ptr, *(other_g_node_ptr->distance_from_previous));
  }
  else
  {
    g_node_ptr->distance_from_previous = NULL;
  }

  return 0;
}

void initialize_idx_in_heap(void* obj, long idx)
{
  VOID_PTR_TO_G_NODE_PTR(obj)->idx_in_heap = idx;
}

long set_distance_in_prior_q(
  min_prior_queue* mn_q,
  unsigned int idx,
  unsigned int* distance
)
{
  graph_node* temp_node = constrcut_graph_node(0);

  if (distance)
  {
    set_distance(temp_node, *distance);
  }

  long result = update_key(mn_q, idx, temp_node);

  free_graph_node(temp_node);

  return result;
}
