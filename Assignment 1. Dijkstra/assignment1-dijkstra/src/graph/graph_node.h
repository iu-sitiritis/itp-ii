#ifndef GRAPH_NODE
#define GRAPH_NODE

#include <stddef.h>
#include "../min_priority_queue/min_priority_queue.h"

#define VOID_PTR_TO_G_NODE_PTR(VOID_PTR) (*((graph_node**)VOID_PTR))

struct linked_list_node_graph_node;
typedef
  struct linked_list_node_graph_node
linked_list_node_graph_node;

typedef
  struct graph_node
  {
    // number that identifies the node
    unsigned int node_id;

    // Total distance from all previous nodes till the source
    // NULL - distance is unknown
    unsigned int* distance_from_previous;
    // list of nodes from which the current can be reached
    linked_list_node_graph_node* previous_nodes;
    // the size of the list
    unsigned int num_previous_nodes;
    // number of paths to the current node from a source
    unsigned long long int num_paths;

    long idx_in_heap;
  }
graph_node;

struct linked_list_node_graph_node
{
  linked_list_node_graph_node* previous_list_node;
  graph_node* cur_graph_node;
};

linked_list_node_graph_node* construct_linked_list_node_graph_node(
  linked_list_node_graph_node* previous_list_node,
  graph_node* cur_graph_node
);

// construct a new graph node with default values and node_id, namely:
// previous_nodes = NULL - no previous nodes
// num_previous_nodes = 0
// distance_from_previous = NULL
// num_paths = 0
// idx_in_heap = 0
graph_node* constrcut_graph_node(unsigned int node_id);

// frees memory allocated for a graph_node
void free_graph_node(graph_node* node);

// adds node_to_add at the beginning of list of previous_nodes of cur_node
void prepend_previous_node(graph_node* cur_node, graph_node* node_to_add);

// *Description:
// Removes a first entry from previous_nodes of a cur_node and decreases
// num_previous_nodes. Since a pointer to removed value is returned,
// memory is not being freed for the popped entry.
// If result_node->cur_graph_node->num_paths changed, since it was
// added to the list of previous nodes - the behavior is undefined.
// *Return value:
// Returns a pointer to the removed value.
// When there is nothing to remove - returns NULL
linked_list_node_graph_node* remove_first_node_from_previous_nodes(
  graph_node* g_node
);

void clear_distance_from_previous(graph_node* node);

// clears the list of previous_nodes of a graph_node
void clear_previous(graph_node* node);

// set *distance_from_previous of cur_node to distance_from_previous
void set_distance(graph_node* cur_node, unsigned int distance_from_previous);

// *Description:
// Compares a g_node's distance with distance
// *Return value:
// negative <=> g_node's distance < distance
// 0 <=> g_node's distance == distance
// positive <=> g_node's distance > distance
int graph_node_ptr_value_comparator(
  const graph_node* g_node,
  unsigned int distance
);

// *Description:
// Compares 2 graph_node*
// *Return value:
// negative <=> left < right
// 0 <=> left == right
// positive <=> left > right
int graph_node_ptr_comparator(
  const graph_node* left_graph_node,
  const graph_node* right_graph_node
);

// *Description:
// Compares 2 graph_node** pointed by void*
// *Return value:
// negative <=> left < right
// 0 <=> left == right
// positive <=> left > right
int graph_node_ptr_ptr_abstract_comparator(
  void* left_graph_node,
  void* right_graph_node
);

// sets a distance of a g_node in a heap to an other_g_node's distance
int set_distance_for_heap(void* g_node, void* other_g_node);

// used to reactively set idx_in_prior_q for an obj
void initialize_idx_in_heap(void* obj, long idx);

// Description:
// Sets mn_q[idx].distance_from_previous == distance, if distance is valid
// Return value:
// Same as update_key()
long set_distance_in_prior_q(
  min_prior_queue* mn_q,
  unsigned int idx,
  unsigned int* distance
);

#endif // GRAPH_NODE
