#include "shortest_paths_graph.h"

#include <stdlib.h>
#include <memory.h>

shortest_paths_graph* construct_shortest_paths_graph(
  unsigned int num_nodes,
  unsigned int source_node_id
)
{
  shortest_paths_graph* result = malloc(sizeof(shortest_paths_graph));
  result->nodes = calloc(num_nodes, sizeof(graph_node*));
  result->num_nodes = num_nodes;
  result->source_node_id = source_node_id;

  for (unsigned int i = 0; i < num_nodes; ++i)
  {
    result->nodes[i] = constrcut_graph_node(i);
  }

  return result;
}

void free_shortest_paths_graph(shortest_paths_graph* graph)
{
  for (unsigned int i = 0; i < graph->num_nodes; ++i)
  {
    free_graph_node(graph->nodes[i]);
  }

  free(graph->nodes);
  free(graph);
}

graph_node* get_node_by_id_from_spg(
  shortest_paths_graph* graph,
  unsigned int node_id
)
{
  return graph->nodes[node_id];
}
