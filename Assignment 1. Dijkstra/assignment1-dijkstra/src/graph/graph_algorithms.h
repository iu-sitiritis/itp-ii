#ifndef GRAPH_ALGORITHMS_H
#define GRAPH_ALGORITHMS_H

#include "adjacency_matrix.h"
#include "shortest_paths_graph.h"

// *Description:
// Returns a graph of the shortest paths to all nodes from a_matrix
// starting from a specified source node using Dijkstra's algorithm
// and a minimal priority queue. If a node in the graph is unreachable
// from the source - it will have its distance_from_previous
// and previous_nodes set to NULL.
// It is assumed that a_matrix points to a valid initialized a_matrix,
// otherwise the behavior is undefined.
shortest_paths_graph* find_shortest_paths_from_single_source(
  const adj_matrix* a_matrix,
  const unsigned int source_node_id
);

#endif // GRAPH_ALGORITHMS_H
