#include "adjacency_matrix.h"

adj_matrix* construct_adj_matrix(unsigned int size)
{
  adj_matrix* result = malloc(sizeof(adj_matrix));

  result->size = size;
  result->matrix = calloc(size, sizeof(unsigned int**));

  for (unsigned int i = 0; i < size; ++i)
  {
    result->matrix[i] = calloc(size, sizeof(unsigned int*));

    for (unsigned int j = 0; j < size; ++j)
    {
      result->matrix[i][j] = NULL;
    }
  }

  return result;
}

void free_adj_matrix(adj_matrix* a_matrix)
{
  for (unsigned int i = 0; i < a_matrix->size; ++i)
  {
    for (unsigned int j = 0; j < a_matrix->size; ++j)
    {
      free(a_matrix->matrix[i][j]);
    }

    free(a_matrix->matrix[i]);
  }

  free(a_matrix->matrix);
  free(a_matrix);
}
