#ifndef ADJACENCY_MATRIX_H
#define ADJACENCY_MATRIX_H

#include <stdlib.h>

typedef
  struct adj_matrix
  {
    unsigned int*** matrix;
    // corresponds to number of nodes in a graph
    unsigned int size;
  }
adj_matrix;

// *Description:
// Constructs a square adjacency matrix of a specified size
// and fills it with NULLs
// *Return value:
// Pointer to the newly create instance of adj_matrix
adj_matrix* construct_adj_matrix(unsigned int size);

// *Description:
// Frees memory allocated to a_matrix
void free_adj_matrix(adj_matrix* a_matrix);

#endif // ADJACENCY_MATRIX_H
