TEMPLATE = app
CONFIG -= app_bundle
CONFIG -= qt
CONFIG += c11

SOURCES += \
    src/main.c \
    src/graph/graph_node.c \
    src/heap/min_heap.c \
    src/min_priority_queue/min_priority_queue.c \
    test/test_graph.c \
    test/test_min_heap.c \
    test/test_min_queue_of_graph_nodes.c \
    src/graph/adjacency_matrix.c \
    src/graph/graph_algorithms.c \
    src/graph/input/input_adj_matrix_for_finding_path.c \
    test/test_input_from_file.c \
    src/graph/shortest_paths_graph.c \
    src/graph/output/output_all_shortest_paths.c

HEADERS += \
    src/common/define.h \
    src/graph/graph_node.h \
    src/heap/heap.h \
    src/heap/min_heap.h \
    src/min_priority_queue/min_priority_queue.h \
    src/graph/adjacency_matrix.h \
    src/graph/graph_algorithms.h \
    src/graph/input/input_adj_matrix_for_finding_path.h \
    src/graph/shortest_paths_graph.h \
    src/graph/output/output_all_shortest_paths.h \
    src/common/define.h

DISTFILES += \
    test/input.txt
