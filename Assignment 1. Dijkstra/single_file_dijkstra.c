#include <stdlib.h>
#include <stdio.h>
#include <limits.h>
#include <ctype.h>
#include <errno.h>
#include <memory.h>
#include <stddef.h>

#ifndef ADJACENCY_MATRIX_H
#define ADJACENCY_MATRIX_H

typedef
  struct adj_matrix
  {
    unsigned int*** matrix;
    // corresponds to number of nodes in a graph
    unsigned int size;
  }
adj_matrix;

// *Description:
// Constructs a square adjacency matrix of a specified size
// and fills it with NULLs
// *Return value:
// Pointer to the newly create instance of adj_matrix
adj_matrix* construct_adj_matrix(unsigned int size);

// *Description:
// Frees memory allocated to a_matrix
void free_adj_matrix(adj_matrix* a_matrix);

#endif // ADJACENCY_MATRIX_H

adj_matrix* construct_adj_matrix(unsigned int size)
{
  adj_matrix* result = malloc(sizeof(adj_matrix));

  result->size = size;
  result->matrix = calloc(size, sizeof(unsigned int**));

  for (unsigned int i = 0; i < size; ++i)
  {
    result->matrix[i] = calloc(size, sizeof(unsigned int*));

    for (unsigned int j = 0; j < size; ++j)
    {
      result->matrix[i][j] = NULL;
    }
  }

  return result;
}

void free_adj_matrix(adj_matrix* a_matrix)
{
  for (unsigned int i = 0; i < a_matrix->size; ++i)
  {
    for (unsigned int j = 0; j < a_matrix->size; ++j)
    {
      free(a_matrix->matrix[i][j]);
    }

    free(a_matrix->matrix[i]);
  }

  free(a_matrix->matrix);
  free(a_matrix);
}

#ifndef INPUT_ADJ_MATRIX_FOR_FINDING_PATH_H
#define INPUT_ADJ_MATRIX_FOR_FINDING_PATH_H


#define MAX_DIGITS 20


#define MIN_NUM_NODES 5
#define MAX_NUM_NODES 50

#define MIN_EDGE_WEIGHT 1
#define MAX_EDGE_WEIGHT 20


typedef
  struct path_finding_input
  {
    adj_matrix* a_matrix;
    unsigned int source_node_id;
    unsigned int destination_node_id;
  }
path_finding_input;

typedef
  enum input_file_read_status
  {
    OK = 0,

    U_INT,
    NO_EDGE,

    GREATER_THAN_MAX,
    NEGATIVE_VALUE,

    NUMBER_OF_NODES_LESS_MIN,
    NUMBER_OF_NODES_GREATER_MAX,

    SOURCE_NODE_OUT_OF_RANGE,
    DESTINATION_NODE_OUT_OF_RANGE,

    MATRIX_SIZE_LESS,
    MATRIX_SIZE_GREATER,

    WEIGHT_LESS_MIN,
    WEIGHT_GREATER_MAX,

    INVALID_FILE_STRUCTURE
  }
input_file_read_status;

// *Description:
// Creates an instance of path_finding_input and initializes it with default
// values, namely:
// a_matrix = NULL
// source_node_id = 0
// destination_node_id = 0
// *Return value:
// Returns a pointer to the newly created instance of path_finding_input
path_finding_input* construct_path_finding_input(void);

// Description:
// Frees memory occupied by a path_finding_input*
void free_path_finding_input(path_finding_input* input);

// *Description:
// Reads
// num_nodes source_node_id destination_node_id
// input_adjacency_matrixt_of_size_num_nodes
// from file referenced by file_ptr.
// and sets result to the newly created instance of path_finding_input
// in case input file is valid, otherwise - do not changes its value.
// All values must be unsigned.
// *Return value:
// Returns a validity of a given input file
input_file_read_status read_from_file(
  FILE* file_ptr,
  path_finding_input* result
);

// *Description:
// Tries to read a next unsigned integer from a given file
// and in case of success places the read unsigned int at *result.
// Unsuccessful read does not change *result
// Utilizes getc, so it advances a cursor within a file.
// Cursor of the file must point directly to a digit, otherwise - error
// will be returned
// *Return value:
// Returns validity of a given input file.
// Possible input file statuses are:
// U_INT - read an unsigned integer for the file
// NO_EDGE - read a '*', result is untouched
// GREATER_THAN_MAX - number of digits is grater than maximum
// INVALID_FILE_STRUCTURE - no digits were read
// NEGATIVE_VALUE - value read is negative
// or the input finished with a non-whitespace symbol
// Sets *result to the unsigned int read
// Sets *last_symbol to the last encountered symbol
static inline input_file_read_status read_next_u_integer_from_file(
  FILE* file_ptr,
  unsigned int* result,
  char* last_symbol
)
{
  char read_sequence[MAX_DIGITS + 1];
  short int cur_digit_idx = -1;

  while (
    (
      isdigit(*last_symbol = (char)getc(file_ptr)) ||
      (
        // the first symbol read is a '-' (minus)
        (cur_digit_idx == -1) &&
        (*last_symbol == '-')
      )
    )
    &&
    cur_digit_idx < MAX_DIGITS
  )
  {
    read_sequence[++cur_digit_idx] = *last_symbol;
  }
  read_sequence[++cur_digit_idx] = '\0';

  input_file_read_status read_status;
  if ((read_sequence[0] == '0') && (cur_digit_idx > 1))
  // if something was actually read and there was a leading '0'
  {
    read_status = INVALID_FILE_STRUCTURE;
  }
  else if (read_sequence[0] == '-')
  {
    if (isspace(*last_symbol) && (cur_digit_idx > 0))
    // the first char read is a '-' (minus)
    // and a number was actually read
    {
      read_status = NEGATIVE_VALUE;
    }
    else
    {
      read_status = INVALID_FILE_STRUCTURE;
    }
  }
  else if (cur_digit_idx >= MAX_DIGITS)
  {
    read_status = GREATER_THAN_MAX;
  }
  else if (*last_symbol == '*' && cur_digit_idx == 0)
  {
    *last_symbol = (char)getc(file_ptr);
    read_status = NO_EDGE;
  }
  else if (
    (!isspace(*last_symbol) && (*last_symbol != EOF)) ||
    cur_digit_idx == 0
  )
  // if the last read symbol is not a whitespace or end of file
  // nothing was read to read_sequence, file is invalid
  {
    read_status = INVALID_FILE_STRUCTURE;
  }
  else
  // if errors did not occur and there is a number to convert
  {
    // convert read_sequence to unsigned int base 10 and
    // set *result to its value
    read_status = U_INT;
    *result = (unsigned int)strtol(read_sequence, NULL, 10);
  }

  return read_status;
}

// *Description:
// Maps read statuses with valid and invalid ones
// *Return value:
// 0 <=> status is invalid
// 1 <=> status is valid
static inline short int valid_read_from_file(input_file_read_status read_status)
{
  short int result = 0;

  switch (read_status)
  {
    case OK:


    case U_INT:
    case NO_EDGE:
    {
      result = 1;
      break;
    }

    case GREATER_THAN_MAX:
    case NEGATIVE_VALUE:


    case NUMBER_OF_NODES_LESS_MIN:
    case NUMBER_OF_NODES_GREATER_MAX:

    case SOURCE_NODE_OUT_OF_RANGE:
    case DESTINATION_NODE_OUT_OF_RANGE:

    case MATRIX_SIZE_LESS:
    case MATRIX_SIZE_GREATER:

    case WEIGHT_LESS_MIN:
    case WEIGHT_GREATER_MAX:

    case INVALID_FILE_STRUCTURE:
    {
      result = 0;
      break;
    }
  }

  return result;
}

#endif // INPUT_ADJ_MATRIX_FOR_FINDING_PATH_H

path_finding_input* construct_path_finding_input()
{
  path_finding_input* result = malloc(sizeof(path_finding_input));
  result->a_matrix = NULL;
  result->source_node_id = 0;
  result->destination_node_id = 0;

  return result;
}

void free_path_finding_input(path_finding_input* input)
{
  if (input->a_matrix)
  {
    free_adj_matrix(input->a_matrix);
  }

  free(input);
}

input_file_read_status read_from_file(
  FILE* file_ptr,
  path_finding_input* result
)
{
  input_file_read_status read_status_result = OK;
  input_file_read_status read_status;
  char last_symbol;

  // read number of nodes
  unsigned int num_nodes;

  read_status =
    read_next_u_integer_from_file(
      file_ptr,
      &num_nodes,
      &last_symbol
    );

  if (read_status == INVALID_FILE_STRUCTURE)
  {
    read_status_result = INVALID_FILE_STRUCTURE;
  }
  else if (read_status == NO_EDGE)
  {
    read_status_result = INVALID_FILE_STRUCTURE;
  }
  else if (read_status == NEGATIVE_VALUE)
  {
    read_status_result = NUMBER_OF_NODES_LESS_MIN;
  }
  else if (
    read_status == GREATER_THAN_MAX ||
    // at this point it is known that num_nodes is assigned to a valid
    // unsigned int value
    num_nodes > MAX_NUM_NODES
  )
  {
    read_status_result = NUMBER_OF_NODES_GREATER_MAX;
  }
  else if (num_nodes < MIN_NUM_NODES)
  {
    read_status_result = NUMBER_OF_NODES_LESS_MIN;
  }
  else if (last_symbol != ' ')
  {
    read_status_result = INVALID_FILE_STRUCTURE;
  }
  else
  // num_nodes has a valid value
  // (i. e. MIN_NUM_NODES <= num_nodes <= MAX_NUM_NODES)
  // read source node id
  {
    unsigned int source_node_id;

    read_status =
      read_next_u_integer_from_file(
        file_ptr,
        &source_node_id,
        &last_symbol
      );

    if (read_status == INVALID_FILE_STRUCTURE)
    {
      read_status_result = INVALID_FILE_STRUCTURE;
    }
    else if (read_status == NO_EDGE)
    {
      read_status_result = INVALID_FILE_STRUCTURE;
    }
    else if (read_status == NEGATIVE_VALUE)
    {
      read_status_result = SOURCE_NODE_OUT_OF_RANGE;
    }
    else if (
      read_status == GREATER_THAN_MAX ||
      // at this point it is known that num_nodes is assigned to a valid
      // unsigned int value
      source_node_id >= num_nodes
    )
    {
      read_status_result = SOURCE_NODE_OUT_OF_RANGE;
    }
    else if (last_symbol != ' ')
    {
      read_status_result = INVALID_FILE_STRUCTURE;
    }
    else
    // source_node_id has a valid value
    // read destination node id
    {
      unsigned int destination_node_id;

      read_status =
        read_next_u_integer_from_file(
          file_ptr,
          &destination_node_id,
          &last_symbol
        );

      if (read_status == INVALID_FILE_STRUCTURE)
      {
        read_status_result = INVALID_FILE_STRUCTURE;
      }
      else if (read_status == NO_EDGE)
      {
        read_status_result = INVALID_FILE_STRUCTURE;
      }
      else if (read_status == NEGATIVE_VALUE)
      {
        read_status_result = DESTINATION_NODE_OUT_OF_RANGE;
      }
      else if (
        read_status == GREATER_THAN_MAX ||
        // at this point it is known that num_nodes is assigned to a valid
        // unsigned int value
        destination_node_id >= num_nodes
      )
      {
        read_status_result = DESTINATION_NODE_OUT_OF_RANGE;
      }
      else if (last_symbol != '\n')
      {
        read_status_result = INVALID_FILE_STRUCTURE;
      }
      else
      // destination_node_id has a valid value
      {
        if ((last_symbol = (char)getc(file_ptr)) != '\n')
        {
          read_status_result = INVALID_FILE_STRUCTURE;
        }
        else
        // read matrix
        {

          adj_matrix* a_matrix = construct_adj_matrix(num_nodes);

          unsigned int i = 0, j = 0;
          unsigned int next_value;

          while ((i < (num_nodes)) && (j < (num_nodes)))
          {

            read_status =
              read_next_u_integer_from_file(
                file_ptr,
                &next_value,
                &last_symbol
              );

            if (read_status == INVALID_FILE_STRUCTURE)
            {
              read_status_result = INVALID_FILE_STRUCTURE;
              break;
            }
            else if (read_status == GREATER_THAN_MAX ||
              (
                (read_status == NO_EDGE) && (i == j)
              )
            )
            {
              read_status_result = WEIGHT_GREATER_MAX;
              break;
            }
            else if (read_status == NEGATIVE_VALUE)
            {
              read_status_result = WEIGHT_LESS_MIN;
              break;
            }
            // at this point input is either a number or a '*'
            else if (read_status == U_INT || read_status == NO_EDGE)
            {
              if ((i == j) && (i == (num_nodes - 1)) && (last_symbol != EOF))
              // the last entry does not ends with a '\n' (new line)
              {
                read_status_result = INVALID_FILE_STRUCTURE;
                break;
              }
              else if (read_status == U_INT)
              {
                if ((i == j) && (next_value > 0))
                {
                  read_status_result = WEIGHT_GREATER_MAX;
                  break;
                }
                else if (
                  next_value < MIN_EDGE_WEIGHT &&
                  // ! (next_value == 0 => i == j)
                  // that is the next_value == 0
                  // does not imply that i == j
                  !(!(next_value == 0) || (i == j))
                )
                {
                  read_status_result = WEIGHT_LESS_MIN;
                  break;
                }
                else if (next_value > MAX_EDGE_WEIGHT)
                {
                  read_status_result = WEIGHT_GREATER_MAX;
                  break;
                }
                else
                // finally, everything is okay and we are able to
                // put this stupid value into the a_matrix
                {
                  a_matrix->matrix[i][j] = malloc(sizeof(unsigned int));
                  *(a_matrix->matrix[i][j]) = next_value;
                }
              }
              // nothing to do for read_status == NO_EDGE, as it is already initialized to NULL

              if (last_symbol == ' ')
              {
                if (j == (num_nodes - 1))
                // Already a mistake in file.
                // To determine the mistake - read next token
                // and determine whether it was valid or not
                {
                  if (
                    valid_read_from_file(
                        read_status =
                      read_next_u_integer_from_file(
                        file_ptr,
                        &next_value,
                        &last_symbol
                      )
                    )
                  )
                  {
                    read_status_result = MATRIX_SIZE_GREATER;
                  }
                  else
                  {
                    read_status_result = INVALID_FILE_STRUCTURE;
                  }
                  
                  break;
                }
                else
                {
                  ++j;
                }
              }
              else if ((last_symbol == '\n') || (last_symbol == EOF))
              {
                ++i;

                if (j < (num_nodes - 1))
                {
                  read_status_result = MATRIX_SIZE_LESS;
                  break;
                }
                else
                {
                  j = 0;
                }
              }

            }

          } // while

          if (read_status_result == OK)
          {
            result->a_matrix = a_matrix;
            result->source_node_id = source_node_id;
            result->destination_node_id = destination_node_id;
          }
        }
      }
    }
  }

  return read_status_result;
}

#ifndef HEAP
#define HEAP

// get parent's index in a heap
static inline unsigned int parent_idx(unsigned int idx)
{
  return (idx - 1) / 2;
}

// get left's index in a heap
static inline unsigned int left_idx(unsigned int idx)
{
  return (2 * idx) + 1;
}

// get right's index in a heap
static inline unsigned int right_idx(unsigned int idx)
{
  return (2 * idx) + 2;
}

#endif

#ifndef MIN_HEAP
#define MIN_HEAP

typedef
  struct min_heap
  {
    void* array;

    unsigned int type_size;

    unsigned int capacity;
    unsigned int heap_size;

    // Description:
    // function used to compare 2 objects stored in the heap
    // negative int <=> left < right
    // Return value:
    // 0 <=> left == right
    // positive int <=> left > right
    // can be reversed, so the heap becomes a max_heap
    int (*comparator)(void* left, void* right);
    // Description:
    // Function used to change the value of a stored object obj to new_value
    // Return value:
    // 0 <=> no errors assigning new_value
    // !0 <=> an error occurred
    int (*set_value)(void* obj, void* new_value);
    // Description:
    // Function used to initialize obj with its index idx in the heap
    // -1 - obj is not in the heap
    void (*initialize_idx)(void* obj, long idx);
  }
min_heap;

// Description:
// construct a new min_heap using:
// an input array
// array element's size
// a comparator for the type
// Return value:
// returns a pointer to the newly created min_heap
min_heap* construct_min_heap(
  const void* array,
  unsigned int array_size,
  unsigned int type_size,
  int (*comparator)(void* left, void* right),
  int (*set_value)(void* qd_obj, void* new_key_value),
  void (*initialize_idx)(void* obj, long idx)
);

// free a min_heap
void free_min_heap(min_heap* in_heap);

// keep the property of a min_heap starting at index idx, namely:
// each parent is <= to its left & right
void min_heapify(min_heap* in_heap, unsigned int idx);

// swap entries in a min_heap at indexes first_idx & last_idx
void swap_entries_min_heap(
  min_heap* in_heap,
  unsigned int first_idx,
  unsigned int last_idx
);

// !0 <=> heap is empty
// 0 <=> not empty
static inline unsigned int is_min_heap_empty(const min_heap* in_heap)
{
  return in_heap->heap_size == 0;
}

// 1 <=> idx is valid for in_heap
// 0 <=> idx is not valid for in_heap
static inline int is_index_valid(const min_heap* in_heap, long idx)
{
  return idx < in_heap->heap_size && idx >= 0;
}

// return element from min_heap at index idx
static inline void* get_by_index_from_min_heap(
  const min_heap* in_heap,
  unsigned int idx
)
{
  return (void *)(((char *)(in_heap->array)) + (in_heap->type_size * idx));
  // cast is essential here according to C standard
}

// return parent's element from min_heap of an element at index idx
static inline void* get_parent_from_min_heap(
  const min_heap* in_heap,
  unsigned int idx
)
{
  return get_by_index_from_min_heap(in_heap, parent_idx(idx));
}

// return left's element from min_heap of an element at index idx
static inline void* get_left_from_min_heap(
    const min_heap* in_heap,
    unsigned int idx
)
{
  return get_by_index_from_min_heap(in_heap, left_idx(idx));
}

// return right's element from min_heap of an element at index idx
static inline void* get_right_from_min_heap(
    const min_heap* in_heap,
    unsigned int idx
)
{
  return get_by_index_from_min_heap(in_heap, right_idx(idx));
}

#endif // MIN_HEAP

min_heap* construct_min_heap(
  const void* array,
  unsigned int array_size,
  unsigned int type_size,
  int (*comparator)(void* left, void* right),
  int (*set_value)(void* qd_obj, void* new_value),
  void (*initialize_idx)(void* obj, long idx)
)
{
  min_heap* result = malloc(sizeof(min_heap));

  result->array = calloc(array_size, type_size);
  memcpy(result->array, array, array_size * type_size);

  result->heap_size = array_size;
  result->capacity = array_size;
  result->type_size = type_size;

  result->comparator = comparator;
  result->set_value = set_value;
  result->initialize_idx = initialize_idx;

  if (initialize_idx)
  {
    for (long i = 0; i < result->heap_size; ++i)
    {
      initialize_idx(
        get_by_index_from_min_heap(result, (unsigned int)i),
        i
      );
    }
  }

  for (long i = ((array_size - 1) / 2); i >= 0; --i)
  {
    min_heapify(result, (unsigned int)i);
  }

  return result;
}

void free_min_heap(min_heap* in_heap)
{
  free(in_heap->array);
  free(in_heap);
}

void min_heapify(min_heap* in_heap, unsigned int idx)
{
  unsigned int left_i = left_idx(idx);
  unsigned int right_i = right_idx(idx);

  unsigned int smallest_idx = idx;

  if (
       (left_i < in_heap->heap_size) &&
       (
         in_heap->comparator(
           get_by_index_from_min_heap(in_heap, left_i),
           get_by_index_from_min_heap(in_heap, idx)
         ) < 0
       )
  )
  {
    smallest_idx = left_i;
  }

  if (
    (right_i < in_heap->heap_size) &&
    (
      in_heap->comparator(
        get_by_index_from_min_heap(in_heap, right_i),
        get_by_index_from_min_heap(in_heap, smallest_idx)
      ) < 0
    )
  )
  {
    smallest_idx = right_i;
  }

  if (smallest_idx != idx)
  {
    swap_entries_min_heap(in_heap, idx, smallest_idx);
    min_heapify(in_heap, smallest_idx);
  }
}

void swap_entries_min_heap(
  min_heap* in_heap,
  unsigned int first_idx,
  unsigned int second_idx
)
{
  void* temp = malloc(in_heap->type_size);
  void* first_ptr = get_by_index_from_min_heap(in_heap, first_idx);
  void* second_ptr = get_by_index_from_min_heap(in_heap, second_idx);

  memcpy(temp, first_ptr, in_heap->type_size);
  memcpy(first_ptr, second_ptr, in_heap->type_size);
  memcpy(second_ptr, temp, in_heap->type_size);

  free(temp);

  if(in_heap->initialize_idx)
  {
    in_heap->initialize_idx(
      get_by_index_from_min_heap(in_heap, first_idx),
      first_idx
    );

    in_heap->initialize_idx(
      get_by_index_from_min_heap(in_heap, second_idx),
      second_idx
    );
  }
}

#ifndef MIN_PRIORITY_QUEUE
#define MIN_PRIORITY_QUEUE

typedef min_heap min_prior_queue;

// extracts minimal value from a min_prior_queue into min_value
// returns # of elements left in the queue
// 0 means no elements left, hence nothing to extract
// the queue is empty - does not change the value of min_value
unsigned int extract_min(min_prior_queue* q, void* min_value);

// Description:
// updates a value of an element stored in the queue at index idx
// by setting it to new_value and keeps the heap property
// Return value:
// returns the new index of the updated object
// Errors:
// if provided idx is incorrect - returns -1
// if new_key_value is an invalid - returns -2
long update_key(min_prior_queue* q, unsigned int idx, void* new_value);

// Description:
// Checks if new_value is a valid value for object stored in q at index idx.
// For min_prior_queue value is valid <=> value <= q[idx]
// For max_prior_queue value is valid <=> value >= q[idx]
// Return value:
// 1 <=> new_key is valid
// 0 <=> new_key is not valid
static inline int is_new_value_valid(
  min_prior_queue* q,
  unsigned int idx,
  void* new_value
)
{
  return q->comparator(new_value, get_by_index_from_min_heap(q, idx)) <= 0;
}

#endif // MIN_PRIORITY_QUEUE

unsigned int extract_min(min_prior_queue* q, void* min_value)
{
  if (!is_min_heap_empty(q))
  {
    memcpy(min_value, q->array, q->type_size);
    memcpy(q->array,
      get_by_index_from_min_heap(q, q->heap_size - 1),
      q->type_size
    );
    --(q->heap_size);

    if(q->initialize_idx)
    {
      q->initialize_idx(min_value, -1);

      if (q->heap_size)
      // if heap has at least 1 entry in it
      {
        q->initialize_idx(q->array, 0);
      }
    }

    min_heapify(q, 0);
  }

  return q->heap_size;
}

long update_key(min_prior_queue* q, unsigned int idx, void* new_value)
{
  // validate idx
  if (!is_index_valid(q, idx))
  {
    return -1;
  }
  else if (!is_new_value_valid(q, idx, &new_value))
  // validate new_key_value
  {
    return -2;
  }

  q->set_value(get_by_index_from_min_heap(q, idx), &new_value);

  // while idx have not reached a root (i. e. idx != 0)
  // compare the q[idx] with its parent,
  // and if it is < its parent - swap them and change idx to parent_idx(idx)
  while (
    idx &&
    (
      q->comparator(
        get_by_index_from_min_heap(q, idx),
        get_by_index_from_min_heap(q, parent_idx(idx))
      ) < 0
    )
  )
  {
    swap_entries_min_heap(q, idx, parent_idx(idx));
    idx = parent_idx(idx);
  }

  return idx;
}

#ifndef GRAPH_NODE
#define GRAPH_NODE

#define VOID_PTR_TO_G_NODE_PTR(VOID_PTR) (*((graph_node**)VOID_PTR))

struct linked_list_node_graph_node;
typedef
  struct linked_list_node_graph_node
linked_list_node_graph_node;

typedef
  struct graph_node
  {
    // number that identifies the node
    unsigned int node_id;

    // Total distance from all previous nodes till the source
    // NULL - distance is unknown
    unsigned int* distance_from_previous;
    // list of nodes from which the current can be reached
    linked_list_node_graph_node* previous_nodes;
    // the size of the list
    unsigned int num_previous_nodes;
    // number of paths to the current node from a source
    unsigned long long int num_paths;

    long idx_in_heap;
  }
graph_node;

struct linked_list_node_graph_node
{
  linked_list_node_graph_node* previous_list_node;
  graph_node* cur_graph_node;
};

linked_list_node_graph_node* construct_linked_list_node_graph_node(
  linked_list_node_graph_node* previous_list_node,
  graph_node* cur_graph_node
);

// construct a new graph node with default values and node_id, namely:
// previous_nodes = NULL - no previous nodes
// num_previous_nodes = 0
// distance_from_previous = NULL
// num_paths = 0
// idx_in_heap = 0
graph_node* constrcut_graph_node(unsigned int node_id);

// frees memory allocated for a graph_node
void free_graph_node(graph_node* node);

// adds node_to_add at the beginning of list of previous_nodes of cur_node
void prepend_previous_node(graph_node* cur_node, graph_node* node_to_add);

// *Description:
// Removes a first entry from previous_nodes of a cur_node and decreases
// num_previous_nodes. Since a pointer to removed value is returned,
// memory is not being freed for the popped entry.
// If result_node->cur_graph_node->num_paths changed, since it was
// added to the list of previous nodes - the behavior is undefined.
// *Return value:
// Returns a pointer to the removed value.
// When there is nothing to remove - returns NULL
linked_list_node_graph_node* remove_first_node_from_previous_nodes(
  graph_node* g_node
);

void clear_distance_from_previous(graph_node* node);

// clears the list of previous_nodes of a graph_node
void clear_previous(graph_node* node);

// set *distance_from_previous of cur_node to distance_from_previous
void set_distance(graph_node* cur_node, unsigned int distance_from_previous);

// *Description:
// Compares a g_node's distance with distance
// *Return value:
// negative <=> g_node's distance < distance
// 0 <=> g_node's distance == distance
// positive <=> g_node's distance > distance
int graph_node_ptr_value_comparator(
  const graph_node* g_node,
  unsigned int distance
);

// *Description:
// Compares 2 graph_node*
// *Return value:
// negative <=> left < right
// 0 <=> left == right
// positive <=> left > right
int graph_node_ptr_comparator(
  const graph_node* left_graph_node,
  const graph_node* right_graph_node
);

// *Description:
// Compares 2 graph_node** pointed by void*
// *Return value:
// negative <=> left < right
// 0 <=> left == right
// positive <=> left > right
int graph_node_ptr_ptr_abstract_comparator(
  void* left_graph_node,
  void* right_graph_node
);

// sets a distance of a g_node in a heap to an other_g_node's distance
int set_distance_for_heap(void* g_node, void* other_g_node);

// used to reactively set idx_in_prior_q for an obj
void initialize_idx_in_heap(void* obj, long idx);

// Description:
// Sets mn_q[idx].distance_from_previous == distance, if distance is valid
// Return value:
// Same as update_key()
long set_distance_in_prior_q(
  min_prior_queue* mn_q,
  unsigned int idx,
  unsigned int* distance
);

#endif // GRAPH_NODE

linked_list_node_graph_node* construct_linked_list_node_graph_node(
  linked_list_node_graph_node* previous_list_node,
  graph_node* cur_graph_node
)
{
  linked_list_node_graph_node* new_list_node =
    malloc(sizeof(linked_list_node_graph_node));

  new_list_node->cur_graph_node = cur_graph_node;
  new_list_node->previous_list_node = previous_list_node;

  return new_list_node;
}

graph_node* constrcut_graph_node(unsigned int node_id)
{
  graph_node* result = malloc(sizeof(graph_node));

  result->node_id = node_id;

  result->distance_from_previous = NULL;
  result->previous_nodes = NULL;
  result->num_previous_nodes = 0;
  result->num_paths = 0;

  result->idx_in_heap = -1;

  return result;
}

void free_graph_node(graph_node* node)
{
  clear_previous(node);
  free(node);
}

void prepend_previous_node(graph_node* cur_node, graph_node* node_to_add)
{
  cur_node->previous_nodes = construct_linked_list_node_graph_node(
        cur_node->previous_nodes,
        node_to_add
      );
  ++cur_node->num_previous_nodes;

  cur_node->num_paths += node_to_add->num_paths;
}

linked_list_node_graph_node* remove_first_node_from_previous_nodes(
  graph_node* g_node
)
{
  linked_list_node_graph_node* result_node = g_node->previous_nodes;

  if (result_node)
  {
    if (
      !(g_node->previous_nodes = result_node->previous_list_node)
    )
    // if the next node assigned to g_node->previous_nodes is NULL
    // clear the distance_from_previous
    {
      clear_distance_from_previous(g_node);
    }
    --(g_node->num_previous_nodes);

    // I warned, if the num_paths has changed - the behavior is undefined
    g_node->num_paths -= result_node->cur_graph_node->num_paths;
  }

  return result_node;
}

void clear_distance_from_previous(graph_node* node)
{
  free(node->distance_from_previous);
  node->distance_from_previous = NULL;
}

void clear_previous(graph_node* node)
{
  clear_distance_from_previous(node);
  node->num_previous_nodes = 0;
  node->num_paths = 0;

  while (node->previous_nodes)
  {
    linked_list_node_graph_node* next_to_clear = node->previous_nodes->previous_list_node;
    free(node->previous_nodes);
    node->previous_nodes = next_to_clear;
  }
}

void set_distance(graph_node* cur_node, unsigned int distance_from_previous)
{
  if (!cur_node->distance_from_previous)
  {
    cur_node->distance_from_previous = malloc(sizeof(unsigned int));
  }

  *(cur_node->distance_from_previous) = distance_from_previous;
}

int graph_node_ptr_value_comparator(
  const graph_node* g_node,
  unsigned int distance
)
{
  unsigned int* left_dist_ptr = g_node->distance_from_previous;

  if (left_dist_ptr)
  {
    return (int)*left_dist_ptr - (int)distance;
  }
  else
  {
    return 1;
  }
}

int graph_node_ptr_comparator(
  const graph_node* left_graph_node,
  const graph_node* right_graph_node
)
{
  unsigned int* left_dist_ptr = left_graph_node->distance_from_previous;
  unsigned int* right_dist_ptr = right_graph_node->distance_from_previous;

  // narrowing conversion is fine here, for only the equivalence of the pointers
  // is the only interest here
  int result = (int)(left_dist_ptr - right_dist_ptr);

  if (result)
  // if the distance is different
  {
    if (!left_dist_ptr)
    // if left_dist_ptr is NULL
    {
      return 1;
    }
    else if (!right_dist_ptr)
    // if right_dist_ptr is NULL
    {
      return -1;
    }
    else
    // if they are both not NULL
    {
      if (*left_dist_ptr < *right_dist_ptr)
      {
        return -1;
      }
      else if (*right_dist_ptr < *left_dist_ptr)
      {
        return 1;
      }
      else
      {
        return 0;
      }
    }

  }

  return result;
}

int graph_node_ptr_ptr_abstract_comparator(
  void* left_graph_node,
  void* right_graph_node
)
{
  return
    graph_node_ptr_comparator(
      VOID_PTR_TO_G_NODE_PTR(left_graph_node),
      VOID_PTR_TO_G_NODE_PTR(right_graph_node)
  );
}

int set_distance_for_heap(void* g_node_ptr_ptr, void* other_g_node_ptr_ptr)
{
  graph_node* g_node_ptr = VOID_PTR_TO_G_NODE_PTR(g_node_ptr_ptr);
  graph_node* other_g_node_ptr = VOID_PTR_TO_G_NODE_PTR(other_g_node_ptr_ptr);

  if (other_g_node_ptr->distance_from_previous)
  {
    set_distance(g_node_ptr, *(other_g_node_ptr->distance_from_previous));
  }
  else
  {
    g_node_ptr->distance_from_previous = NULL;
  }

  return 0;
}

void initialize_idx_in_heap(void* obj, long idx)
{
  VOID_PTR_TO_G_NODE_PTR(obj)->idx_in_heap = idx;
}

long set_distance_in_prior_q(
  min_prior_queue* mn_q,
  unsigned int idx,
  unsigned int* distance
)
{
  graph_node* temp_node = constrcut_graph_node(0);

  if (distance)
  {
    set_distance(temp_node, *distance);
  }

  long result = update_key(mn_q, idx, temp_node);

  free_graph_node(temp_node);

  return result;
}

#ifndef SHORTEST_PATH_GRAPH_H
#define SHORTEST_PATH_GRAPH_H

typedef
  struct shortest_paths_graph
  {
    // array of pointer to all nodes of the graph of a size num_nodes
    graph_node** nodes;
    unsigned int num_nodes;
    unsigned int source_node_id;
  }
shortest_paths_graph;

// *Description:
// Constructs a new default instance of shortest_path_graph.
// num_nodes & source_node_id will be set to corresponding arguments,
// nodes will be initialized with num_nodes graph_nodes with their
// IDs set in range [0; num_nodes)
// Return value:
// Pointer to the newly created instance
shortest_paths_graph* construct_shortest_paths_graph(
  unsigned int num_nodes,
  unsigned int source_node_id
);

// *Description:
// Free memory occupied by a shortest_paths_graph
void free_shortest_paths_graph(shortest_paths_graph* graph);

// *Description:
// Returns a graph node with its node_id == node_id
// If node_id is invalid - returns NULL
graph_node* get_node_by_id_from_spg(
  shortest_paths_graph* graph,
  unsigned int node_id
);

#endif // SHORTEST_PATH_GRAPH_H

shortest_paths_graph* construct_shortest_paths_graph(
  unsigned int num_nodes,
  unsigned int source_node_id
)
{
  shortest_paths_graph* result = malloc(sizeof(shortest_paths_graph));
  result->nodes = calloc(num_nodes, sizeof(graph_node*));
  result->num_nodes = num_nodes;
  result->source_node_id = source_node_id;

  for (unsigned int i = 0; i < num_nodes; ++i)
  {
    result->nodes[i] = constrcut_graph_node(i);
  }

  return result;
}

void free_shortest_paths_graph(shortest_paths_graph* graph)
{
  for (unsigned int i = 0; i < graph->num_nodes; ++i)
  {
    free_graph_node(graph->nodes[i]);
  }

  free(graph->nodes);
  free(graph);
}

graph_node* get_node_by_id_from_spg(
  shortest_paths_graph* graph,
  unsigned int node_id
)
{
  return graph->nodes[node_id];
}

#ifndef GRAPH_ALGORITHMS_H
#define GRAPH_ALGORITHMS_H

// *Description:
// Returns a graph of the shortest paths to all nodes from a_matrix
// starting from a specified source node using Dijkstra's algorithm
// and a minimal priority queue. If a node in the graph is unreachable
// from the source - it will have its distance_from_previous
// and previous_nodes set to NULL.
// It is assumed that a_matrix points to a valid initialized a_matrix,
// otherwise the behavior is undefined.
shortest_paths_graph* find_shortest_paths_from_single_source(
  const adj_matrix* a_matrix,
  const unsigned int source_node_id
);

#endif // GRAPH_ALGORITHMS_H

shortest_paths_graph* find_shortest_paths_from_single_source(
  const adj_matrix* a_matrix,
  const unsigned int source_node_id
)
{
  // initialize the resulting graph
  shortest_paths_graph* result_graph =
    construct_shortest_paths_graph(a_matrix->size, source_node_id);

  graph_node* source_node =
    get_node_by_id_from_spg(result_graph, source_node_id);
  set_distance(source_node, 0);
  source_node->num_paths = 1;

  min_prior_queue* mn_q = construct_min_heap(
      result_graph->nodes,
      result_graph->num_nodes,
      sizeof(graph_node*),
      graph_node_ptr_ptr_abstract_comparator,
      set_distance_for_heap,
      initialize_idx_in_heap
    );

  graph_node* cur_node;

  while (!is_min_heap_empty(mn_q))
  // while there are left unprocessed nodes
  {
    extract_min(mn_q, &cur_node);

    if (cur_node->distance_from_previous)
    // and the extracted node is reachable
    {
      for (unsigned int i = 0; i < a_matrix->size; ++i)
      {
        unsigned int* dist_to_adj_node =
          a_matrix->matrix[i][cur_node->node_id];

        if (
          dist_to_adj_node &&
          cur_node->node_id != i
        )
        // if other node is adjacent to the current
        {
          graph_node* adj_node = get_node_by_id_from_spg(result_graph, i);

          // relax
          unsigned int distance_to_dest_node =
            *(cur_node->distance_from_previous) + *dist_to_adj_node;

          int comparison_result =
            graph_node_ptr_value_comparator(
              adj_node,
              distance_to_dest_node
            );

          if (comparison_result > 0)
          // new distance is shorter
          {
            clear_previous(adj_node);
            prepend_previous_node(adj_node, cur_node);
            set_distance_in_prior_q(
              mn_q,
              (unsigned int)adj_node->idx_in_heap,
              &distance_to_dest_node
            );
          }
          else if (comparison_result == 0)
          // new distance is the same
          {
            prepend_previous_node(adj_node, cur_node);
          }
        }
      }
    }
  }

  free_min_heap(mn_q);
  return result_graph;
}

#ifndef OUTPUT_ALL_SHORTEST_PATHS_H
#define OUTPUT_ALL_SHORTEST_PATHS_H

// *Description:
// Having a shortest_paths_graph prints all (shortes) paths
// from destination_node to the source_node into a given file.
// If destination_node is unreachable from the source_node, prints:
// "Initial and destination cities are not connected".
// In case the destination_node is reachable from the source_node prints
// in the following format:
// The shortest path is <shortest_paths_length>.
// Number of shortest paths is <num_shortest_paths>:
// <shortest_path_number>. <path>
// E. g.:
// The shortest path is 9.
// Number of shortest paths is 2:
// 1. 3 -> 0 -> 4
// 2. 3 -> 1 -> 4
void print_all_shortest_paths_from_node_to_file(
  FILE* file_ptr,
  const shortest_paths_graph* spg,
  unsigned int destination_node_id
);

// *Return value
// Returns number of characters written to the file
int recursively_print_all_shortest_paths_from_node_to_file(
    FILE* file_ptr,
    graph_node* cur_node,
    unsigned int* paths_counter,
    graph_node* print_list
);

// *Return value
// Returns number of characters written to the file
int print_shortest_path_from_list(
  FILE* file_ptr,
  linked_list_node_graph_node* list_node,
  unsigned int path_number
);

// *Description:
// Prints an error message corresponding to error_code into a given file.
void print_error(
  FILE* file_ptr,
  input_file_read_status error_code
);

#endif // OUTPUT_ALL_SHORTEST_PATHS_H

void print_all_shortest_paths_from_node_to_file(
  FILE* file_ptr,
  const shortest_paths_graph* spg,
  unsigned int destination_node_id
)
{
  graph_node* destination_node = get_node_by_id_from_spg(
    spg,
    destination_node_id
  );

  if (destination_node->num_paths)
  {
    fprintf(
      file_ptr,
      "The shortest path is %u.\n",
      *destination_node->distance_from_previous
    );
    fprintf(
      file_ptr,
      "The number of shortest paths is %llu:\n",
      destination_node->num_paths
    );
  }
  else
  {
    fprintf(file_ptr,
      "Initial and destination cities are not connected\n"
    );
  }

  unsigned int num_paths = 0;
  graph_node* print_list = constrcut_graph_node(0);
  recursively_print_all_shortest_paths_from_node_to_file(
    file_ptr,
    get_node_by_id_from_spg(
      spg,
      destination_node_id
    ),
    &num_paths,
    print_list
  );
  free_graph_node(print_list);
}

int recursively_print_all_shortest_paths_from_node_to_file(
  FILE* file_ptr,
  graph_node* cur_node,
  unsigned int* paths_counter,
  graph_node* print_list
)
{
  int num_chars_written = 0;

  if (cur_node->distance_from_previous)
  // distance is not infinite
  {
    prepend_previous_node(print_list, cur_node);

    if (*(cur_node->distance_from_previous))
    // if the current node is not a source
    {
      linked_list_node_graph_node* cur_list_node =
        cur_node->previous_nodes;

      while (cur_list_node)
      // for all previous nodes
      {
        num_chars_written +=
          recursively_print_all_shortest_paths_from_node_to_file(
            file_ptr,
            cur_list_node->cur_graph_node,
            paths_counter,
            print_list
          );

        cur_list_node = cur_list_node->previous_list_node;
      }
    }
    else
    {
      num_chars_written +=
        print_shortest_path_from_list(
          file_ptr,
          print_list->previous_nodes,
          ++(*paths_counter)
        );
    }

    // backtrack - remove the current node and free memory
    free(
      remove_first_node_from_previous_nodes(print_list)
    );
  }

  return num_chars_written;
}

int print_shortest_path_from_list(
  FILE* file_ptr,
  linked_list_node_graph_node* list_node,
  unsigned int path_number
)
{
  int num_chars_written = 0;

  num_chars_written += fprintf(file_ptr, "%u. ", path_number);

  while (list_node)
  {
    unsigned int cur_node_id = list_node->cur_graph_node->node_id;
    list_node = list_node->previous_list_node;

    if (list_node)
    // if current node to print is not the last in the path
    {
      num_chars_written += fprintf(file_ptr, "%u -> ", cur_node_id);
    }
    else
    // if the node is the last to print
    {
      num_chars_written += fprintf(file_ptr, "%u", cur_node_id);
    }
  }

  putc('\n', file_ptr);
  return num_chars_written + 1;
}

void print_error(
  FILE* file_ptr,
  input_file_read_status error_code
)
{
  switch (error_code)
  {
    case NUMBER_OF_NODES_LESS_MIN:
    case NUMBER_OF_NODES_GREATER_MAX:
    {
      fprintf(file_ptr, "Number of cities is out of range\n");
      break;
    }
    case SOURCE_NODE_OUT_OF_RANGE:
    {
      fprintf(file_ptr, "Chosen initial city does not exist\n");
      break;
    }
    case DESTINATION_NODE_OUT_OF_RANGE:
    {
      fprintf(file_ptr, "Chosen destination city does not exist\n");
      break;
    }
    case MATRIX_SIZE_LESS:
    case MATRIX_SIZE_GREATER:
    {
      fprintf(file_ptr, "Matrix size does not suit to the number of cities\n");
      break;
    }
    case WEIGHT_LESS_MIN:
    case WEIGHT_GREATER_MAX:
    {
      fprintf(file_ptr, "The distance between some cities is out of range\n");
      break;
    }
    case INVALID_FILE_STRUCTURE:
    {
      fprintf(file_ptr, "Structure of the input is invalid\n");
      break;
    }

    default:
    {
      fprintf(file_ptr, "An unknown error has occurred\n");
      break;
    }
  }
}

void find_shortest_paths(
  FILE* input_file_ptr,
  FILE* output_file_ptr
)
{
  path_finding_input* input_pf = construct_path_finding_input();
  input_file_read_status read_status = read_from_file(input_file_ptr, input_pf);

  switch (read_status)
  {
    case OK:
    {
      //print_path_finding_input(input_pf);

      shortest_paths_graph* spg =
        find_shortest_paths_from_single_source(
          input_pf->a_matrix,
          input_pf->source_node_id
        );

      print_all_shortest_paths_from_node_to_file(
          output_file_ptr,
          spg,
          input_pf->destination_node_id
      );

      free_shortest_paths_graph((shortest_paths_graph*)spg);
      break;
    }

    default:
    {
      print_error(output_file_ptr, read_status);
    }
  }

  // free resources
  free_path_finding_input(input_pf);
}

int main()
{
  FILE* input_file_ptr = fopen("input.txt", "r");
  FILE* output_file_ptr = fopen("TymurLysenkoOutput.txt", "w");

  find_shortest_paths(input_file_ptr, output_file_ptr);

  // close files
  fclose(input_file_ptr);
  fclose(output_file_ptr);
  return 0;
}
