	.file	"fact_while.c"
	.text
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC1:
	.string	"%d"
	.text
	.p2align 4,,15
	.globl	fact
	.type	fact, @function
fact:
.LFB11:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	xorl	%eax, %eax
	movl	%edi, %ebp
	movl	$2, %esi
	pushq	%rbx
	.cfi_def_cfa_offset 24
	.cfi_offset 3, -24
	movl	%edi, %ebx
	leaq	.LC1(%rip), %rdi
	subq	$24, %rsp
	.cfi_def_cfa_offset 48
	call	printf@PLT
	cmpb	$1, %bl
	jbe	.L7
	leal	-2(%rbx), %eax
	leal	-1(%rbx), %ecx
	cmpb	$14, %al
	jbe	.L8
	movl	%ebx, 12(%rsp)
	movl	%ecx, %edx
	movdqa	.LC3(%rip), %xmm7
	xorl	%eax, %eax
	pxor	%xmm6, %xmm6
	shrb	$4, %dl
	pxor	%xmm5, %xmm5
	pxor	%xmm1, %xmm1
	movdqa	.LC0(%rip), %xmm9
	movzbl	%dl, %edx
	movd	12(%rsp), %xmm4
	punpcklbw	%xmm4, %xmm4
	punpcklwd	%xmm4, %xmm4
	pshufd	$0, %xmm4, %xmm4
	paddb	.LC2(%rip), %xmm4
	.p2align 4,,10
	.p2align 3
.L4:
	movdqa	%xmm4, %xmm8
	movdqa	%xmm4, %xmm0
	paddb	%xmm7, %xmm4
	addl	$1, %eax
	punpcklbw	%xmm6, %xmm8
	punpckhbw	%xmm6, %xmm0
	movdqa	%xmm8, %xmm2
	punpckhwd	%xmm5, %xmm8
	movdqa	%xmm0, %xmm3
	punpcklwd	%xmm5, %xmm2
	punpcklwd	%xmm5, %xmm3
	punpckhwd	%xmm5, %xmm0
	movdqa	%xmm2, %xmm12
	movdqa	%xmm2, %xmm10
	punpckldq	%xmm1, %xmm12
	punpckhdq	%xmm1, %xmm10
	movdqa	%xmm12, %xmm2
	movdqa	%xmm10, %xmm11
	movdqa	%xmm12, %xmm13
	psrlq	$32, %xmm11
	psrlq	$32, %xmm2
	pmuludq	%xmm10, %xmm13
	pmuludq	%xmm10, %xmm2
	movdqa	%xmm11, %xmm10
	pmuludq	%xmm12, %xmm10
	paddq	%xmm10, %xmm2
	movdqa	%xmm8, %xmm10
	psllq	$32, %xmm2
	punpckhdq	%xmm1, %xmm10
	paddq	%xmm13, %xmm2
	movdqa	%xmm8, %xmm13
	movdqa	%xmm10, %xmm12
	punpckldq	%xmm1, %xmm13
	psrlq	$32, %xmm12
	movdqa	%xmm13, %xmm8
	movdqa	%xmm13, %xmm11
	psrlq	$32, %xmm8
	pmuludq	%xmm10, %xmm11
	pmuludq	%xmm10, %xmm8
	movdqa	%xmm12, %xmm10
	movdqa	%xmm2, %xmm12
	pmuludq	%xmm13, %xmm10
	paddq	%xmm10, %xmm8
	psllq	$32, %xmm8
	paddq	%xmm8, %xmm11
	movdqa	%xmm2, %xmm8
	pmuludq	%xmm11, %xmm12
	psrlq	$32, %xmm8
	movdqa	%xmm11, %xmm10
	psrlq	$32, %xmm11
	pmuludq	%xmm10, %xmm8
	pmuludq	%xmm11, %xmm2
	paddq	%xmm2, %xmm8
	movdqa	%xmm3, %xmm2
	psllq	$32, %xmm8
	punpckhdq	%xmm1, %xmm2
	paddq	%xmm12, %xmm8
	movdqa	%xmm3, %xmm12
	movdqa	%xmm2, %xmm11
	punpckldq	%xmm1, %xmm12
	psrlq	$32, %xmm11
	movdqa	%xmm12, %xmm3
	movdqa	%xmm12, %xmm10
	psrlq	$32, %xmm3
	pmuludq	%xmm2, %xmm10
	pmuludq	%xmm2, %xmm3
	movdqa	%xmm11, %xmm2
	pmuludq	%xmm12, %xmm2
	movdqa	%xmm0, %xmm12
	punpckhdq	%xmm1, %xmm0
	punpckldq	%xmm1, %xmm12
	movdqa	%xmm12, %xmm11
	psrlq	$32, %xmm11
	paddq	%xmm2, %xmm3
	psllq	$32, %xmm3
	paddq	%xmm3, %xmm10
	movdqa	%xmm10, %xmm3
	movdqa	%xmm10, %xmm2
	psrlq	$32, %xmm3
	pmuludq	%xmm11, %xmm10
	movdqa	%xmm8, %xmm11
	pmuludq	%xmm12, %xmm3
	pmuludq	%xmm12, %xmm2
	paddq	%xmm10, %xmm3
	psllq	$32, %xmm3
	paddq	%xmm3, %xmm2
	movdqa	%xmm8, %xmm3
	psrlq	$32, %xmm3
	pmuludq	%xmm2, %xmm11
	movdqa	%xmm2, %xmm10
	psrlq	$32, %xmm10
	pmuludq	%xmm2, %xmm3
	movdqa	%xmm0, %xmm2
	pmuludq	%xmm10, %xmm8
	psrlq	$32, %xmm2
	movdqa	%xmm9, %xmm10
	psrlq	$32, %xmm10
	pmuludq	%xmm9, %xmm2
	paddq	%xmm8, %xmm3
	movdqa	%xmm0, %xmm8
	pmuludq	%xmm10, %xmm0
	pmuludq	%xmm9, %xmm8
	psllq	$32, %xmm3
	paddq	%xmm11, %xmm3
	paddq	%xmm0, %xmm2
	movdqa	%xmm3, %xmm0
	psllq	$32, %xmm2
	psrlq	$32, %xmm0
	paddq	%xmm2, %xmm8
	movdqa	%xmm3, %xmm2
	pmuludq	%xmm8, %xmm0
	pmuludq	%xmm8, %xmm2
	movdqa	%xmm8, %xmm10
	psrlq	$32, %xmm10
	pmuludq	%xmm10, %xmm3
	paddq	%xmm3, %xmm0
	psllq	$32, %xmm0
	paddq	%xmm0, %xmm2
	movdqa	%xmm2, %xmm9
	cmpl	%edx, %eax
	jne	.L4
	psrldq	$8, %xmm2
	movdqa	%xmm9, %xmm1
	movl	%ecx, %edx
	movl	%ebx, %ebp
	movdqa	%xmm2, %xmm0
	psrlq	$32, %xmm1
	movdqa	%xmm9, %xmm3
	andl	$-16, %edx
	psrlq	$32, %xmm0
	pmuludq	%xmm2, %xmm1
	subl	%edx, %ebp
	pmuludq	%xmm9, %xmm0
	pmuludq	%xmm2, %xmm3
	paddq	%xmm0, %xmm1
	psllq	$32, %xmm1
	paddq	%xmm3, %xmm1
	movq	%xmm1, %rax
	cmpb	%dl, %cl
	je	.L1
.L3:
	movzbl	%bpl, %edx
	leal	-1(%rbp), %ecx
	imulq	%rdx, %rax
	cmpb	$1, %cl
	je	.L1
	movzbl	%cl, %ecx
	leal	-2(%rbp), %esi
	imulq	%rcx, %rax
	cmpb	$1, %sil
	je	.L1
	movzbl	%sil, %esi
	leal	-3(%rbp), %edx
	imulq	%rsi, %rax
	cmpb	$1, %dl
	je	.L1
	movzbl	%dl, %edx
	leal	-4(%rbp), %ecx
	imulq	%rdx, %rax
	cmpb	$1, %cl
	je	.L1
	movzbl	%cl, %ecx
	leal	-5(%rbp), %edx
	imulq	%rcx, %rax
	cmpb	$1, %dl
	je	.L1
	movzbl	%dl, %edx
	leal	-6(%rbp), %ecx
	imulq	%rdx, %rax
	cmpb	$1, %cl
	je	.L1
	movzbl	%cl, %ecx
	leal	-7(%rbp), %edx
	imulq	%rcx, %rax
	cmpb	$1, %dl
	je	.L1
	movzbl	%dl, %edx
	leal	-8(%rbp), %ecx
	imulq	%rdx, %rax
	cmpb	$1, %cl
	je	.L1
	movzbl	%cl, %ecx
	leal	-9(%rbp), %edx
	imulq	%rcx, %rax
	cmpb	$1, %dl
	je	.L1
	movzbl	%dl, %edx
	leal	-10(%rbp), %ecx
	imulq	%rdx, %rax
	cmpb	$1, %cl
	je	.L1
	movzbl	%cl, %ecx
	leal	-11(%rbp), %edx
	imulq	%rcx, %rax
	cmpb	$1, %dl
	je	.L1
	movzbl	%dl, %edx
	leal	-12(%rbp), %ecx
	imulq	%rdx, %rax
	cmpb	$1, %cl
	je	.L1
	movzbl	%cl, %ecx
	leal	-13(%rbp), %edx
	imulq	%rcx, %rax
	cmpb	$1, %dl
	je	.L1
	movzbl	%dl, %edx
	movq	%rdx, %rcx
	leal	-14(%rbp), %edx
	imulq	%rax, %rcx
	movzbl	%dl, %eax
	imulq	%rcx, %rax
.L1:
	addq	$24, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
	.p2align 4,,10
	.p2align 3
.L7:
	.cfi_restore_state
	addq	$24, %rsp
	.cfi_remember_state
	.cfi_def_cfa_offset 24
	movl	$1, %eax
	popq	%rbx
	.cfi_def_cfa_offset 16
	popq	%rbp
	.cfi_def_cfa_offset 8
	ret
.L8:
	.cfi_restore_state
	movl	$1, %eax
	jmp	.L3
	.cfi_endproc
.LFE11:
	.size	fact, .-fact
	.section	.rodata.str1.1
.LC4:
	.string	"6! = %llu\n"
	.section	.text.startup,"ax",@progbits
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB12:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	movl	$2, %esi
	leaq	.LC1(%rip), %rdi
	xorl	%eax, %eax
	call	printf@PLT
	movl	$720, %esi
	leaq	.LC4(%rip), %rdi
	xorl	%eax, %eax
	call	printf@PLT
	xorl	%eax, %eax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE12:
	.size	main, .-main
	.section	.rodata.cst16,"aM",@progbits,16
	.align 16
.LC0:
	.quad	1
	.quad	1
	.align 16
.LC2:
	.byte	0
	.byte	-1
	.byte	-2
	.byte	-3
	.byte	-4
	.byte	-5
	.byte	-6
	.byte	-7
	.byte	-8
	.byte	-9
	.byte	-10
	.byte	-11
	.byte	-12
	.byte	-13
	.byte	-14
	.byte	-15
	.align 16
.LC3:
	.byte	-16
	.byte	-16
	.byte	-16
	.byte	-16
	.byte	-16
	.byte	-16
	.byte	-16
	.byte	-16
	.byte	-16
	.byte	-16
	.byte	-16
	.byte	-16
	.byte	-16
	.byte	-16
	.byte	-16
	.byte	-16
	.ident	"GCC: (GNU) 8.2.1 20181127"
	.section	.note.GNU-stack,"",@progbits
