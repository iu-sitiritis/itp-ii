	.file	"fact_while.c"
	.text
	.p2align 4,,15
	.globl	fact
	.type	fact, @function
fact:
.LFB11:
	.cfi_startproc
	movzbl	%dil, %edx
	movl	$1, %eax
	cmpb	$1, %dil
	jbe	.L4
	.p2align 4,,10
	.p2align 3
.L3:
	imulq	%rdx, %rax
	subq	$1, %rdx
	cmpq	$1, %rdx
	jne	.L3
	ret
	.p2align 4,,10
	.p2align 3
.L4:
	ret
	.cfi_endproc
.LFE11:
	.size	fact, .-fact
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"6! = %llu\n"
	.section	.text.startup,"ax",@progbits
	.p2align 4,,15
	.globl	main
	.type	main, @function
main:
.LFB12:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	movl	$720, %esi
	leaq	.LC0(%rip), %rdi
	xorl	%eax, %eax
	call	printf@PLT
	xorl	%eax, %eax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE12:
	.size	main, .-main
	.ident	"GCC: (GNU) 8.2.1 20181127"
	.section	.note.GNU-stack,"",@progbits
