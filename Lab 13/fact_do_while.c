#include <stdio.h>

unsigned long long int fact(unsigned char n)
{
  unsigned long long int result = 1;

  do
  {
    if (n <= 1)
    {
      break;
    }

    result *= n--;
  }
  while (n > 1);

  return result;
}

int main()
{
  printf("6! = %llu\n", fact(6));

  return 0;
}