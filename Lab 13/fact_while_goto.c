#include <stdio.h>

unsigned long long int fact(unsigned char n)
{
  unsigned long long int result = 1;

  loop:
    if (n > 1)
    {
      goto loop_body;
    }
    else
    {
      goto end_loop;
    }

    loop_body:
      result *= n--;
      goto loop;
  end_loop:

  return result;
}

int main()
{
  printf("6! = %llu\n", fact(6));

  return 0;
}
