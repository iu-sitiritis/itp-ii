	.file	"fact_while.c"
	.text
	.globl	fact
	.type	fact, @function
fact:
.LFB11:
	.cfi_startproc
	cmpb	$1, %dil
	jbe	.L4
	movzbl	%dil, %edx
	movl	$1, %eax
.L3:
	imulq	%rdx, %rax
	subq	$1, %rdx
	cmpb	$1, %dl
	ja	.L3
	ret
.L4:
	movl	$1, %eax
	ret
	.cfi_endproc
.LFE11:
	.size	fact, .-fact
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"6! = %llu\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB12:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	movl	$6, %edi
	call	fact
	movq	%rax, %rsi
	leaq	.LC0(%rip), %rdi
	movl	$0, %eax
	call	printf@PLT
	movl	$0, %eax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE12:
	.size	main, .-main
	.ident	"GCC: (GNU) 8.2.1 20181127"
	.section	.note.GNU-stack,"",@progbits
