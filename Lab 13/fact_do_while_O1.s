	.file	"fact_do_while.c"
	.text
	.globl	fact
	.type	fact, @function
fact:
.LFB11:
	.cfi_startproc
	movl	%edi, %eax
	cmpb	$1, %dil
	jbe	.L4
	subl	$1, %edi
	movzbl	%al, %eax
	cmpb	$1, %dil
	jbe	.L1
	movzbl	%dil, %edi
.L3:
	imulq	%rdi, %rax
	subq	$1, %rdi
	cmpb	$1, %dil
	ja	.L3
	ret
.L4:
	movl	$1, %eax
.L1:
	ret
	.cfi_endproc
.LFE11:
	.size	fact, .-fact
	.section	.rodata.str1.1,"aMS",@progbits,1
.LC0:
	.string	"6! = %llu\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB12:
	.cfi_startproc
	subq	$8, %rsp
	.cfi_def_cfa_offset 16
	movl	$6, %edi
	call	fact
	movq	%rax, %rsi
	leaq	.LC0(%rip), %rdi
	movl	$0, %eax
	call	printf@PLT
	movl	$0, %eax
	addq	$8, %rsp
	.cfi_def_cfa_offset 8
	ret
	.cfi_endproc
.LFE12:
	.size	main, .-main
	.ident	"GCC: (GNU) 8.2.1 20181127"
	.section	.note.GNU-stack,"",@progbits
