#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NUM_CHARS 256

int main()
{
  int charnum_map[NUM_CHARS];
  char* chars_included = malloc(sizeof(char)*NUM_CHARS);

  for (int i = 0; i < NUM_CHARS; ++i)
  {
    charnum_map[i] = 0;
  }

  char c = EOF;
  int uq_char_idx = 0, max_num = 0;
  while ((c = getchar()) != '\n')
  {
    charnum_map[c] += 1;

    if (max_num < charnum_map[c])
    {
      max_num = charnum_map[c];
    }

    if (!strchr(chars_included, c))
    {
      chars_included[uq_char_idx] = c;
      ++uq_char_idx;
      printf("%c", c);
    }
  }
  chars_included[uq_char_idx] = '\0';
  printf("\n");

  for (int i = 0; i < max_num; ++i)
  {
    for (char* j = chars_included; *j; ++j)
    {
      if (charnum_map[*j])
      {
        printf(".");
        --charnum_map[*j];
      }
      else
      {
        printf(" ");
      }
    }

    printf("\n");
  }

  return 0;
}
