#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define NUM_CHARS 256

int main()
{
  int charnum_map[NUM_CHARS];
  char* chars_included = malloc(sizeof(char)*NUM_CHARS);

  for (int i = 0; i < NUM_CHARS; ++i)
  {
    charnum_map[i] = 0;
  }

  char c = EOF;
  int uq_char_idx = 0;
  while ((c = getchar()) != '\n')
  {
    charnum_map[c] += 1;

    if (!strchr(chars_included, c))
    {
      chars_included[uq_char_idx] = c;
      ++uq_char_idx;
    }
  }
  chars_included[uq_char_idx] = '\0';

  while(*chars_included)
  {
    printf("%c ", *chars_included);

    for (int i = 0; i < charnum_map[*chars_included]; ++i)
    {
      printf(".");
    }
    printf("\n");

    ++chars_included;
  }

  return 0;
}
