#include <stdio.h>
#include <string.h>

#define NUM_CHARS 256

int main()
{
  int char_num_map[NUM_CHARS];
  int char_ord_map[NUM_CHARS];
  char chars_included[NUM_CHARS];

  for (int i = 0; i < NUM_CHARS; ++i)
  {
    char_num_map[i] = 0;
    char_ord_map[i] = 0;
  }

  char c = EOF;
  int uq_char_idx = 0;
  while ((c = getchar()) != '\n')
  {
    char_num_map[c] += 1;

    if (!strchr(chars_included, c))
    {
      chars_included[uq_char_idx] = c;
      chars_included[++uq_char_idx] = '\0';
    }

    char_ord_map[c] = 1;
    for (int i = 0; i < uq_char_idx; ++i)
    {
      if (chars_included[i] != c)
      {
        char_ord_map[chars_included[i]] += 1;
      }
    }
  }

  

  return 0;
}
