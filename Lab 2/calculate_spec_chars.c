#include <stdio.h>
#include <ctype.h>

int main()
{
  int num_chars = 0,
      num_words = 0,
      num_lines = 1;
  int in_word = 0;
  char c = EOF;

  while((c = getchar()) != EOF)
  {
    if (isalnum(c))
    {
      if (!in_word)
      {
        in_word = 1;
        ++num_words;
      }
    }
    else
    {
      in_word = 0;

      if (c == '\n')
      {
        ++num_lines;
      }
    }

    ++num_chars;
  }

  printf("Lines = %d\nWords = %d\nChars = %d\n",
    num_lines, num_words, num_chars);

  return 0;
}
