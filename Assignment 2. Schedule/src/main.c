#include <stdio.h>

#include "input/input.h"
#include "logic/schedule_optimization.h"

void print_email()
{
  FILE* email_file = fopen("TimurLyisenkoEmail.txt", "w");

  fprintf(
    email_file,
    "t.lyisenko@innopolis.university"
  );

  fclose(email_file);
}

int main()
{
  print_email();

  char consider_file = 0;

  for (unsigned char i = MAX_NUM_FILES; i > 0; --i)
  {
    char input_file_name[12];
    sprintf(input_file_name, "input%u.txt", i);
    FILE* input_file = fopen(input_file_name, "r");

    if (input_file)
    {
      consider_file = 1;
    }

    if (consider_file)
    {
      char output_file_name[12];
      sprintf(output_file_name, "output%u.txt", i);
      FILE* output_file = fopen(output_file_name, "w");

      if (input_file)
      {
        file_read_status read_status = OK;
        schedule_input* input = read_input_file(input_file, &read_status);

        if (read_status == OK)
        {
          assignment_distribution* solution =
            find_any_most_optimal_schedule(
              input
            );

          print_assignment_distribution(solution, output_file);
          print_distribution_errors(solution, input, output_file);
          fprintf(output_file, "Total score is %u.", solution->penalty);

          free_assignment_distribution(solution);
          free_schedule_input(input);
        }
        else
        {
          fprintf(output_file, "Invalid input.");
        }

        fclose(input_file);
      }
      else
      {
        fprintf(output_file, "Invalid input.");
      }

      fclose(output_file);
    }
  }

  return 0;
}
