#ifndef DLL_NODE_H
#define DLL_NODE_H

#include <stdarg.h>

/*!
 * @brief Represents a node in double linked list
 */
typedef
  struct dll_node
dll_node;

/*!
 * @brief Represents a node in double linked list
 */
struct dll_node
{
  /*!
   * @brief value Value of any type, stored within the current node
   */
  void* value;

  /*!
   * @brief left Pointer to a node on the left of the current
   */
  dll_node* left;
  dll_node* right;

  /*!
   * @brief Indicates if there was allocated memory for the value and value was
   * copied. Used to know if the value needs to be freed. Managed by the
   * functions, needs not to be modified by the user.
   */
  char value_allocated : 1;
};

/*!
 * @brief Creates a new dll_node with a memcopy of a value pointed by
 * <code>value</code>, left & right, which can be NULL
 * @param value Value stored within the created node
 * @param left Pointer to the node that is considered to be on the left from
 * the node being created
 * @param right Pointer to the node that is considered to be on the left from
 * the node being created
 * @return A newly created node with given assigned members
 */
dll_node* construct_dll_node_copy(
  void* value,
  const unsigned int type_size,
  dll_node* left,
  dll_node* right
);

/*!
 * @brief Creates a new dll_node with a memcopy of a value pointed by
 * <code>value</code>, that is not connected to
 * any other nodes
 * @param value Value stored within the created node
 * @return A newly created node with given assigned members
 */
dll_node* construct_single_dll_node_with_value_copy(
  void* value,
  const unsigned int type_size
);

/**
 * @brief Creates a new dll_node with a memcopy of a value pointed by
 * <code>value</code> & right, which can be NULL
 * @param value Value stored within the created node
 * @param right Pointer to the node that is considered to be on the left from
 * the node being created
 * @return A newly created node with given assigned members
 */
dll_node* construct_dll_node_with_value_right_copy(
  void* value,
  const unsigned int type_size,
  dll_node* right
);

/*!
 * @brief Creates a new dll_node with a memcopy of a value pointed by
 * <code>value</code> & left, which can be NULL
 * @param value Value stored within the created node
 * @param left Pointer to the node that is considered to be on the left from
 * the node being created
 * @return A newly created node with given assigned members
 */
dll_node* construct_dll_node_with_value_left_copy(
  void* value,
  const unsigned int type_size,
  dll_node* left
);

/*!
 * @brief Creates a new dll_node with a given value, left & right,
 * which can be NULL
 * @param value Value stored within the created node
 * @param left Pointer to the node that is considered to be on the left from
 * the node being created
 * @param right Pointer to the node that is considered to be on the left from
 * the node being created
 * @return A newly created node with given assigned members
 */
dll_node* construct_dll_node(
  void* value,
  dll_node* left,
  dll_node* right
);

/*!
 * @brief Creates a new dll_node with a given value, that is not connected to
 * any other nodes
 * @param value Value stored within the created node
 * @return A newly created node with given assigned members
 */
dll_node* construct_single_dll_node_with_value(
  void* value
);

/**
 * @brief Creates a new dll_node with a given value & right, which can be NULL
 * @param value Value stored within the created node
 * @param right Pointer to the node that is considered to be on the left from
 * the node being created
 * @return A newly created node with given assigned members
 */
dll_node* construct_dll_node_with_value_right(
  void* value,
  dll_node* right
);

/*!
 * @brief Creates a new dll_node with a given value & left, which can be NULL
 * @param value Value stored within the created node
 * @param left Pointer to the node that is considered to be on the left from
 * the node being created
 * @return A newly created node with given assigned members
 */
dll_node* construct_dll_node_with_value_left(
  void* value,
  dll_node* left
);


/*!
 * @brief Creates a new empty dll_node. It contains no value, left & right
 * (all are NULL-s)
 * @return A newly created empty node
 */
dll_node* construct_empty_dll_node(void);


/*!
 * @brief Frees memory allocated to a given dll_node
 * @param node_to_free Pointer to node which must be freed
 * @param destructor Function that frees memory for a value stored in the node.
 * If null, uses free function instead
 */
void free_dll_node(dll_node* node_to_free, void (*destructor)(void* value));

/*!
 * @brief Wrapper to <code>free_dll_node</code> to be used by
 * <code>dll_map_for_all_nodes</code> to release memory allocated to nodes of a
 * list.
 * @see <code>free_dll_node</code>
 * @see <code>dll_map_for_all_nodes</code>
 */
void free_dll_node_wrapper(dll_node* node_to_free, va_list args);

#endif // DLL_NODE_H
