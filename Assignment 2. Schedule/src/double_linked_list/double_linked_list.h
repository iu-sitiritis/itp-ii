#ifndef DOUBLE_LINKED_LIST_H
#define DOUBLE_LINKED_LIST_H

#include "dll_node.h"

/*!
 * @brief Structure that implements a double linked list for any type
 * and utilizes an auxiliary pointer to its node - cursor, value of which
 * changes to point to a node that was accessed the last by different
 * operations on the list.
 */
typedef
  struct double_linked_list
  {
    /*!
     * @brief Number of elements in the list.
     */
    unsigned int size;
    /*!
     * @brief Size in bytes of a type stored in the list.
     */
    unsigned int type_size;

    #define dll_head_idx (0)
    #define dll_tail_idx(LIST_PTR) \
    ((LIST_PTR->size == 0) ? 0 : (LIST_PTR->size - 1))
    /*!
     * @brief Index of the node currently pointed by the cursor
     * in the list. If there are no elements in the list - cursor_idx == 0
     */
    unsigned int cursor_idx;

    /*!
     * @brief Points to the first node in the linked list.
     */
    dll_node* head;
    /*!
     * @brief Points to the last element in the linked list.
     */
    dll_node* tail;
    /*!
     * @brief Points to the last accessed element
     * by the operations performed on the list, such as add, get, etc.
     */
    dll_node* cursor;

    /*!
     * @brief Comparator for the elements stored in the list. Can be NULL.
     * If it is NULL, then all operations which require to compare elements
     * have no effect and behave as there is nothing to compare.
     */
    const int (*comp)(const void* left, const void* right);
    /*!
     * @brief Destructor for the pointer to the stored element. NULL, by
     * default, so usual free(value) is used.
     * @param value Pointer to the value to be freed.
     */
    void (*destr)(void* value);
  }
double_linked_list;


/*!
 * @brief Constructs a new empty double linked list, initialized with given
 * comparator and destructor.
 * @param type_size Size of a type that is stored in the list.
 * @param comp Comparator for elements stored in the list.
 * @param destr Destructor for elements stored in the list.
 * @return A pointer to the newly constructed double linked list.
 */
double_linked_list* construct_double_linked_list_with_comp_destr(
  const unsigned int type_size,
  const int (*comp)(const void* left, const void* right),
  void (*destr)(void* value)
);

/*!
 * @brief Constructs a new empty double linked list, initialized with given
 * comparator.
 * @param type_size Size of a type that is stored in the list.
 * @param comp Comparator for elements stored in the list.
 * @return A pointer to the newly constructed double linked list.
 */
double_linked_list* construct_double_linked_list_with_comp(
  const unsigned int type_size,
  const int (*comp)(const void* left, const void* right)
);

/*!
 * @brief Constructs a new empty double linked list, initialized with given
 * destructor.
 * @param type_size Size of a type that is stored in the list.
 * @param destr Destructor for elements stored in the list.
 * @return A pointer to the newly constructed double linked list.
 */
double_linked_list* construct_double_linked_list_with_destr(
  const unsigned int type_size,
  void (*destr)(void* value)
);

/*!
 * @brief Constructs a new empty double linked list,
 * initialized with default values.
 * @param type_size Size of a type that is stored in the list.
 * @return A pointer to the newly constructed double linked list.
 */
double_linked_list* construct_empty_double_linked_list(
  const unsigned int type_size
);

/*!
 * @brief Copies a given double linked list and returns a pointer to the double
 * linked list where it was copied.
 * @param list_to_copy List to copy from.
 * @return Pointer to a new double linked list with copied data.
 */
double_linked_list* copy_double_linked_list(double_linked_list* list_to_copy);


/*!
 * @brief Frees memory used by a double linked list.
 * @param dll List to free.
 */
void free_double_linked_list(double_linked_list* dll);

/*!
 * @brief Frees memory used by a double linked list.
 * @param dll List to free.
 */
void free_double_linked_list_wrapper(void* dll);


/*!
 * @brief Checks if a given double linked list is empty.
 * @param this List to perform the action for.
 * @return Returns a non-zero value if list is empty, otherwise - 0
 */
static inline const int dll_is_empty(const double_linked_list* this)
{
  return (this->size == 0);
}

/*!
 * @brief Checks if a given index is valid (that is, there is
 * an element at the index in the list) for a given double linked list.
 * @param this List to perform the action for.
 * @param idx Index to perform the action for.
 * @return Non-zero value if index is valid, 0 - otherwise.
 */
static inline const int dll_is_index_valid(
  const double_linked_list* this,
  const unsigned int idx
)
{
  return (idx < this->size);
}

/*!
 * @brief Checks if a given range [idx_from, idx_to] is,
 * valid (that is all values in range [idx_from, idx_to] are valid indexes and
 * idx_from <= idx_to) for a given double linked list.
 * @param this List to perform the action for.
 * @param idx_from The left value of a given range.
 * @param idx_to The right value of a given range.
 * @return Non-zero value if range is valid, 0 - otherwise.
 */
static inline const int dll_is_range_valid(
  const double_linked_list* this,
  const unsigned int idx_from,
  const unsigned int idx_to
)
{
  return dll_is_index_valid(this, idx_from) &&
         dll_is_index_valid(this, idx_to) &&
         (idx_from <= idx_to);
}


/*!
 * @brief Adds a copy of a given element (pointed by value) at the beginning of
 * a given double linked list.
 * @param this Double linked list @ which an element is to be inserted.
 * @param value Pointer to value to be copied into the list.
 * @return Pointer to the given list.
 */
double_linked_list* dll_prepend_copy(double_linked_list* this, void* value);

/*!
 * @brief Adds a given element at the beginning of a given double linked list.
 * @param this Double linked list @ which an element is to be inserted.
 * @param value Pointer to value, which (pointer) will be added into the list.
 * @return Pointer to the given list.
 */
double_linked_list* dll_prepend_value(double_linked_list* this, void* value);

/*!
 * @brief Adds a copy of a given element (pointed by value) at the end of
 * a given double linked list.
 * @param this Double linked list @ which an element is to be inserted.
 * @param value Pointer to value to be copied into the list.
 * @return Pointer to the given list.
 */
double_linked_list* dll_append_copy(double_linked_list* this, void* value);

/*!
 * @brief Adds a given element at the beginning of a given double linked list.
 * @param this Double linked list @ which an element is to be inserted.
 * @param value Pointer to value, which (pointer) will be added into the list.
 * @return Pointer to the given list.
 */
double_linked_list* dll_append_value(double_linked_list* this, void* value);

/*!
 * @brief Adds a copy of a given element (pointed by value) at a given index, so
 * that it is guaranteed for the element to reside at the index and the previous
 * element will have its index = index + 1. If index is invalid, then no value
 * is inserted.
 * @param this Double linked list @ which an element is to be inserted.
 * @param value Value to be inserted.
 * @param idx Index on which the operation is to be performed.
 * @return Pointer to the given list. If a given index <code>idx</code> is
 * invalid, NULL is returned.
 */
double_linked_list* dll_add_copy_at_idx(
  double_linked_list* this,
  void* value,
  const unsigned int idx
);

/*!
 * @brief Adds a copy of a given element (pointed by value) at a given index, so
 * that it is guaranteed for the element to reside at the index and the previous
 * element will have its index = index + 1. If index is invalid, then no value
 * is inserted.
 * @param this Double linked list @ which an element is to be inserted.
 * @param value Value to be inserted.
 * @param idx Index on which the operation is to be performed.
 * @return Pointer to the given list. If a given index <code>idx</code> is
 * invalid, NULL is returned.
 */
double_linked_list* dll_add_value_at_idx(
  double_linked_list* this,
  void* value,
  const unsigned int idx
);


/*!
 * @brief Removes all elements from a given double linked list and sets related
 * members to appropriate values.
 * @param this List from which the element is to be removed.
 * @return Always returns a pointer to the given list.
 */
double_linked_list* dll_clear(double_linked_list* this);

/*!
 * @brief Removes the very first element from a given double linked list. If the
 * list is empty - nothing happens.
 * @param this List from which the element is to be removed.
 * @return Always returns a pointer to the given list.
 */
double_linked_list* dll_remove_first(double_linked_list* this);

/*!
 * @brief Removes the very last element from a given double linked list. If the
 * list is empty - nothing happens.
 * @param this List from which the element is to be removed.
 * @return Always returns a pointer to the given list.
 */
double_linked_list* dll_remove_last(double_linked_list* this);

/*!
 * @brief Removes an element at a given index in a given list. If the
 * list is empty or index is not valid - nothing happens.
 * @param this List from which the element is to be removed.
 * @param idx Index on which the operation is to be performed.
 * @return Always returns a pointer to the given list.
 */
double_linked_list* dll_remove_at_idx(
  double_linked_list* this,
  const unsigned int idx
);

/*!
 * @brief Removes the first element, value of which matches with a given value
 * from a given list. If the list is empty or has no elements matched with a
 * given value - nothing happens. If such value was deleted - cursor and its
 * index will be set to the node before the deleted one and its index
 * respectively if it was not the head, otherwise, sets cursor and its index to
 * the head.
 * @param this List from which the element is to be removed.
 * @param value Value first entry of which is to be removed from the list.
 * @return Always returns a pointer to the given list.
 */
double_linked_list* dll_remove_first_found(
  double_linked_list* this,
  const void* value
);

/*!
 * @brief Removes all elements from a given list, value of which match with
 * a given value. If the list is empty or has no elements matched with a
 * given value - nothing happens. If such value was deleted - cursor and its
 * index will be set to the node before the first deleted one and its index
 * respectively if it was not the head, otherwise, sets cursor and its index to
 * the head.
 * @param this List from which the element is to be removed.
 * @param value Value to match elements against.
 * @return Always returns a pointer to the given list.
 */
double_linked_list* dll_remove_all_found(
  double_linked_list* this,
  const void* value
);


/*!
 * @brief Returns a void* to the element at a given index in a given list.
 * @param this List to perform the action for.
 * @param idx Index to perform the action for.
 * @return void* to an element at index <code>idx</code>. If <code>idx</code>
 * is not valid - returns NULL.
 */
void* dll_get_at(double_linked_list* this, const unsigned int idx);

/*!
 * @brief Traverses a given double linked list, until it finds an element with
 * value <code>value</code>. If such value was found - cursor and its index
 * will be set to the found node containing the element and its index
 * respectively.
 * @param this List to perform the action for.
 * @param value Value to search for.
 * @return Value stored in the list equal to the given. If no such element was
 * found or comp is NULL - returns NULL.
 */
void* dll_find_first(double_linked_list* this, const void* value);


/*!
 * @brief Applies a given function to value of each element of a given list.
 * @param this List to perform the action for.
 * @param func_wrapper Function to be applied to each element. The function is a
 * wrapper to the original one, taking its arguments packed in
 * <code>va_list</code>. Inside, the wrapper should unpack the list and pass
 * each unpacked argument to the function it wraps function.
 * @param ... Other arguments that will be passed to a given function. These
 * arguments are forwarded directly to a given function without being copied. If
 * their number does not match with one required by <code>func_wrapper</code>,
 * the behavior is undefined.
 */
void dll_map_for_all(
  double_linked_list* this,
  void (*const func_wrapper)(void* this, va_list args),
  ...
);

#endif // DOUBLE_LINKED_LIST_H
