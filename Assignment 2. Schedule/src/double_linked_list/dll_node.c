#include "dll_node.h"

#include <memory.h>
#include <stdlib.h>

dll_node* construct_dll_node_copy(
  void* value,
  const unsigned int type_size,
  dll_node* left,
  dll_node* right
)
{
  dll_node* new_dll_node = malloc(sizeof(dll_node));
  new_dll_node->value = malloc(type_size);

  memcpy(new_dll_node->value, value, type_size);
  new_dll_node->left = left;
  new_dll_node->right = right;
  new_dll_node->value_allocated = 1;

  return new_dll_node;
}

dll_node* construct_single_dll_node_with_value_copy(
  void* value,
  const unsigned int type_size
)
{
  return construct_dll_node_copy(value, type_size, NULL, NULL);
}

dll_node* construct_dll_node_with_value_right_copy(
  void* value,
  const unsigned int type_size,
  dll_node* right
)
{
  return construct_dll_node_copy(value, type_size, NULL, right);
}

dll_node* construct_dll_node_with_value_left_copy(
  void* value,
  const unsigned int type_size,
  dll_node* left
)
{
  return construct_dll_node_copy(value, type_size, left, NULL);
}

dll_node* construct_dll_node(
  void* value,
  dll_node* left,
  dll_node* right
)
{
  dll_node* new_dll_node = malloc(sizeof(dll_node));

  new_dll_node->value = value;
  new_dll_node->left = left;
  new_dll_node->right = right;
  new_dll_node->value_allocated = 0;

  return new_dll_node;
}

dll_node* construct_single_dll_node_with_value(
  void* value
)
{
  construct_dll_node(value, NULL, NULL);
}

dll_node* construct_dll_node_with_value_right(
  void* value,
  dll_node* right
)
{
  construct_dll_node(value, NULL, right);
}

dll_node* construct_dll_node_with_value_left(
  void* value,
  dll_node* left
)
{
  construct_dll_node(value, left, NULL);
}

dll_node* construct_empty_dll_node(void)
{
  construct_dll_node(NULL, NULL, NULL);
}


void free_dll_node(
  dll_node* node_to_free,
  void (*destructor)(void* value)
)
{
  if (destructor)
  {
    destructor(node_to_free->value);
  }
  else if (node_to_free->value_allocated)
  {
    free(node_to_free->value);
  }

  free(node_to_free);
}

void free_dll_node_wrapper(dll_node* node_to_free, va_list args)
{
  void (*destructor)(void* value) = va_arg(
    args,
    void (*)(void* value)
  );

  free_dll_node(node_to_free, destructor);
}
