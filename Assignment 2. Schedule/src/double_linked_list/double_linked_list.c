#include "double_linked_list.h"

#include <stdlib.h>
#include <math.h>

/*!
 * @brief Works the same as <code>dll_map_for_all</code> except that the
 * <code>func_wrapper</code> is applied to nodes of a given list.
 * @see <code>dll_map_for_all</code>
 */
static void dll_map_for_all_nodes(
  double_linked_list* this,
  void (*const func_wrapper)(dll_node* this, va_list args),
  ...
)
{
  dll_node* cur_node = this->head;

  while (cur_node)
  {
    dll_node* right_node = cur_node->right;

    va_list args;
    va_start(args, func_wrapper);

    func_wrapper(cur_node, args);

    va_end(args);

    cur_node = right_node;
  }
}


double_linked_list* construct_double_linked_list_with_comp_destr(
  const unsigned int type_size,
  const int (*comp)(const void* left, const void* right),
  void (*destr)(void* value)
)
{
  double_linked_list* result = malloc(sizeof(double_linked_list));

  result->size = 0;
  result->type_size = type_size;
  result->head = NULL;
  result->tail = NULL;
  result->cursor = NULL;
  result->cursor_idx = 0;
  result->comp = comp;
  result->destr = destr;

  return result;
}

double_linked_list* construct_double_linked_list_with_comp(
  unsigned int type_size,
  const int (*comp)(const void* left, const void* right)
)
{
  return construct_double_linked_list_with_comp_destr(type_size, comp, NULL);
}

double_linked_list* construct_double_linked_list_with_destr(
  const unsigned int type_size,
  void (*destr)(void* value)
)
{
  return construct_double_linked_list_with_comp_destr(type_size, NULL, destr);
}

double_linked_list* construct_empty_double_linked_list(
  const unsigned int type_size
)
{
  return construct_double_linked_list_with_comp_destr(type_size, NULL, NULL);
}

/*!
 * @brief Auxiliary function used to perform a copying of a list.
 * @param value Value to append to a given list.
 * @param list Double linked list to copy to.
 * @see <code>dll_append</code>
 * @see <code>copy_double_linked_list</code>
 */
static void dll_append_reversed_arg_wrapper(
  dll_node* node_to_copy,
  va_list list
)
{
  double_linked_list* list_to_copy_to = va_arg(list, double_linked_list*);

  if (node_to_copy->value_allocated)
  {
    dll_append_copy(list_to_copy_to, node_to_copy->value);
  }
  else
  {
    dll_append_value(list_to_copy_to, node_to_copy->value);
  }
}

double_linked_list* copy_double_linked_list(double_linked_list* list_to_copy)
{
  double_linked_list* result = construct_double_linked_list_with_comp_destr(
    list_to_copy->type_size,
    list_to_copy->comp,
    list_to_copy->destr
  );

  // copy value from list_to_copy to result
  dll_map_for_all_nodes(
    list_to_copy,
    dll_append_reversed_arg_wrapper,
    result
  );

  return result;
}


void free_double_linked_list(double_linked_list* dll)
{
  dll_map_for_all_nodes(dll, free_dll_node_wrapper, dll->destr);
  free(dll);
}

void free_double_linked_list_wrapper(void* dll)
{
  free_double_linked_list(dll);
}


/*!
 * @brief Traverses a list starting at <code>from_node</code> which has index
 * <code>from_node_idx</code> in the right direction until index
 * <code>to_node_idx</code> is reached. If range [<code>from_node_idx</code>,
 * <code>to_node_idx</code>] is invalid - returns NULL. Sets cursor to the
 * node at <code>to_node_idx</code> in the list.
 * @param this List to perform the action for.
 * @param from_node Node at which traversal is started.
 * @param from_node_idx Index of <code>from_node</code> in <code>this</code>.
 * @param to_node_idx Index at which traversal stops.
 * @return Node at index <code>to_node_idx</code>. If a given range is invalid -
 * returns NULL.
 */
static dll_node* dll_traverse_right_from_to(
  double_linked_list* this,
  dll_node* from_node,
  unsigned int from_node_idx,
  unsigned int to_node_idx
)
{
  dll_node* result = NULL;

  if (dll_is_range_valid(this, from_node_idx, to_node_idx))
  {
    while (from_node_idx < to_node_idx)
    {
      from_node = from_node->right;
      ++from_node_idx;
    }

    result = (this->cursor = from_node);
    this->cursor_idx = from_node_idx;
  }

  return result;
}

/*!
 * @brief Traverses a list starting at <code>from_node</code> which has index
 * <code>from_node_idx</code> in the left direction until index
 * <code>to_node_idx</code> is reached. If range [<code>to_node_idx</code>,
 * <code>from_node_idx</code>] is invalid - returns NULL. Sets cursor to the
 * node at <code>to_node_idx</code> in the list.
 * @param this List to perform the action for.
 * @param from_node Node at which traversal is started.
 * @param from_node_idx Index of <code>from_node</code> in <code>this</code>.
 * @param to_node_idx Index at which traversal stops.
 * @return Node at index <code>to_node_idx</code>. If a given range is invalid -
 * returns NULL.
 */
static dll_node* dll_traverse_left_from_to(
  double_linked_list* this,
  dll_node* from_node,
  unsigned int from_node_idx,
  unsigned int to_node_idx
)
{
  dll_node* result = NULL;

  if (dll_is_range_valid(this, to_node_idx, from_node_idx))
  {
    while (from_node_idx > to_node_idx)
    {
      from_node = from_node->left;
      --from_node_idx;
    }

    result = (this->cursor = from_node);
    this->cursor_idx = from_node_idx;
  }

  return result;
}

/*!
 * @brief Traverses a list starting from a given node, which has a given index
 * in the list until it reaches the target index. Depending on the
 * <code>from_node_idx</code> and <code>to_node_idx</code> the appropriate
 * traversal is performed (left to right or right to left).
 * @param this List to perform the action for.
 * @param from_node Node at which traversal is started.
 * @param from_node_idx Index of <code>from_node</code> in <code>this</code>.
 * @param to_node_idx Index at which traversal stops.
 * @return Node at index <code>to_node_idx</code>. If a given range is invalid -
 * returns NULL.
 */
static dll_node* dll_traverse_from_to(
  double_linked_list* this,
  dll_node* from_node,
  const unsigned int from_node_idx,
  const unsigned int to_node_idx
)
{
  dll_node* result = NULL;

  if (dll_is_range_valid(this, from_node_idx, to_node_idx))
  {
    result = dll_traverse_right_from_to(
      this,
      from_node,
      from_node_idx,
      to_node_idx
    );
  }
  else if (dll_is_range_valid(this, to_node_idx, from_node_idx))
  {
    result = dll_traverse_left_from_to(
      this,
      from_node,
      from_node_idx,
      to_node_idx
    );
  }

  return result;
}

/*!
 * @brief Works the same as <code>dll_get_at</code>, except that it returns
 * a node itself.
 * @return Node at index <code>idx</code>. If <code>idx</code> is not valid -
 * returns NULL.
 * @see <code>dll_get_at</code>
 */
static dll_node* dll_get_node_at(
  double_linked_list* this,
  const unsigned int idx
)
{
  dll_node* result_node = NULL;

  if (dll_is_index_valid(this, idx))
  {
    const unsigned int dist_from_head = idx;
    const unsigned int dist_from_cursor = labs(
      (long)(this->cursor_idx) - idx
    );
    const unsigned int dist_from_tail = dll_tail_idx(this) - idx;

    if (
      (dist_from_cursor <= dist_from_head) &&
      (dist_from_cursor <= dist_from_tail)
    )
    {
      result_node = dll_traverse_from_to(
        this,
        this->cursor,
        this->cursor_idx,
        idx
      );
    }
    else if (dist_from_head <= dist_from_tail)
    {
      result_node = dll_traverse_from_to(this, this->head, dll_head_idx, idx);
    }
    else
    {
      result_node = dll_traverse_from_to(
        this,
        this->tail,
        dll_tail_idx(this),
        idx
      );
    }
  }

  return result_node;
}

/*!
 * @brief Traverses a given double linked list from the beginning and returns
 * a node, such that its value is equal to the given, i. e.
 * comp(node->value, value) == 0. If such node was found - cursor and its index
 * will be set to the found element and its index respectively.
 * @param this List to perform the action for.
 * @param value Value to compare against.
 * @return Node with value stored equal to the given. If no such node was found
 * or comp is NULL - returns NULL.
 */
static dll_node* dll_find_first_node(
  double_linked_list* this,
  const void* value
)
{
  dll_node* cur_node = NULL;

  if (this->comp)
  {
    cur_node = this->head;
    unsigned int cur_node_idx = dll_head_idx;

    while (cur_node)
    {
      if ((this->comp(cur_node->value, value)) == 0)
      {
        this->cursor = cur_node;
        this->cursor_idx = cur_node_idx;
        break;
      }

      cur_node = cur_node->right;
      ++cur_node_idx;
    }
  }

  return cur_node;
}


double_linked_list* dll_prepend_copy(double_linked_list* this, void* value)
{
  this->cursor = construct_dll_node_with_value_right_copy(
    value,
    this->type_size,
    this->head
  );
  this->cursor_idx = dll_head_idx;

  if (dll_is_empty(this))
  {
    this->tail = this->cursor;
  }
  else
  {
    this->head->left = this->cursor;
  }

  this->head = this->cursor;
  ++(this->size);

  return this;
}

double_linked_list* dll_prepend_value(double_linked_list* this, void* value)
{
  this->cursor = construct_dll_node_with_value_right(
    value,
    this->head
  );
  this->cursor_idx = dll_head_idx;

  if (dll_is_empty(this))
  {
    this->tail = this->cursor;
  }
  else
  {
    this->head->left = this->cursor;
  }

  this->head = this->cursor;
  ++(this->size);

  return this;
}

double_linked_list* dll_append_copy(double_linked_list* this, void* value)
{
  this->cursor = construct_dll_node_with_value_left_copy(
    value,
    this->type_size,
    this->tail
  );
  this->cursor_idx = dll_tail_idx(this) + 1;

  if (dll_is_empty(this))
  {
    this->head = this->cursor;
  }
  else
  {
    this->tail->right = this->cursor;
  }

  this->tail = this->cursor;
  ++(this->size);

  return this;
}

double_linked_list* dll_append_value(double_linked_list* this, void* value)
{
  this->cursor = construct_dll_node_with_value_left(
    value,
    this->tail
  );
  this->cursor_idx = dll_tail_idx(this) + 1;

  if (dll_is_empty(this))
  {
    this->head = this->cursor;
  }
  else
  {
    this->tail->right = this->cursor;
  }

  this->tail = this->cursor;
  ++(this->size);

  return this;
}

double_linked_list* dll_add_copy_at_idx(
  double_linked_list* this,
  void* value,
  const unsigned int idx
)
{
  double_linked_list* result = NULL;
  dll_node* node_at_idx = dll_get_node_at(this, idx);

  if (node_at_idx)
  {
    if (idx == dll_head_idx)
    {
      dll_prepend_copy(this, value);
    }
    else if (idx == dll_tail_idx(this))
    {
      dll_append_copy(this, value);
    }
    else
    {
      dll_node* prev_node = node_at_idx->left;

      this->cursor = construct_dll_node_copy(
        value,
        this->type_size,
        prev_node,
        node_at_idx
      );
      // this->cursor_idx already has a correct index

      node_at_idx->left = (prev_node->right = this->cursor);
      ++(this->size);
    }

    result = this;
  }

  return result;
}

double_linked_list* dll_add_value_at_idx(
  double_linked_list* this,
  void* value,
  const unsigned int idx
)
{
  double_linked_list* result = NULL;
  dll_node* node_at_idx = dll_get_node_at(this, idx);

  if (node_at_idx)
  {
    if (idx == dll_head_idx)
    {
      dll_prepend_value(this, value);
    }
    else if (idx == dll_tail_idx(this))
    {
      dll_append_value(this, value);
    }
    else
    {
      dll_node* prev_node = node_at_idx->left;

      this->cursor = construct_dll_node(
        value,
        prev_node,
        node_at_idx
      );
      // this->cursor_idx already has a correct index

      node_at_idx->left = (prev_node->right = this->cursor);
      ++(this->size);
    }

    result = this;
  }

  return result;
}


double_linked_list* dll_clear(double_linked_list* this)
{
  dll_map_for_all_nodes(this, free_dll_node_wrapper, this->destr);
  this->size = this->cursor_idx = 0;
  this->cursor = this->head = this->tail = NULL;

  return this;
}

double_linked_list* dll_remove_first(double_linked_list* this)
{
  if (this->size == 1)
  {
    dll_clear(this);
  }
  else
  {
    dll_node* node_to_remove = this->head;

    this->head = (this->cursor = this->head->right);
    this->head->left = NULL;
    if (this->tail == this->head)
    {
      this->tail->left = NULL;
    }
    this->cursor_idx = dll_head_idx;

    --(this->size);

    free_dll_node(node_to_remove, this->destr);
  }

  return this;
}

double_linked_list* dll_remove_last(double_linked_list* this)
{
  if (this->size == 1)
  {
    dll_clear(this);
  }
  else
  {
    dll_node* node_to_remove = this->tail;

    this->tail = (this->cursor = this->tail->left);
    this->tail->right = NULL;
    if (this->head == this->tail)
    {
      this->head->right = NULL;
    }
    --(this->size);
    this->cursor_idx = dll_tail_idx(this);

    free_dll_node(node_to_remove, this->destr);
  }

  return this;
}

double_linked_list* dll_remove_at_idx(
  double_linked_list* this,
  const unsigned int idx
)
{
    if (idx == dll_head_idx)
    {
      dll_remove_first(this);
    }
    else if (idx == dll_tail_idx(this))
    {
      dll_remove_last(this);
    }
    else
    {
      dll_node* node_to_remove = dll_get_node_at(this, idx);

      if (node_to_remove)
      {
        this->cursor = (node_to_remove->left->right = node_to_remove->right);
        node_to_remove->right->left = node_to_remove->left;
        --(this->size);

        free_dll_node(node_to_remove, this->destr);
      }
    }

  return this;
}

double_linked_list* dll_remove_first_found(
  double_linked_list* this,
  const void* value
)
{
  if (dll_find_first_node(this, value))
  {
    if (this->cursor == this->head)
    {
      dll_remove_first(this);
    }
    else if (this->cursor == this->tail)
    {
      dll_remove_last(this);
    }
    else
    {
      this->cursor->left->right = this->cursor->right;
      dll_node* new_cursor = (this->cursor->right->left = this->cursor->left);
      free_dll_node(this->cursor, this->destr);
      this->cursor = new_cursor;
      --(this->cursor_idx);
      --(this->size);
    }
  }

  return this;
}

double_linked_list* dll_remove_all_found(
  double_linked_list* this,
  const void* value
)
{
  if (this->comp)
  {
    dll_remove_first_found(this, value);

    dll_node* cur_node = this->cursor;

    while (cur_node == this->head)
    {
      dll_remove_first_found(this, value);
      cur_node = this->cursor;
    }

    cur_node = cur_node->right;

    while (cur_node)
    {
      dll_node* right = cur_node->right;

      if (this->comp(cur_node->value, value) == 0)
      {
        if (cur_node == this->tail)
        {
          dll_remove_last(this);
        }
        else
        {
          cur_node->left->right = right;
          right->left = cur_node->left;
          free_dll_node(cur_node, this->destr);
          --(this->size);
        }
      }

      cur_node = right;
    }
  }

  return this;
}


void* dll_get_at(double_linked_list* this, const unsigned int idx)
{
  dll_node* result_node = dll_get_node_at(this, idx);

  return (result_node) ? result_node->value : NULL;
}

void* dll_find_first(double_linked_list* this, const void* value)
{
  return (dll_find_first_node(this, value)) ? this->cursor->value : NULL;
}


void dll_map_for_all(
  double_linked_list* this,
  void (*const func_wrapper)(void* this, va_list args),
  ...
)
{
  dll_node* cur_node = this->head;

  while (cur_node)
  {
    dll_node* right_node = cur_node->right;

    va_list args;
    va_start(args, func_wrapper);

    func_wrapper(cur_node->value, args);

    va_end(args);

    cur_node = right_node;
  }
}
