#ifndef ASSIGNMENT_2_SCHEDULE_INPUT_H
#define ASSIGNMENT_2_SCHEDULE_INPUT_H

#include <stdio.h>
#include <string.h>
#include <math.h>

#include "schedule_input.h"
#include "str_utils.h"
#include "../common/common.h"
#include "../model/model_include.h"


#define MAX_NUM_FILES 50
#define MIN_NUM_FILES 1
// With '\0', so maximum number of digits is 2
#define FILE_NUM_STR_LEN 3
// With '\0', so max file name length is 11
#define MAX_FILENAME_STR_LEN 12
#define INPUT_FILE_PREFIX input
#define INPUT_FILE_EXT .txt
// With '\0'
#define MAX_LINE_SIZE 1024


typedef
  enum file_read_status
  {
    /*!
     * @brief File read successfully.
     */
    OK = 0,
    /*!
     * @brief File has an invalid structure.
     */
    INVALID_STRUCTURE,
    /*!
     * @brief Indicates that file not exists.
     */
    FILE_NOT_EXISTS
  }
file_read_status;

static inline char is_invalid_name(const char* str)
{
  return !is_word_of_letters(str)
      || (strcmp(str, STR_VAL(PROFESSOR_DELIM_TOK)) == 0)
      || (strcmp(str, STR_VAL(STUDENT_DELIM_TOK)) == 0)
      || (strcmp(str, STR_VAL(TA_DELIM_TOK)) == 0)
  ;
}

double_linked_list* read_courses_from_input_file(
  FILE* file_ptr,
  file_read_status* restrict read_status
)
{
  double_linked_list* courses = construct_double_linked_list_with_comp_destr(
    sizeof(course),
    course_string_comparator_wrapper,
    free_course_wrapper
  );
  char* cur_read_seq = NULL;
  size_t cur_read_seq_size = 0;

  while (1)
  {
    char* tok = NULL;
    char* course_name = NULL;
    long req_labs;
    long max_students;

    // read current line
    getline(&cur_read_seq, &cur_read_seq_size, file_ptr);

    if (
      feof(file_ptr) ||
      // try to read a course name
      (tok = strtok(cur_read_seq, " \n")) == NULL ||
      !is_word_of_letters(tok) ||
      strcmp(tok, STR_VAL(TA_DELIM_TOK)) == 0 ||
      strcmp(tok, STR_VAL(STUDENT_DELIM_TOK)) == 0 ||
      dll_find_first(courses, tok)
    )
    {
      free_double_linked_list(courses);
      courses = NULL;
      *read_status = INVALID_STRUCTURE;
      break;
    }
    else
    {
      if (strcmp(tok, STR_VAL(PROFESSOR_DELIM_TOK)) == 0)
      {
        if (strtok(NULL, " \n"))
        {
          free_double_linked_list(courses);
          courses = NULL;
          *read_status = INVALID_STRUCTURE;
        }
        else
        {
          *read_status = OK;
        }

        break;
      }
      else
      // the currently read sequence is a valid course name
      {
        // copy currently read sequence to course_name
        course_name = calloc(sizeof(char), strlen(tok) + 1);
        strcpy(course_name, tok);

        // try to read a required number of labs
        tok = strtok(NULL, " ");

        if (
          tok == NULL ||
          !is_word_of_digits_without_lead_zero(tok)
        )
        {
          free(course_name);
          free_double_linked_list(courses);
          courses = NULL;
          *read_status = INVALID_STRUCTURE;
          break;
        }
        else
        // read sequence is indeed some unsigned integer without leading zeros
        {
          req_labs = strtol(tok, NULL, 10);

          // try to read a number of students
          tok = strtok(NULL, "\n");

          if (
            tok == NULL ||
            !is_word_of_digits_without_lead_zero(tok)
          )
          {
            free(course_name);
            free_double_linked_list(courses);
            courses = NULL;
            *read_status = INVALID_STRUCTURE;
            break;
          }
          else
          // read sequence is indeed some unsigned integer without leading zeros
          {
            max_students = strtol(tok, NULL, 10);

            if (strtok(NULL, " \n"))
            {
              free(course_name);
              free_double_linked_list(courses);
              courses = NULL;
              *read_status = INVALID_STRUCTURE;
              break;
            }

            dll_append_value(
              courses,
              construct_course(
                course_name,
                req_labs,
                max_students
              )
            );

            free(course_name);
          }
        }
      }
    }
  }

  free(cur_read_seq);
  return courses;
}

double_linked_list* read_list_of_courses(
  char* courses_str,
  double_linked_list* available_courses,
  file_read_status* restrict read_status
)
{
  double_linked_list* courses = construct_double_linked_list_with_comp(
    sizeof(course),
    course_string_comparator_wrapper
  );

  char* course_name = strtok(courses_str, " \n");

  if (course_name)
  {
    while (course_name)
    {
      if (is_invalid_name(course_name))
      {
        free_double_linked_list(courses);
        courses = NULL;
        *read_status = INVALID_STRUCTURE;
        break;
      }
      else
      // course has a valid name and is available in list of all courses
      {
        course* cur_course = dll_find_first(available_courses, course_name);

        if (
          cur_course &&
          !dll_find_first(courses, cur_course)
        )
        {
          dll_append_value(courses, cur_course);
          *read_status = OK;
        }
        else
        {
          free_double_linked_list(courses);
          courses = NULL;
          *read_status = INVALID_STRUCTURE;
          break;
        }
      }

      course_name = strtok(NULL, " \n");
    }
  }
  else
  // no courses on the line; invalid structure in terms of task and
  // implementation
  {
    free_double_linked_list(courses);
    courses = NULL;
    *read_status = INVALID_STRUCTURE;
  }

  return courses;
}

double_linked_list* read_professors_from_input_file(
  FILE* file_ptr,
  double_linked_list* available_courses,
  file_read_status* restrict read_status
)
{
  double_linked_list* professors = construct_double_linked_list_with_comp_destr(
    sizeof(professor),
    professor_name_comparator_wrapper,
    free_professor_wrapper
  );
  char* cur_read_seq = NULL;
  size_t cur_read_seq_size = 0;

  while (1)
  {
    char* tok = NULL;
    being_name professor_name;

    // try to read a professor's first name
    getdelim(&cur_read_seq, &cur_read_seq_size, '\n', file_ptr);

    if (
      feof(file_ptr) ||
      (tok = strtok(cur_read_seq, " \n")) == NULL ||
      !is_word_of_letters(tok) ||
      strcmp(tok, STR_VAL(PROFESSOR_DELIM_TOK)) == 0 ||
      strcmp(tok, STR_VAL(STUDENT_DELIM_TOK)) == 0
    )
    {
      free_double_linked_list(professors);
      professors = NULL;
      *read_status = INVALID_STRUCTURE;
      break;
    }
    else
    {
      if (strcmp(tok, STR_VAL(TA_DELIM_TOK)) == 0)
      // Is it a TA token?
      {
        if (strtok(NULL, " \n"))
        {
          free_double_linked_list(professors);
          professors = NULL;
          *read_status = INVALID_STRUCTURE;
        }
        else
        {
          *read_status = OK;
        }

        break;
      }
      else
      // the currently read sequence is a valid professor's first name
      {
        // copy currently read sequence to professor_name->first_name
        strncpy(
          professor_name.first_name,
          tok,
          FIRST_NAME_SIZE - 1
        );
        professor_name.first_name[FIRST_NAME_SIZE - 1] = '\0';

        // try to read professor's last name
        tok = strtok(NULL, " ");

        if (tok == NULL)
        {
          free_double_linked_list(professors);
          professors = NULL;
          *read_status = INVALID_STRUCTURE;
          break;
        }
        else
        {
          // copy currently read sequence to professor_name->last_name
          strncpy(
            professor_name.last_name,
            tok,
            LAST_NAME_SIZE - 1
          );
          professor_name.last_name[LAST_NAME_SIZE - 1] = '\0';

          if (
            is_invalid_name(tok) ||
            dll_find_first(professors, &professor_name)
          )
          {
            free_double_linked_list(professors);
            professors = NULL;
            *read_status = INVALID_STRUCTURE;
            break;
          }
          else
          // A new professor's first and last names are read and both are valid;
          // Try to read courses
          {
            tok = strtok(NULL, "\n");

            double_linked_list* trained_courses = read_list_of_courses(
              tok,
              available_courses,
              read_status
            );

            if (*read_status == INVALID_STRUCTURE)
            {
              // trained courses list does no need to be freed,
              // read_list_of_courses manages that; moreover it is null
              free_double_linked_list(professors);
              professors = NULL;
              break;
            }
            else
            {
              dll_append_value(
                professors,
                construct_professor_from_name_courses(
                  &professor_name,
                  trained_courses
                )
              );

              free_double_linked_list(trained_courses);
            }
          }
        }
      }
    }
  }

  free(cur_read_seq);
  return professors;
}

double_linked_list* read_tas_from_input_file(
  FILE* file_ptr,
  double_linked_list* available_courses,
  file_read_status* restrict read_status
)
{
  double_linked_list* tas = construct_double_linked_list_with_comp_destr(
    sizeof(teaching_assistant),
    ta_name_comparator_wrapper,
    free_teaching_assistant_wrapper
  );
  char* cur_read_seq = NULL;
  size_t cur_read_seq_size = 0;

  while (1)
  {
    char* tok = NULL;
    being_name ta_name;
    ssize_t num_read_chars = 0;

    // try to read a ta's first name
    num_read_chars = getdelim(
      &cur_read_seq,
      &cur_read_seq_size,
      '\n',
      file_ptr
    );

    if (
      (feof(file_ptr) && num_read_chars <= 0) ||
      (tok = strtok(cur_read_seq, " \n")) == NULL ||
      !is_word_of_letters(tok) ||
      strcmp(tok, STR_VAL(PROFESSOR_DELIM_TOK)) == 0 ||
      strcmp(tok, STR_VAL(TA_DELIM_TOK)) == 0
    )
    {
      free_double_linked_list(tas);
      tas = NULL;
      *read_status = INVALID_STRUCTURE;
      break;
    }
    else
    {
      if (strcmp(tok, STR_VAL(STUDENT_DELIM_TOK)) == 0)
      // Is it a student token?
      {
        if (strtok(NULL, " \n"))
        {
          free_double_linked_list(tas);
          tas = NULL;
          *read_status = INVALID_STRUCTURE;
        }
        else
        {
          *read_status = OK;
        }

        break;
      }
      else
      // the currently read sequence is a valid ta's first name
      {
        // copy currently read sequence to ta_name->first_name
        strncpy(
          ta_name.first_name,
          tok,
          FIRST_NAME_SIZE - 1
        );
        ta_name.first_name[FIRST_NAME_SIZE - 1] = '\0';

        // try to read ta's last name
        tok = strtok(NULL, " ");

        if (tok == NULL)
        {
          free_double_linked_list(tas);
          tas = NULL;
          *read_status = INVALID_STRUCTURE;
          break;
        }
        else
        {
          // copy currently read sequence to ta_name->last_name
          strncpy(
            ta_name.last_name,
            tok,
            LAST_NAME_SIZE - 1
          );
          ta_name.last_name[LAST_NAME_SIZE - 1] = '\0';

          if (
            is_invalid_name(tok) ||
            dll_find_first(tas, &ta_name)
          )
          {
            free_double_linked_list(tas);
            tas = NULL;
            *read_status = INVALID_STRUCTURE;
            break;
          }
          else
          // A new ta's first and last names are read and are both valid;
          // Try to read courses
          {
            tok = strtok(NULL, "\n");

            double_linked_list* trained_courses = read_list_of_courses(
              tok,
              available_courses,
              read_status
            );

            if (*read_status == INVALID_STRUCTURE)
            {
              // trained courses list does no need to be freed,
              // read_list_of_courses manages that; moreover it is null
              free_double_linked_list(tas);
              tas = NULL;
              break;
            }
            else
            {
              dll_append_value(
                tas,
                construct_ta_from_name_courses(
                  &ta_name,
                  trained_courses
                )
              );

              free_double_linked_list(trained_courses);
            }
          }
        }
      }
    }
  }

  free(cur_read_seq);
  return tas;
}

double_linked_list* read_students_from_input_file(
  FILE* file_ptr,
  double_linked_list* available_courses,
  file_read_status* restrict read_status
)
{
  double_linked_list* students = construct_double_linked_list_with_comp_destr(
    sizeof(student),
    student_id_comparator_wrapper,
    free_student_wrapper
  );
  char* cur_read_seq = NULL;
  size_t cur_read_seq_size = 0;

  while (1)
  {
    char* tok = NULL;
    being_name student_name;
    char id[STUDENT_ID_STR_SIZE];
    ssize_t read_chars = -1;

    // try to read a student's first name
    read_chars = getdelim(&cur_read_seq, &cur_read_seq_size, '\n', file_ptr);

    if (
      feof(file_ptr) &&
      read_chars == -1
    )
    {
      *read_status = OK;

      break;
    }
    else
    {
      tok = strtok(cur_read_seq, " \n");

      if (
        tok == NULL ||
        is_invalid_name(tok)
      )
      {
        free_double_linked_list(students);
        students = NULL;
        *read_status = INVALID_STRUCTURE;
        break;
      }
      else
      // the currently read sequence is a valid student's first name
      {
        // copy currently read sequence to student_name->first_name
        strncpy(
          student_name.first_name,
          tok,
          FIRST_NAME_SIZE - 1
        );
        student_name.first_name[FIRST_NAME_SIZE - 1] = '\0';

        // try to read ta's last name
        tok = strtok(NULL, " ");

        if (
          tok == NULL ||
          is_invalid_name(tok)
        )
        {
          free_double_linked_list(students);
          students = NULL;
          *read_status = INVALID_STRUCTURE;
          break;
        }
        else
        {
          // copy currently read sequence to student_name->last_name
          strncpy(
            student_name.last_name,
            tok,
            LAST_NAME_SIZE - 1
          );
          student_name.last_name[LAST_NAME_SIZE - 1] = '\0';

          // A new student's first and last names are read and are both valid;
          // Try to read ID
          tok = strtok(NULL, " ");

          if (tok == NULL || !is_valid_student_id(tok))
          {
            free_double_linked_list(students);
            students = NULL;
            *read_status = INVALID_STRUCTURE;
            break;
          }
          else
          {
            strcpy(id, tok);

            if (dll_find_first(students, id))
            {
              free_double_linked_list(students);
              students = NULL;
              *read_status = INVALID_STRUCTURE;
              break;
            }
            else
            {
              tok = strtok(NULL, "\n");

              double_linked_list* trained_courses = read_list_of_courses(
                tok,
                available_courses,
                read_status
              );

              if (*read_status == INVALID_STRUCTURE)
              {
                // trained courses list does no need to be freed,
                // read_list_of_courses manages that; moreover it is null
                free_double_linked_list(students);
                students = NULL;
                break;
              }
              else
              {
                dll_append_value(
                  students,
                  construct_student_from_name_courses(
                    &student_name,
                    id,
                    trained_courses
                  )
                );

                free_double_linked_list(trained_courses);
              }
            }
          }
        }
      }
    }
  }

  free(cur_read_seq);
  return students;
}

schedule_input* read_input_file(
  FILE* file_ptr,
  file_read_status* restrict read_status
)
{
  schedule_input* result = NULL;

  if (file_ptr)
  {
    double_linked_list* courses = read_courses_from_input_file(
      file_ptr,
      read_status
    );

    if (*read_status == OK)
    {
      double_linked_list* professors = read_professors_from_input_file(
        file_ptr,
        courses,
        read_status
      );

      if (*read_status == OK)
      {
        double_linked_list* tas = read_tas_from_input_file(
          file_ptr,
          courses,
          read_status
        );

        if (*read_status == OK)
        {
          double_linked_list* students = read_students_from_input_file(
            file_ptr,
            courses,
            read_status
          );

          if (*read_status == OK)
          {
            result = construct_schedule_input(
              courses,
              professors,
              tas,
              students
            );
          }
          else
          {
            free_double_linked_list(courses);
            free_double_linked_list(professors);
            free_double_linked_list(tas);
          }
        }
        else
        {
          free_double_linked_list(courses);
          free_double_linked_list(professors);
        }
      }
      else
      {
        free_double_linked_list(courses);
      }
    }
  }
  else
  {
    *read_status = FILE_NOT_EXISTS;
  }

  return result;
}

#endif //ASSIGNMENT_2_SCHEDULE_INPUT_H
