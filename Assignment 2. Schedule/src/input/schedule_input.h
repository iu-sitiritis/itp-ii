#ifndef ASSIGNMENT_2_SCHEDULE_SCHEDULE_INPUT_H
#define ASSIGNMENT_2_SCHEDULE_SCHEDULE_INPUT_H

#include <stdlib.h>

#include "../double_linked_list/double_linked_list.h"
#include "../model/model_include.h"

typedef
  struct schedule_input
  {
    double_linked_list* courses;
    double_linked_list* professors;
    double_linked_list* tas;
    double_linked_list* students;
    unsigned int total_penalty;
  }
schedule_input;

schedule_input* construct_schedule_input(
  double_linked_list* courses,
  double_linked_list* professors,
  double_linked_list* tas,
  double_linked_list* students
)
{
  schedule_input* result = malloc(sizeof(schedule_input));

  result->courses = courses;
  result->professors = professors;
  result->tas = tas;
  result->students = students;

  unsigned int students_penalty = 0;
  dll_map_for_all(
    students,
    add_penalty_for_student_wrapper,
    &students_penalty
  );

  result->total_penalty =
    (COURSE_NOT_RUNNING_PENALTY * courses->size) +
    (PROFESSOR_NOT_RUNNING_COURSE_PENALTY *
    PROFESSOR_MAX_ASSIGNED_TRAINED_COURSES * professors->size) +
    (TA_LACK_LAB_PENALTY * TA_MAX_ASSIGNED_TRAINED_COURSES * tas->size) +
    students_penalty
  ;

  return result;
}

void free_schedule_input(schedule_input* schedule_input_to_free)
{
  free_double_linked_list(schedule_input_to_free->courses);
  free_double_linked_list(schedule_input_to_free->professors);
  free_double_linked_list(schedule_input_to_free->tas);
  free_double_linked_list(schedule_input_to_free->students);
  free(schedule_input_to_free);
}

#endif //ASSIGNMENT_2_SCHEDULE_SCHEDULE_INPUT_H
