#ifndef ASSIGNMENT_2_SCHEDULE_STR_UTILS_H
#define ASSIGNMENT_2_SCHEDULE_STR_UTILS_H

#include <stddef.h>
#include <ctype.h>

char is_word_of_letters(const char* str)
{
  if (*str)
  {
    while (*str)
    {
      if (!isalpha(*str))
      {
        return 0;
      }

      ++str;
    }

    return 1;
  }
  else
  {
    return 0;
  }
}

char is_word_of_digits(const char* str)
{
  while (*str)
  {
    if (!isdigit(*str))
    {
      return 0;
    }

    ++str;
  }

  return 1;
}

char is_word_of_digits_without_lead_zero(const char* str)
{
  char result = 0;

  if (*str && (*str != '0'))
  {
    result = is_word_of_digits(str);
  }

  return result;
}

/**
 * @brief Deletes whitespace characters from the right of a given
 * null-terminated string by putting '\0' after the last non-whitespace
 * character.
 * @param str Null-terminated string to trim from the right. Not null.
 */
void trim_right(char* restrict str)
{
  char* restrict last_nonws = str;

  while (*str)
  {
    if (!isspace(*str))
    {
      last_nonws = str;
    }

    ++str;
  }

  *last_nonws = '\0';
}

/**
 * @brief Deletes whitespace characters from the right of a given
 * null-terminated string by putting '\0' after the last non-whitespace
 * character. If the size of the string is known it is preferable to use
 * this function over trim_right(), because it will perform faster.
 * @param str Null-terminated string to trim from the right. Not null.
 * @param len Length of str until and including the first null-terminator.
 * @see trim_right()
 */
void trim_char_arr_right(char* restrict str, size_t len)
{
  if (len > 0)
  {
    str += len - 1;
    char* restrict pos = str;

    while (len > 0 && (isspace(*str) || (*str == '\0')))
    {
      pos = str;

      --str;
      --len;
    }

    *pos = '\0';
  }
}

#endif //ASSIGNMENT_2_SCHEDULE_STR_UTILS_H
