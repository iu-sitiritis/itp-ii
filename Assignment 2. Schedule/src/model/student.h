#ifndef ASSIGNMENT_2_SCHEDULE_STUDENT_H
#define ASSIGNMENT_2_SCHEDULE_STUDENT_H

#include <stdlib.h>
#include <memory.h>
#include <ctype.h>

#include "../double_linked_list/double_linked_list.h"


// including '\0', so ID itself is exactly 5 chars
#define STUDENT_ID_STR_SIZE 6

#define STUDENT_LEAST_AMOUNT_REQ_CLASS 1

#define STUDENT_LACK_COURSE_PENALTY 1

#define STUDENT_DELIM_TOK S


typedef
  struct student
  {
    being_name name;
    char id[STUDENT_ID_STR_SIZE];
    double_linked_list* requested_courses;
  }
student;


student* construct_student(
  const char* first_name,
  const char* last_name,
  const char id[STUDENT_ID_STR_SIZE],
  double_linked_list* requested_courses
)
{
  student* result = malloc(sizeof(student));

  strncpy(result->name.first_name, first_name, FIRST_NAME_SIZE - 1);
  result->name.first_name[FIRST_NAME_SIZE - 1] = '\0';
  strncpy(result->name.last_name, last_name, LAST_NAME_SIZE - 1);
  result->name.last_name[LAST_NAME_SIZE - 1] = '\0';
  strncpy(result->id, id, STUDENT_ID_STR_SIZE - 1);
  result->id[STUDENT_ID_STR_SIZE - 1] = '\0';
  result->requested_courses = copy_double_linked_list(requested_courses);

  return result;
}

student* construct_student_from_name_courses(
  const being_name* name,
  const char id[STUDENT_ID_STR_SIZE],
  double_linked_list* requested_courses
)
{
  return construct_student(
    name->first_name,
    name->last_name,
    id,
    requested_courses
  );
}

student* copy_student_dont_copy_courses(
  student* student_to_copy
)
{
  student* result = malloc(sizeof(student));

  strncpy(
    result->name.first_name,
    student_to_copy->name.first_name,
    FIRST_NAME_SIZE - 1
  );
  result->name.first_name[FIRST_NAME_SIZE - 1] = '\0';
  strncpy(
    result->name.last_name,
    student_to_copy->name.last_name,
    LAST_NAME_SIZE - 1
  );
  result->name.last_name[LAST_NAME_SIZE - 1] = '\0';
  strcpy(result->id, student_to_copy->id);
  result->requested_courses = student_to_copy->requested_courses;

  return result;
}


void free_student(student* student_to_free)
{
  free_double_linked_list(student_to_free->requested_courses);
  free(student_to_free);
}

void free_student_leave_courses(student* student_to_free)
{
  free(student_to_free);
}

void free_student_wrapper(void* student_to_free)
{
  free_student(student_to_free);
}

void free_student_leave_courses_wrapper(void* student_to_free)
{
  free_student_leave_courses(student_to_free);
}


const int student_id_comparator_wrapper(
  const void* in_student,
  const void* id
)
{
  return strcmp(
    ((student*)in_student)->id,
    id
  );
}


unsigned char is_valid_student_id(const char* id)
{
  size_t cur_char_i = 0;

  for (
    ;
    cur_char_i < STUDENT_ID_STR_SIZE &&
    *id;
    ++id, ++cur_char_i
  )
  {
    if (!isalnum(*id))
    {
      return 0;
    }
  }

  return (*id || cur_char_i != (STUDENT_ID_STR_SIZE - 1)) ? 0 : 1;
}


void add_penalty_for_student_wrapper(
  void* in_student,
  va_list accumulator_arg
)
{
  unsigned int* accumulator = va_arg(accumulator_arg, unsigned int*);
  *accumulator +=
    STUDENT_LACK_COURSE_PENALTY *
    ((student*)in_student)->requested_courses->size
  ;
}


void print_student(student* stud, FILE* file_ptr)
{
  fprintf(
    file_ptr,
    "%s %s %s\n",
    stud->name.first_name,
    stud->name.last_name,
    stud->id
  );
}

void print_student_wrapper(void* stud, va_list file_p)
{
  FILE* file_ptr = va_arg(file_p, FILE*);
  print_student(stud, file_ptr);
}

#endif //ASSIGNMENT_2_SCHEDULE_STUDENT_H
