#ifndef ASSIGNMENT_2_SCHEDULE_BEING_NAME_H
#define ASSIGNMENT_2_SCHEDULE_BEING_NAME_H

#define FIRST_NAME_SIZE 64
#define LAST_NAME_SIZE 64


typedef
  struct being_name
  {
    char first_name[FIRST_NAME_SIZE];
    char last_name[LAST_NAME_SIZE];
  }
being_name;

#endif //ASSIGNMENT_2_SCHEDULE_BEING_NAME_H
