#ifndef ASSIGNMENT_2_SCHEDULE_PROFESSOR_H
#define ASSIGNMENT_2_SCHEDULE_PROFESSOR_H

#include <stdlib.h>
#include <memory.h>

#include "being_name.h"
#include "../double_linked_list/double_linked_list.h"


#define PROFESSOR_MAX_ASSIGNED_TRAINED_COURSES 2
#define PROFESSOR_MAX_ASSIGNED_UNTRAINED_COURSES 1

#define PROFESSOR_LEAST_NUM_TRAINED_COURSES 1

#define PROFESSOR_NOT_RUNNING_COURSE_PENALTY 5

#define PROFESSOR_DELIM_TOK P


typedef
  struct professor
  {
    being_name name;
    double_linked_list* trained_courses;
  }
professor;


professor* construct_professor(
  const char* first_name,
  const char* last_name,
  double_linked_list* trained_courses
)
{
  professor* result = malloc(sizeof(professor));

  strncpy(result->name.first_name, first_name, FIRST_NAME_SIZE - 1);
  result->name.first_name[FIRST_NAME_SIZE - 1] = '\0';
  strncpy(result->name.last_name, last_name, LAST_NAME_SIZE - 1);
  result->name.last_name[LAST_NAME_SIZE - 1] = '\0';
  result->trained_courses = copy_double_linked_list(trained_courses);

  return result;
}

professor* construct_professor_from_name_courses(
  const being_name* name,
  double_linked_list* trained_courses
)
{
  return construct_professor(
    name->first_name,
    name->last_name,
    trained_courses
  );
}

void free_professor(professor* professor_to_free)
{
  free_double_linked_list(professor_to_free->trained_courses);
  free(professor_to_free);
}

void free_professor_wrapper(void* professor_to_free)
{
  free_professor(professor_to_free);
}


int professor_name_comparator(
  const professor* in_prof,
  const being_name* name_of_being
)
{
  int result = strcmp(
    in_prof->name.first_name,
    name_of_being->first_name
  );

  if (!result)
  {
    result = strcmp(
      in_prof->name.last_name,
      name_of_being->last_name
    );
  }

  return result;
}

int professor_comparator(
  const professor* left,
  const professor* right
)
{
  if (left == right)
  {
    return 0;
  }
  else if (!left)
  {
    return -1;
  }
  else if (!right)
  {
    return 1;
  }
  else
  {
    return professor_name_comparator(left, &(right->name));
  }
}

const int professor_name_comparator_wrapper(
  const void* in_prof,
  const void* name_of_being
)
{
  return professor_name_comparator(
    (const professor*)in_prof,
    (const being_name*)name_of_being
  );
}

#endif //ASSIGNMENT_2_SCHEDULE_PROFESSOR_H
