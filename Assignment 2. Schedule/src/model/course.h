#ifndef ASSIGNMENT_2_SCHEDULE_COURSE_H
#define ASSIGNMENT_2_SCHEDULE_COURSE_H

#include <stdlib.h>
#include <memory.h>
#include <string.h>

#define COURSE_NAME_SIZE 128

#define COURSE_LEAST_AMOUNT_STUDENTS 1
#define COURSE_LEAST_AMOUNT_LABS 1

#define COURSE_NOT_RUNNING_PENALTY 20


typedef
  struct course
  {
    char name[COURSE_NAME_SIZE];
    unsigned int req_labs;
    unsigned int max_students;
    unsigned int unassigned_labs;
  }
course;

course* construct_course(
  const char* name,
  unsigned int req_labs,
  unsigned int max_students
)
{
  course* result = malloc(sizeof(course));

  strncpy(result->name, name, COURSE_NAME_SIZE - 1);
  result->name[COURSE_NAME_SIZE - 1] = '\0';
  result->req_labs = req_labs;
  result->max_students = max_students;
  result->unassigned_labs = req_labs;

  return result;
}

void free_course(course* course_to_free)
{
  free(course_to_free);
}

void free_course_wrapper(void* course_to_free)
{
  free_course(course_to_free);
}


const int course_string_comparator_wrapper(
  const void* in_course,
  const void* name
)
{
  return strcmp(
    ((const course*)in_course)->name,
    (const char*)name
  );
}

const int course_ptr_string_comparator(const void* in_course, const void* name)
{
  return strcmp(
    (*((const course**)in_course))->name,
    (const char*)name
  );
}

#endif //ASSIGNMENT_2_SCHEDULE_COURSE_H
