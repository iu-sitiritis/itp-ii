#ifndef ASSIGNMENT_2_SCHEDULE_TEACHING_ASSISTANT_H
#define ASSIGNMENT_2_SCHEDULE_TEACHING_ASSISTANT_H

#include <stdlib.h>
#include <memory.h>

#include "../double_linked_list/double_linked_list.h"


#define TA_MAX_ASSIGNED_TRAINED_COURSES 4

#define TA_LEAST_NUM_TRAINED_COURSES 1

#define TA_LACK_LAB_PENALTY 2

#define TA_DELIM_TOK T


typedef
  struct teaching_assistant
  {
    being_name name;
    double_linked_list* trained_courses;
    unsigned int labs_left;
  }
teaching_assistant;


teaching_assistant* construct_teaching_assistant(
  const char* first_name,
  const char* last_name,
  double_linked_list* trained_courses
)
{
  teaching_assistant* result = malloc(sizeof(teaching_assistant));

  strncpy(result->name.first_name, first_name, FIRST_NAME_SIZE - 1);
  result->name.first_name[FIRST_NAME_SIZE - 1] = '\0';
  strncpy(result->name.last_name, last_name, LAST_NAME_SIZE - 1);
  result->name.last_name[LAST_NAME_SIZE - 1] = '\0';
  result->trained_courses = copy_double_linked_list(trained_courses);
  result->labs_left = TA_MAX_ASSIGNED_TRAINED_COURSES;

  return result;
}

teaching_assistant* construct_ta_from_name_courses(
  const being_name* name,
  double_linked_list* trained_courses
)
{
  return construct_teaching_assistant(
    name->first_name,
    name->last_name,
    trained_courses
  );
}

teaching_assistant* copy_teaching_assistant(
  teaching_assistant* ta_to_copy
)
{
  teaching_assistant* result =
    construct_ta_from_name_courses(
    &ta_to_copy->name,
    ta_to_copy->trained_courses
  );

  result->labs_left = ta_to_copy->labs_left;

  return result;
}

teaching_assistant* copy_teaching_assistant_dont_copy_courses(
  teaching_assistant* ta_to_copy
)
{
  teaching_assistant* result = malloc(sizeof(teaching_assistant));

  strncpy(
    result->name.first_name,
    ta_to_copy->name.first_name,
    FIRST_NAME_SIZE - 1
  );
  result->name.first_name[FIRST_NAME_SIZE - 1] = '\0';
  strncpy(
    result->name.last_name,
    ta_to_copy->name.last_name,
    LAST_NAME_SIZE - 1
  );
  result->name.last_name[LAST_NAME_SIZE - 1] = '\0';
  result->trained_courses = ta_to_copy->trained_courses;
  result->labs_left = TA_MAX_ASSIGNED_TRAINED_COURSES;

  return result;
}

void free_teaching_assistant(teaching_assistant* ta_to_free)
{
  free_double_linked_list(ta_to_free->trained_courses);
  free(ta_to_free);
}

void free_teaching_assistant_leave_courses(
  teaching_assistant* ta_to_free
)
{
  free(ta_to_free);
}

void free_teaching_assistant_wrapper(void* ta_to_free)
{
  free_teaching_assistant(ta_to_free);
}

void free_teaching_assistant_leave_courses_wrapper(
  void* ta_to_free
)
{
  free_teaching_assistant_leave_courses(ta_to_free);
}


const int ta_name_comparator_wrapper(
  const void* in_ta,
  const void* name_of_being
)
{
  int result = strcmp(
    ((teaching_assistant*)in_ta)->name.first_name,
    ((being_name*)name_of_being)->first_name
  );

  if (!result)
  {
    result = strcmp(
      ((teaching_assistant*)in_ta)->name.last_name,
      ((being_name*)name_of_being)->last_name
    );
  }

  return result;
}


void calculate_penalty_cancellation(
  teaching_assistant* ta,
  unsigned int* penalty_cancellation
)
{
  *penalty_cancellation +=
    (TA_MAX_ASSIGNED_TRAINED_COURSES - (ta->labs_left)) *
    TA_LACK_LAB_PENALTY;
}

void calculate_penalty_cancellation_wrapper(
  void* ta,
  va_list penalty_cancellation
)
{
  unsigned int* cancellation = va_arg(penalty_cancellation, unsigned int*);

  calculate_penalty_cancellation(ta, cancellation);
}


void print_ta(teaching_assistant* ta, FILE* file_ptr)
{
  for (
    unsigned int labs_occupied =
      TA_MAX_ASSIGNED_TRAINED_COURSES - ta->labs_left;
    labs_occupied > 0;
    --labs_occupied
  )
  {
    fprintf(
      file_ptr,
      "%s %s\n",
      ta->name.first_name,
      ta->name.last_name
    );
  }
}

void print_ta_wrapper(void* ta, va_list file_p)
{
  FILE* file_ptr = va_arg(file_p, FILE*);
  print_ta(ta, file_ptr);
}

#endif //ASSIGNMENT_2_SCHEDULE_TEACHING_ASSISTANT_H
