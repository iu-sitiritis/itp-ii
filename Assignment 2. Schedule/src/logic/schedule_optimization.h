#ifndef ASSIGNMENT_2_SCHEDULE_SCHEDULE_OPTIMIZATION_H
#define ASSIGNMENT_2_SCHEDULE_SCHEDULE_OPTIMIZATION_H

#include "assignment_distribution.h"

// -----------------------------------------------------------------------------
// Professors to a course
// -----------------------------------------------------------------------------

void assign_professor_to_course(
  professor* professor_to_assign,
  course* course_to_assign,
  double_linked_list* course_assignments_list_to_append
)
{
  dll_append_value(
    course_assignments_list_to_append,
    construct_course_assignment_with_course_prof(
      course_to_assign,
      professor_to_assign
    )
  );
}

void assign_professor_to_course_wrapper(
  void* passed_professor,
  va_list course_assiglist
)
{
  course* course_to_assign =
    va_arg(course_assiglist, course*);
  double_linked_list* course_assignments_list_to_append =
    va_arg(course_assiglist, double_linked_list*);

  assign_professor_to_course(
    (professor*)passed_professor,
    course_to_assign,
    course_assignments_list_to_append
  );
}

/**
 * @return Double linked list of course_assignments. Each entry contains
 * a unique combination of each professor from the universe of professors with
 * a given course, plus case, when a course lacks a professor with calculated
 * corresponding penalty cancellation for the assignment. Size of the resulting
 * lists is |professors| + 1.
 */
double_linked_list* assign_professors_to_course(
  course* course_to_assign,
  double_linked_list* professors
)
{
  double_linked_list* result = construct_double_linked_list_with_destr(
    sizeof(course_assignment),
    free_course_assignment_leave_course_prof_wrapper
  );

  dll_map_for_all(
    professors,
    assign_professor_to_course_wrapper,
    course_to_assign,
    result
  );

  return result;
}

// -----------------------------------------------------------------------------
// TAs to a course
// -----------------------------------------------------------------------------

void add_assignable_to_course_tas_to_list(
  teaching_assistant* ta,
  course* course_to_test,
  double_linked_list* list_to_append
)
{
  if (dll_find_first(ta->trained_courses, course_to_test->name))
  // if ta is trained for the course
  {
    dll_append_value(list_to_append, ta);
  }
}

void add_assignable_to_course_tas_to_list_wrapper(
  void* ta,
  va_list course_list_to_append
)
{
  course* course_to_test =
    va_arg(course_list_to_append, course*);
  double_linked_list* list_to_append =
    va_arg(course_list_to_append, double_linked_list*);

  add_assignable_to_course_tas_to_list(ta, course_to_test, list_to_append);
}

void insert_ta_copy_into_list(teaching_assistant* ta, double_linked_list* list)
{
  dll_append_copy(list, ta);
}

void insert_ta_copy_into_list_wrapper(void* ta, va_list dll)
{
  double_linked_list* list = va_arg(dll, double_linked_list*);

  insert_ta_copy_into_list(ta, list);
}

void get_combinations_of_tas_for_labs_backtrack(
  double_linked_list* tas_available,
  double_linked_list* tas_to_assign,
  unsigned int course_labs_left,
  unsigned int first_ta_labs_left,
  double_linked_list* result_list
)
{
  double_linked_list* copy_tas_to_assign =
    construct_double_linked_list_with_comp_destr(
      sizeof(teaching_assistant),
      ta_name_comparator_wrapper,
      free_teaching_assistant_leave_courses_wrapper
    );
  dll_map_for_all(
    tas_to_assign,
    insert_ta_copy_into_list_wrapper,
    copy_tas_to_assign
  );

  double_linked_list* copy_tas_available =
    construct_double_linked_list_with_comp_destr(
      sizeof(teaching_assistant),
      ta_name_comparator_wrapper,
      free_teaching_assistant_leave_courses_wrapper
    );
  dll_map_for_all(
    tas_available,
    insert_ta_copy_into_list_wrapper,
    copy_tas_available
  );

  while (!dll_is_empty(copy_tas_available))
  {
    if (course_labs_left == 0)
    // If no more labs left, then we are at the bottom of a backtracking tree
    // and a copy of tas_to_assign needs to be added to the result_list
    {
      dll_append_value(
        result_list,
        copy_double_linked_list(copy_tas_to_assign)
      );
      break;
    }
    else
    {
      // Copy tas to assign again
      dll_clear(copy_tas_to_assign);
      dll_map_for_all(
        tas_to_assign,
        insert_ta_copy_into_list_wrapper,
        copy_tas_to_assign
      );

      if (first_ta_labs_left == 0)
      // if the first TA ran out of his labs, delete him from the list and continue
      // with other TAs
      {
        dll_remove_first(copy_tas_available);
      }

      if (!dll_is_empty(copy_tas_available))
      {
        // search for TA by name in the list of TAs to assign
        teaching_assistant* cur_ta =
          (teaching_assistant*)copy_tas_available->head->value;

        teaching_assistant* found_ta = dll_find_first(
          copy_tas_to_assign,
          &(cur_ta->name)
        );

        if (found_ta)
        {
          --(found_ta->labs_left);
        }
        else
        {
          found_ta = copy_teaching_assistant_dont_copy_courses(cur_ta);
          first_ta_labs_left = found_ta->labs_left;
          --(found_ta->labs_left);

          dll_append_value(
            copy_tas_to_assign,
            found_ta
          );
        }

        get_combinations_of_tas_for_labs_backtrack(
          copy_tas_available,
          copy_tas_to_assign,
          course_labs_left - 1,
          first_ta_labs_left - 1,
          result_list
        );

        dll_remove_first(copy_tas_available);
      }
    }
  }

  free_double_linked_list(copy_tas_available);
  free_double_linked_list(copy_tas_to_assign);
}

void assign_ta_combination_to_course(
  double_linked_list* ta_combination,
  course* course_to_assign,
  double_linked_list* list_to_add
)
{
//  course_assignment* value_to_add =
//    construct_course_assignment_with_course_tas(
//      course_to_assign,
//      ta_combination
//    );
  dll_append_value(
    list_to_add,
    construct_course_assignment_with_course_tas(
      course_to_assign,
      ta_combination
    )
  );
}

void assign_ta_combination_to_course_wrapper(
  void* ta_combination,
  va_list course_listtoadd
)
{
  course* course_to_assign = va_arg(
    course_listtoadd,
    course*
  );
  double_linked_list* list_to_add = va_arg(
    course_listtoadd,
    double_linked_list*
  );

  assign_ta_combination_to_course(
    ta_combination,
    course_to_assign,
    list_to_add
  );
}

double_linked_list* assign_tas_to_course(
  course* course_to_assign,
  double_linked_list* all_tas
)
{
  double_linked_list* assignable_tas = construct_double_linked_list_with_comp(
    sizeof(teaching_assistant),
    ta_name_comparator_wrapper
  );

  // Find all assignable to a given course tas
  dll_map_for_all(
    all_tas,
    add_assignable_to_course_tas_to_list_wrapper,
    course_to_assign,
    assignable_tas
  );

  double_linked_list* ta_combinations =
    construct_empty_double_linked_list(
      sizeof(double_linked_list*)
    );

  double_linked_list* tas_to_assign =
    construct_double_linked_list_with_comp_destr(
      sizeof(teaching_assistant),
      ta_name_comparator_wrapper,
      free_teaching_assistant_wrapper
    );

  get_combinations_of_tas_for_labs_backtrack(
    assignable_tas,
    tas_to_assign,
    course_to_assign->req_labs,
    TA_MAX_ASSIGNED_TRAINED_COURSES,
    ta_combinations
  );
  free_double_linked_list(tas_to_assign);

  free_double_linked_list(assignable_tas);

  double_linked_list* result = construct_double_linked_list_with_destr(
    sizeof(course_assignment),
    free_course_assignment_leave_course_prof_wrapper
  );

  dll_map_for_all(
    ta_combinations,
    assign_ta_combination_to_course_wrapper,
    course_to_assign,
    result
  );

  free_double_linked_list(ta_combinations);

  return result;
}

// -----------------------------------------------------------------------------
// Mix TAs & Profs for a single course
// -----------------------------------------------------------------------------

void combine_tas_with_prof(
  course_assignment* course_assignment_tas,
  course_assignment* professor_course_assignment,
  double_linked_list* result_course_assignment
)
{
  course_assignment* result_assignment = subtle_copy_course_assignment(
    professor_course_assignment
  );
  result_assignment->assigned_tas = copy_double_linked_list(
    course_assignment_tas->assigned_tas
  );
  result_assignment->penalty_cancellation +=
    course_assignment_tas->penalty_cancellation +
    COURSE_NOT_RUNNING_PENALTY
    // since course is now running
  ;

  dll_append_value(
    result_course_assignment,
    result_assignment
  );
}

void combine_tas_with_prof_wrapper(
  void* course_assignment_tas,
  va_list profCa_result
)
{
  course_assignment* professor_course_assignment =
    va_arg(profCa_result, course_assignment*);
  double_linked_list* result_course_assignment =
    va_arg(profCa_result, double_linked_list*);

  combine_tas_with_prof(
    course_assignment_tas,
    professor_course_assignment,
    result_course_assignment
  );
}

void combine_tas_prof_course_assignment(
  course_assignment* prof_course_assignment,
  double_linked_list* tas_course_assignments,
  double_linked_list* result_course_assignment
)
{
  dll_map_for_all(
    tas_course_assignments,
    combine_tas_with_prof_wrapper,
    prof_course_assignment,
    result_course_assignment
  );
}

void combine_tas_prof_course_assignment_wrapper(
  void* prof_course_assignment,
  va_list taCa_result
)
{
  double_linked_list* tas_course_assignments =
    va_arg(taCa_result, double_linked_list*);
  double_linked_list* result_course_assignment =
    va_arg(taCa_result, double_linked_list*);

  combine_tas_prof_course_assignment(
    prof_course_assignment,
    tas_course_assignments,
    result_course_assignment
  );
}

double_linked_list* get_combinations_of_professors_tas_for_course(
  course* course_obj,
  double_linked_list* all_profs,
  double_linked_list* all_tas
)
{
  double_linked_list* result = construct_double_linked_list_with_destr(
    sizeof(course_assignment),
    free_course_assignment_leave_course_prof_wrapper
  );

  double_linked_list* profs_assigned = assign_professors_to_course(
    course_obj,
    all_profs
  );

  double_linked_list* tas_assigned = assign_tas_to_course(
    course_obj,
    all_tas
  );
//  tas_assigned->destr = NULL;

  dll_map_for_all(
    profs_assigned,
    combine_tas_prof_course_assignment_wrapper,
    tas_assigned,
    result
  );

  course_assignment* no_profs_tas =
    construct_course_assignment_with_course_prof(
      course_obj,
      NULL
    );
  no_profs_tas->penalty_cancellation = 0;

  dll_append_value(
    result,
    no_profs_tas
  );

  free_double_linked_list(profs_assigned);
  free_double_linked_list(tas_assigned);

  return result;
}

// -----------------------------------------------------------------------------
// Mix TAs & Profs for all courses
// -----------------------------------------------------------------------------

void insert_combinations_of_profs_tas_for_course_into_list(
  course* cur_course,
  double_linked_list* all_profs,
  double_linked_list* all_tas,
  double_linked_list* list_to_insert
)
{
  dll_append_value(list_to_insert,
    get_combinations_of_professors_tas_for_course(
      cur_course,
      all_profs,
      all_tas
    )
  );
}

void insert_combinations_of_profs_tas_for_course_into_list_wrapper(
  void* cur_course,
  va_list profs_tas_resultList
)
{
  double_linked_list* all_profs = va_arg(
    profs_tas_resultList,
    double_linked_list*
  );
  double_linked_list* all_tas = va_arg(
    profs_tas_resultList,
    double_linked_list*
  );
  double_linked_list* list_to_insert = va_arg(
    profs_tas_resultList,
    double_linked_list*
  );

  insert_combinations_of_profs_tas_for_course_into_list(
    cur_course,
    all_profs,
    all_tas,
    list_to_insert
  );
}

double_linked_list* get_combinations_of_tas_professors_for_all_courses(
  double_linked_list* all_courses,
  double_linked_list* all_profs,
  double_linked_list* all_tas
)
{
  // list, where each entry is a list of course_assignments for a particular
  // course
  double_linked_list* result = construct_double_linked_list_with_destr(
    sizeof(double_linked_list*),
    free_double_linked_list_wrapper
  );

  dll_map_for_all(
    all_courses,
    insert_combinations_of_profs_tas_for_course_into_list_wrapper,
    all_profs,
    all_tas,
    result
  );

  return result;
}

// -----------------------------------------------------------------------------
// Produce all possible combinations of assignments of Profs, TAs, students for
// all courses from all combinations for each course and find a combination
// with a minimal penalty
// -----------------------------------------------------------------------------

char professor_with_course_assignable_to_distribution(
  course_assignment* prof_course,
  assignment_distribution* distribution
)
{
  char result = 1;

  if (prof_course->assigned_professor)
  {
    unsigned char num_courses_assigned = 0;
    dll_node* cur_node = distribution->assigned_courses->head;

    while (cur_node)
    {
      const professor* cur_prof =
        ((course_assignment*)cur_node->value)->assigned_professor;

      if (professor_comparator(prof_course->assigned_professor, cur_prof) == 0)
      {
        if (
          dll_find_first(
            prof_course->assigned_professor->trained_courses,
            ((course_assignment*)cur_node->value)->course_obj->name
          )
        )
        {
          ++num_courses_assigned;

          if (num_courses_assigned >= PROFESSOR_MAX_ASSIGNED_TRAINED_COURSES)
          {
            result = 0;
            break;
          }
          else if (
            !dll_find_first(
              prof_course->assigned_professor->trained_courses,
              prof_course->course_obj->name
            )
          )
          {
            result = 0;
            break;
          }
        }
        else
        // professor already has an assignment for a course he is not trained
        {
          result = 0;
          break;
        }
      }

      cur_node = cur_node->right;
    }
  }

  return result;
}

char ta_assignable_to_distribution(
  teaching_assistant* ta,
  assignment_distribution* distribution
)
{
  unsigned int labs_occupied_in_distribution = 0;

  dll_map_for_all(
    distribution->assigned_courses,
    calculate_number_occupied_labs_for_ta_in_distribution_wrapper,
    ta,
    &labs_occupied_in_distribution
  );

  return (ta->labs_left >= labs_occupied_in_distribution);
}

char all_tas_assignable_to_distribution(
  double_linked_list* tas,
  assignment_distribution* distribution
)
{
  char result = 1;

  if (tas)
  {
    dll_node* cur_node = tas->head;

    while (result && cur_node)
    {
      result = ta_assignable_to_distribution(cur_node->value, distribution);

      cur_node = cur_node->right;
    }
  }

  return result;
}

char is_valid_combination(
  assignment_distribution* distribution_so_far,
  course_assignment* assignment_to_add
)
{
  return
    professor_with_course_assignable_to_distribution(
      assignment_to_add,
      distribution_so_far
    ) &&
    all_tas_assignable_to_distribution(
      assignment_to_add->assigned_tas,
      distribution_so_far
    )
  ;
}

void try_add_course_assignment_to_distribution(
  assignment_distribution* distribution,
  course_assignment* assignment,
  double_linked_list* result
)
{
  if (is_valid_combination(distribution, assignment))
  {
    assignment_distribution* distribution_copy =
      deep_copy_assignment_distribution(
        distribution
      );

    dll_append_value(
      distribution_copy->assigned_courses,
      deep_copy_course_assignment_lists(
        assignment
      )
    );

    distribution_copy->penalty -= assignment->penalty_cancellation;

    dll_append_value(
      result,
      distribution_copy
    );
  }
}

void try_add_course_assignment_to_distribution_wrapper(
  void* distribution,
  va_list assignment_result
)
{
  course_assignment* assignment = va_arg(assignment_result, course_assignment*);
  double_linked_list* result = va_arg(assignment_result, double_linked_list*);

  try_add_course_assignment_to_distribution(
    distribution,
    assignment,
    result
  );
}

void distribute_course_assignment_among_distributions(
  course_assignment* assignment,
  double_linked_list* distributions_so_far,
  double_linked_list* result,
  unsigned int total_penalty
)
{
  if (dll_is_empty(distributions_so_far))
  {
    assignment_distribution* distribution_to_add =
      construct_empty_assignment_distribution_with_penalty(
        total_penalty
      );

    // TODO: might be memory issue
    dll_append_value(
      distribution_to_add->assigned_courses,
      deep_copy_course_assignment_lists(
        assignment
      )
    );

    distribution_to_add->penalty -= assignment->penalty_cancellation;

    dll_append_value(
      result,
      distribution_to_add
    );
  }
  else
  {
    dll_map_for_all(
      distributions_so_far,
      try_add_course_assignment_to_distribution_wrapper,
      assignment,
      result
    );
  }
}

void distribute_course_assignment_among_distributions_wrapper(
  void* assignment,
  va_list distrbsSoFar_result_penalty
)
{
  double_linked_list* distributions_so_far =
    va_arg(
      distrbsSoFar_result_penalty,
      double_linked_list*
    );
  double_linked_list* result =
    va_arg(
      distrbsSoFar_result_penalty,
      double_linked_list*
    );
  unsigned int total_penalty =
    va_arg(
      distrbsSoFar_result_penalty,
      unsigned int
    );

  distribute_course_assignment_among_distributions(
    assignment,
    distributions_so_far,
    result,
    total_penalty
  );
}

void try_add_student_to_course_assignment(
  student* student_to_add,
  course_assignment* assignment,
  unsigned int* penalty
)
{
  if (
    dll_find_first(
      student_to_add->requested_courses,
      assignment->course_obj->name
    ) &&
    assignment->students_left &&
    assignment->assigned_professor
  )
  {
    dll_append_value(assignment->assigned_students, student_to_add);
    --(assignment->students_left);
    assignment->penalty_cancellation += STUDENT_LACK_COURSE_PENALTY;
    *penalty -= STUDENT_LACK_COURSE_PENALTY;
  }
}

void try_add_student_to_course_assignment_wrapper(
  void* student_to_add,
  va_list assignment_penalty
)
{
  course_assignment* assignment =
    va_arg(assignment_penalty, course_assignment*);
  unsigned int* penalty =
    va_arg(assignment_penalty, unsigned int*);

  try_add_student_to_course_assignment(student_to_add, assignment, penalty);
}

void assign_students_for_course(
  course_assignment* assignment,
  double_linked_list* all_students,
  unsigned int* penalty
)
{
  if (!(assignment->assigned_students))
  {
    assignment->assigned_students = construct_double_linked_list_with_comp(
      sizeof(student),
      student_id_comparator_wrapper
    );
  }

  dll_map_for_all(
    all_students,
    try_add_student_to_course_assignment_wrapper,
    assignment,
    penalty
  );
}

void assign_students_for_course_wrapper(
  void* assignment,
  va_list allStudents_penalty
)
{
  double_linked_list* all_students = va_arg(
    allStudents_penalty,
    double_linked_list*
  );
  unsigned int* penalty = va_arg(allStudents_penalty, unsigned int*);

  assign_students_for_course(assignment, all_students, penalty);
}

void assign_students_to_distribution(
  assignment_distribution* distribution,
  double_linked_list* all_students
)
{
  dll_map_for_all(
    distribution->assigned_courses,
    assign_students_for_course_wrapper,
    all_students,
    &(distribution->penalty)
  );
}

void assign_students_to_distribution_wrapper(
  void* distribution,
  va_list all_students_arg
)
{
  double_linked_list* all_students = va_arg(
    all_students_arg,
    double_linked_list*
  );

  assign_students_to_distribution(distribution, all_students);
}

double_linked_list* get_all_courses_professors_tas_students_distributions(
  schedule_input* input
)
{
  double_linked_list* tas_profs_courses_combinations =
    get_combinations_of_tas_professors_for_all_courses(
      input->courses,
      input->professors,
      input->tas
    );

  double_linked_list* result = construct_double_linked_list_with_destr(
    sizeof(assignment_distribution),
    free_assignment_distribution_wrapper
  );

  dll_node* cur_course_combinations_node = tas_profs_courses_combinations->head;

  while (cur_course_combinations_node)
  {
    double_linked_list* temp_list = construct_double_linked_list_with_destr(
      sizeof(assignment_distribution),
      free_assignment_distribution_wrapper
    );

    dll_map_for_all(
      cur_course_combinations_node->value,
      distribute_course_assignment_among_distributions_wrapper,
      result,
      temp_list,
      input->total_penalty
    );

    free_double_linked_list(result);
    result = temp_list;
    cur_course_combinations_node = cur_course_combinations_node->right;
  }

  dll_map_for_all(
    result,
    assign_students_to_distribution_wrapper,
    input->students
  );

  free_double_linked_list(tas_profs_courses_combinations);
  return result;
}

void assign_optimal_schedule_to_result(
  assignment_distribution* distribution,
  assignment_distribution** optimal_distribution
)
{
  if (
    (distribution->penalty < (*optimal_distribution)->penalty) ||
    dll_is_empty((*optimal_distribution)->assigned_courses)
  )
  {
    free_assignment_distribution(*optimal_distribution);
    *optimal_distribution = deep_copy_assignment_distribution(distribution);
  }
}

void assign_optimal_schedule_to_result_wrapper(
  void* distribution,
  va_list optimal_distribution
)
{
  assignment_distribution** optimal_distrib = va_arg(
    optimal_distribution,
    assignment_distribution**
  );

  assign_optimal_schedule_to_result(distribution, optimal_distrib);
}

assignment_distribution* find_first_most_optimal_schedule(schedule_input* input)
{
  assignment_distribution* result =
    construct_empty_assignment_distribution_with_penalty(
      input->total_penalty
    );

  double_linked_list* all_combinations =
    get_all_courses_professors_tas_students_distributions(
      input
    );

  dll_map_for_all(
    all_combinations,
    assign_optimal_schedule_to_result_wrapper,
    &result
  );

  free_double_linked_list(all_combinations);
  return result;
}

void optimize_schedule_recursively(
  unsigned int cur_course_idx,
  double_linked_list* tas_profs_courses_combinations,
  double_linked_list* all_students,
  assignment_distribution** optimal_schedule_so_far,
  assignment_distribution* generated_distribution
)
{
  double_linked_list* cur_course_combinations = (double_linked_list*)
    dll_get_at(tas_profs_courses_combinations, cur_course_idx);

  for (
    size_t i = 0;
    i < cur_course_combinations->size;
    ++i
  )
  // iterate over all combinations for the course with index cur_course_idx
  {
    course_assignment* cur_course_assig_comb = (course_assignment*)
      dll_get_at(cur_course_combinations, i);

    if (
      is_valid_combination(
        generated_distribution,
        cur_course_assig_comb
      )
    )
    {
      dll_append_value(
        generated_distribution->assigned_courses,
        deep_copy_course_assignment_lists(
          cur_course_assig_comb
        )
      );
      generated_distribution->penalty -=
        cur_course_assig_comb->penalty_cancellation;

      if (cur_course_idx == dll_tail_idx(tas_profs_courses_combinations))
      // base case of recursion: cur_course_idx is the last course
      {
        assignment_distribution* copy_generated_distribution =
          deep_copy_assignment_distribution(
            generated_distribution
          );

        assign_students_to_distribution(
          copy_generated_distribution,
          all_students
        );

        if (
          copy_generated_distribution->penalty <
          (*(optimal_schedule_so_far))->penalty
          )
        // generated schedule is more optimal then the most optimal so far
        {
          free_assignment_distribution(*optimal_schedule_so_far);
          *optimal_schedule_so_far = deep_copy_assignment_distribution(
            copy_generated_distribution
          );
        }

        free_assignment_distribution(copy_generated_distribution);
      }
      else
      // recursive case: cur_course_idx is not the index of combinations for the
      // last course
      {
        optimize_schedule_recursively(
          cur_course_idx + 1,
          tas_profs_courses_combinations,
          all_students,
          optimal_schedule_so_far,
          generated_distribution
        );
      }

      generated_distribution->penalty +=
        cur_course_assig_comb->penalty_cancellation;
      dll_remove_last(generated_distribution->assigned_courses);
    }
  }
}

assignment_distribution* find_any_most_optimal_schedule(schedule_input* input)
{
  assignment_distribution* result =
    construct_empty_assignment_distribution_with_penalty(
      input->total_penalty
    );

  double_linked_list* tas_profs_courses_combinations =
    get_combinations_of_tas_professors_for_all_courses(
      input->courses,
      input->professors,
      input->tas
    );

  assignment_distribution* generated_distribution =
    construct_empty_assignment_distribution_with_penalty(
      input->total_penalty
    );

  optimize_schedule_recursively(
    0,
    tas_profs_courses_combinations,
    input->students,
    &result,
    generated_distribution
  );

  free_assignment_distribution(generated_distribution);
  free_double_linked_list(tas_profs_courses_combinations);
  return result;
}

#endif //ASSIGNMENT_2_SCHEDULE_SCHEDULE_OPTIMIZATION_H
