#ifndef assignment_2_schedule_final_assignment_state_h
#define assignment_2_schedule_final_assignment_state_h

#include "../double_linked_list/double_linked_list.h"
#include "../model/model_include.h"
#include "../input/schedule_input.h"

typedef
  struct course_assignment
  {
    course* course_obj;
    professor* assigned_professor;
    double_linked_list* assigned_tas;
    double_linked_list* assigned_students;

    unsigned int students_left;
    unsigned int penalty_cancellation;
  }
course_assignment;

course_assignment* construct_course_assignment_with_course_prof(
  course* course_obj,
  professor* assigned_professor
)
{
  course_assignment* result = malloc(sizeof(course_assignment));

  result->course_obj = course_obj;
  result->assigned_professor = assigned_professor;
  result->assigned_tas = null;
  result->assigned_students = null;
  result->students_left = course_obj->max_students;
  result->penalty_cancellation = professor_not_running_course_penalty;

  return result;
}

course_assignment* construct_course_assignment_with_course_tas(
  course* course_obj,
  double_linked_list* assigned_tas
)
{
  course_assignment* result = malloc(sizeof(course_assignment));

  result->course_obj = course_obj;
  result->assigned_professor = null;
  result->assigned_tas = assigned_tas;
  result->assigned_students = null;
//    construct_empty_double_linked_list(
//    sizeof(student)
//  );
  result->students_left = course_obj->max_students;
  result->penalty_cancellation = 0;
  dll_map_for_all(
    assigned_tas,
    calculate_penalty_cancellation_wrapper,
    &(result->penalty_cancellation)
  );

  return result;
}

course_assignment* subtle_copy_course_assignment(
  course_assignment* other
)
{
  course_assignment* result = malloc(sizeof(course_assignment));

  result->course_obj = other->course_obj;
  result->assigned_professor = other->assigned_professor;
  result->assigned_tas = other->assigned_tas;
  result->assigned_students = other->assigned_students;
  result->students_left = other->students_left;
  result->penalty_cancellation = other->penalty_cancellation;

  return result;
}

course_assignment* deep_copy_course_assignment_lists(
  course_assignment* other
)
{
  course_assignment* result = malloc(sizeof(course_assignment));

  result->course_obj = other->course_obj;
  result->assigned_professor = other->assigned_professor;
  result->assigned_tas = (other->assigned_tas) ?
                         copy_double_linked_list(other->assigned_tas):
                         null;
  result->assigned_students = (other->assigned_students) ?
                              copy_double_linked_list(other->assigned_students):
                              null;
  result->students_left = other->students_left;
  result->penalty_cancellation = other->penalty_cancellation;

  return result;
}


void free_course_assignment_leave_course_prof(course_assignment* obj_to_free)
{
  if (obj_to_free->assigned_tas)
  {
    free_double_linked_list(obj_to_free->assigned_tas);
  }

  if (obj_to_free->assigned_students)
  {
    free_double_linked_list(obj_to_free->assigned_students);
  }

  free(obj_to_free);
}

void free_course_assignment_leave_course_prof_wrapper(
  void* course_assignment_to_free
)
{
  free_course_assignment_leave_course_prof(course_assignment_to_free);
}


void print_course_assignment(course_assignment* assignment, file* file_ptr)
{
  if (assignment->assigned_professor)
  {
    fprintf(file_ptr, "%s\n", assignment->course_obj->name);

    fprintf(
      file_ptr,
      "%s %s\n",
      assignment->assigned_professor->name.first_name,
      assignment->assigned_professor->name.last_name
    );

    dll_map_for_all(
      assignment->assigned_tas,
      print_ta_wrapper,
      file_ptr
    );

    dll_map_for_all(
      assignment->assigned_students,
      print_student_wrapper,
      file_ptr
    );

    fprintf(file_ptr, "\n");
  }
}

void print_course_assignment_wrapper(void* assignment, va_list file_p)
{
  file* file_ptr = va_arg(file_p, file*);
  print_course_assignment(assignment, file_ptr);
}


typedef
  struct assignment_distribution
  {
    double_linked_list* assigned_courses;
    unsigned int penalty;
  }
assignment_distribution;

assignment_distribution* construct_empty_assignment_distribution_with_penalty(
  unsigned int initial_penalty
)
{
  assignment_distribution* result = malloc(sizeof(assignment_distribution));

  result->assigned_courses = construct_double_linked_list_with_destr(
    sizeof(course_assignment),
    free_course_assignment_leave_course_prof_wrapper
  );
  result->penalty = initial_penalty;

  return result;
}

void deep_copy_assigned_course_into_list(
  course_assignment* assignment,
  double_linked_list* course_assignments
)
{
  dll_append_value(
    course_assignments,
    deep_copy_course_assignment_lists(assignment)
  );
}

void deep_copy_assigned_course_into_list_wrapper(
  void* assignment,
  va_list course_assigs
)
{
  double_linked_list* course_assignments = va_arg(
    course_assigs,
    double_linked_list*
  );

  deep_copy_assigned_course_into_list(assignment, course_assignments);
}

assignment_distribution* deep_copy_assignment_distribution(
  assignment_distribution* other
)
{
  assignment_distribution* result = malloc(sizeof(assignment_distribution));

  result->assigned_courses = construct_double_linked_list_with_destr(
    sizeof(course_assignment),
    free_course_assignment_leave_course_prof_wrapper
  );
  dll_map_for_all(
    other->assigned_courses,
    deep_copy_assigned_course_into_list_wrapper,
    result->assigned_courses
  );
  result->penalty = other->penalty;

  return result;
}


void free_assignment_distribution(assignment_distribution* obj_to_free)
{
  free_double_linked_list(obj_to_free->assigned_courses);
  free(obj_to_free);
}

void free_assignment_distribution_wrapper(void* assignment_distribution_to_free)
{
  free_assignment_distribution(assignment_distribution_to_free);
}


void print_assignment_distribution(
  assignment_distribution* distribution,
  file* file_ptr
)
{
  dll_map_for_all(
    distribution->assigned_courses,
    print_course_assignment_wrapper,
    file_ptr
  );
}

void print_course_assignment_course_error(
  course_assignment* ca,
  file* file_ptr
)
{
  if (!(ca->assigned_professor))
  {
    fprintf(
      file_ptr,
      "%s cannot be run.\n",
      ca->course_obj->name
    );
  }
}

void print_course_assignment_course_error_wrapper(
  void* ca,
  va_list file_ptr_arg
)
{
  file* file_ptr = va_arg(file_ptr_arg, file*);

  print_course_assignment_course_error(ca, file_ptr);
}

void print_distribution_courses_errors(
  assignment_distribution* distribution,
  file* file_ptr
)
{
  dll_map_for_all(
    distribution->assigned_courses,
    print_course_assignment_course_error_wrapper,
    file_ptr
  );
}

void print_distribution_professor_error(
  professor* prof,
  assignment_distribution* distribution,
  file* file_ptr
)
{
  unsigned char classes_running = 0;
  char* result_error = "is unassigned";

  dll_node* iter_node = distribution->assigned_courses->head;
  while (iter_node)
  {
    course_assignment* cur_assignment = (course_assignment*)iter_node->value;

    if (professor_comparator(prof, cur_assignment->assigned_professor) == 0)
    {
      if (
        dll_find_first(prof->trained_courses, cur_assignment->course_obj->name)
      )
      {
        ++classes_running;

        if (classes_running < professor_max_assigned_trained_courses)
        {
          result_error = "is lacking class";
        }
        else
        {
          result_error = null;
          break;
        }
      }
      else
      {
        fprintf(
          file_ptr,
          "%s %s is not trained for %s.\n",
          prof->name.first_name,
          prof->name.last_name,
          cur_assignment->course_obj->name
        );
        return;
      }
    }

    iter_node = iter_node->right;
  }

  if (result_error)
  {
    fprintf(
      file_ptr,
      "%s %s %s.\n",
      prof->name.first_name,
      prof->name.last_name,
      result_error
    );
  }
}

void print_distribution_professor_error_wrapper(
  void* prof,
  va_list distribution_fileptr
)
{
  assignment_distribution* distribution =
    va_arg(distribution_fileptr, assignment_distribution*);
  file* file_ptr =
    va_arg(distribution_fileptr, file*);

  print_distribution_professor_error(prof, distribution, file_ptr);
}

void print_distribution_professors_errors(
  double_linked_list* all_professors,
  assignment_distribution* distribution,
  file* file_ptr
)
{
  dll_map_for_all(
    all_professors,
    print_distribution_professor_error_wrapper,
    distribution,
    file_ptr
  );
}

void calculate_number_occupied_labs_for_ta_in_distribution(
  course_assignment* course_assig,
  teaching_assistant* ta,
  unsigned int* labs_occupied
)
{
  if (course_assig->assigned_tas)
  {
    teaching_assistant* found_ta = dll_find_first(
      course_assig->assigned_tas,
      &(ta->name)
    );

    if (found_ta)
    {
      *labs_occupied += ta_max_assigned_trained_courses - found_ta->labs_left;
    }
  }
}

void calculate_number_occupied_labs_for_ta_in_distribution_wrapper(
  void* course_assig,
  va_list ta_labsoccup
)
{
  teaching_assistant* ta = va_arg(ta_labsoccup, teaching_assistant*);
  unsigned int* labs_occupied = va_arg(ta_labsoccup, unsigned int*);

  calculate_number_occupied_labs_for_ta_in_distribution(
    course_assig,
    ta,
    labs_occupied
  );
}

void print_distribution_ta_error(
  teaching_assistant* ta,
  assignment_distribution* distribution,
  file* file_ptr
)
{
  unsigned int labs_occupied = 0;

  dll_map_for_all(
    distribution->assigned_courses,
    calculate_number_occupied_labs_for_ta_in_distribution_wrapper,
    ta,
    &labs_occupied
  );

  unsigned int labs_left = ta_max_assigned_trained_courses - labs_occupied;

  if (labs_left)
  {
    fprintf(
      file_ptr,
      "%s %s is lacking %u lab(s).\n",
      ta->name.first_name,
      ta->name.last_name,
      labs_left
    );
  }
}

void print_distribution_ta_error_wrapper(
  void* ta,
  va_list distrib_fileptr
)
{
  assignment_distribution* distribution =
    va_arg(distrib_fileptr, assignment_distribution*);
  file* file_ptr =
    va_arg(distrib_fileptr, file*);

  print_distribution_ta_error(ta, distribution, file_ptr);
}

void print_distribution_tas_errors(
  double_linked_list* tas,
  assignment_distribution* distribution,
  file* file_ptr
)
{
  dll_map_for_all(
    tas,
    print_distribution_ta_error_wrapper,
    distribution,
    file_ptr
  );
}

void remove_assigned_course_from_list_if_student_assigned(
  course_assignment* ca,
  student* stud,
  double_linked_list* result
)
{
  if (dll_find_first(ca->assigned_students, stud->id))
  {
    dll_remove_first_found(result, ca->course_obj->name);
  }
}

void remove_assigned_course_from_list_if_student_assigned_wrapper(
  void* ca,
  va_list stud_result
)
{
  student* stud = va_arg(stud_result, student*);
  double_linked_list* result = va_arg(stud_result, double_linked_list*);

  remove_assigned_course_from_list_if_student_assigned(
    ca,
    stud,
    result
  );
}

void print_student_lack_course_error(
  course* course_to_print,
  student* stud,
  file* file_ptr
)
{
  fprintf(
    file_ptr,
    "%s %s is lacking %s.\n",
    stud->name.first_name,
    stud->name.last_name,
    course_to_print->name
  );
}

void print_student_lack_course_error_wrapper(
  void* course_to_print,
  va_list stud_fileptr
)
{
  student* stud = va_arg(stud_fileptr, student*);
  file* file_ptr = va_arg(stud_fileptr, file*);

  print_student_lack_course_error(course_to_print, stud, file_ptr);
}

void print_distribution_student_error(
  student* stud,
  assignment_distribution* distribution,
  file* file_ptr
)
{
  double_linked_list* student_unassigned_courses =
    copy_double_linked_list(
      stud->requested_courses
    );

  dll_map_for_all(
    distribution->assigned_courses,
    remove_assigned_course_from_list_if_student_assigned_wrapper,
    stud,
    student_unassigned_courses
  );

  dll_map_for_all(
    student_unassigned_courses,
    print_student_lack_course_error_wrapper,
    stud,
    file_ptr
  );

  free_double_linked_list(student_unassigned_courses);
}

void print_distribution_student_error_wrapper(
  void* stud,
  va_list distribution_fileptr
)
{
  assignment_distribution* distribution =
    va_arg(distribution_fileptr, assignment_distribution*);
  file* file_ptr =
    va_arg(distribution_fileptr, file*);

  print_distribution_student_error(stud, distribution, file_ptr);
}

void print_distribution_students_errors(
  double_linked_list* students,
  assignment_distribution* distribution,
  file* file_ptr
)
{
  dll_map_for_all(
    students,
    print_distribution_student_error_wrapper,
    distribution,
    file_ptr
  );
}

void print_distribution_errors(
  assignment_distribution* distribution,
  schedule_input* input,
  file* file_ptr
)
{
  print_distribution_courses_errors(distribution, file_ptr);

  print_distribution_professors_errors(
    input->professors,
    distribution,
    file_ptr
  );

  print_distribution_tas_errors(
    input->tas,
    distribution,
    file_ptr
  );

  print_distribution_students_errors(
    input->students,
    distribution,
    file_ptr
  );
}

#endif //assignment_2_schedule_final_assignment_state_h
