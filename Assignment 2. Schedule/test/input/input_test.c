#include <dirent.h>
#include <unistd.h>

#include <stdio.h>

#include "input/input.h"

#define TEST_FILES_DIR_STR "../test/input/input_files/all"


void print_course_wrapper(void* course_to_print, va_list no_args)
{
  course* cur_course = (course*)course_to_print;
  printf(
    "    %s %u %u\n",
    cur_course->name,
    cur_course->req_labs,
    cur_course->max_students
  );
}

void print_ent_course_wrapper(void* course_to_print, va_list no_args)
{
  course* cur_course = (course*)course_to_print;
  printf(
    "      %s %u %u\n",
    cur_course->name,
    cur_course->req_labs,
    cur_course->max_students
  );
}

void print_prof_wrapper(void* prof_to_print, va_list no_args)
{
  professor* cur_prof = (professor*)prof_to_print;
  printf(
    "    %s %s\n",
    cur_prof->name.first_name,
    cur_prof->name.last_name
  );
  dll_map_for_all(cur_prof->trained_courses, print_ent_course_wrapper);
}

void print_ta_wrapper(void* ta_to_print, va_list no_args)
{
  teaching_assistant* cur_ta = (teaching_assistant*)ta_to_print;
  printf(
    "    %s %s\n",
    cur_ta->name.first_name,
    cur_ta->name.last_name
  );
  dll_map_for_all(cur_ta->trained_courses, print_ent_course_wrapper);
}

void print_student_wrapper(void* student_to_print, va_list no_args)
{
  student* cur_stud = (student*)student_to_print;
  printf(
    "    %s %s %s\n",
    cur_stud->name.first_name,
    cur_stud->name.last_name,
    cur_stud->id
  );
  dll_map_for_all(cur_stud->requested_courses, print_ent_course_wrapper);
}

void print_schedule_input(schedule_input* obj_to_print)
{
  printf("  Maximal possible penalty: %u\n", obj_to_print->total_penalty);
  printf("  Courses:\n");
  dll_map_for_all(obj_to_print->courses, print_course_wrapper);
  printf("  Professors:\n");
  dll_map_for_all(obj_to_print->professors, print_prof_wrapper);
  printf("  TAs:\n");
  dll_map_for_all(obj_to_print->tas, print_ta_wrapper);
  printf("  Students:\n");
  dll_map_for_all(obj_to_print->students, print_student_wrapper);
}


int main()
{
  chdir(TEST_FILES_DIR_STR);
  DIR* dir = opendir(".");

  if (dir)
  {
    struct dirent* dentry;
    while ((dentry = readdir(dir)))
    {
      if (
        (strcmp(dentry->d_name, ".") != 0) &&
        (strcmp(dentry->d_name, "..") != 0)
        )
      {
        printf("[File %s]\n", dentry->d_name);
        FILE* test_file = fopen(dentry->d_name, "r");
        //        FILE* test_file = fopen("empty.txt", "r");

        file_read_status read_status = OK;
        schedule_input* input = read_input_file(test_file, &read_status);

        if (input)
        {
          print_schedule_input(input);
          free_schedule_input(input);
        }

        switch (read_status)
        {
          case OK:
          {
            printf("OK\n\n");
            break;
          }
          case INVALID_STRUCTURE:
          {
            printf("INVALID_STRUCTURE\n\n");
            break;
          }
          case FILE_NOT_EXISTS:
          {
            printf("FILE_NOT_EXISTS\n\n");
            break;
          }
          case WRONG_FILE_NUMBER:
          {
            printf("WRONG_FILE_NUMBER\n\n");
            break;
          }
          default:
          {
            printf("Unknown error occurred\n\n");
          }
        }

        fclose(test_file);
      }
    }

    closedir(dir);
  }

  return 0;
}
