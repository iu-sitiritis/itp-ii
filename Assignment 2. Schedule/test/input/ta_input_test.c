#include <dirent.h>
#include <unistd.h>

#include <stdio.h>

#include "input/input.h"

#define TEST_FILES_DIR_STR "../test/input/input_files/ta"


void print_course_wrapper(void* course_to_print, va_list no_args)
{
  course* cur_course = (course*)course_to_print;
  printf(
    "    %s %u %u\n",
    cur_course->name,
    cur_course->req_labs,
    cur_course->max_students
  );
}

void print_ent_course_wrapper(void* course_to_print, va_list no_args)
{
  course* cur_course = (course*)course_to_print;
  printf(
    "      %s %u %u\n",
    cur_course->name,
    cur_course->req_labs,
    cur_course->max_students
  );
}

void print_prof_wrapper(void* prof_to_print, va_list no_args)
{
  professor* cur_prof = (professor*)prof_to_print;
  printf(
    "    %s %s\n",
    cur_prof->name.first_name,
    cur_prof->name.last_name
  );
  dll_map_for_all(cur_prof->trained_courses, print_ent_course_wrapper);
}

void print_ta_wrapper(void* ta_to_print, va_list no_args)
{
  teaching_assistant* cur_ta = (teaching_assistant*)ta_to_print;
  printf(
    "    %s %s\n",
    cur_ta->name.first_name,
    cur_ta->name.last_name
  );
  dll_map_for_all(cur_ta->trained_courses, print_ent_course_wrapper);
}

int main()
{
  chdir(TEST_FILES_DIR_STR);
  DIR* dir = opendir(".");

  if (dir)
  {
    struct dirent* dentry;
    while ((dentry = readdir(dir)))
    {
      if (
        (strcmp(dentry->d_name, ".") != 0) &&
        (strcmp(dentry->d_name, "..") != 0)
        )
      {
        printf("[File %s]\n", dentry->d_name);
        FILE* test_file = fopen(dentry->d_name, "r");
        //        FILE* test_file = fopen("repeated_course.txt", "r");

        file_read_status read_status;

        double_linked_list* courses =
          read_courses_from_input_file(test_file, &read_status);

        if (read_status == OK)
        {
          printf("  Courses:\n");
          dll_map_for_all(courses, print_course_wrapper);

          double_linked_list* professors = read_professors_from_input_file(
            test_file,
            courses,
            &read_status
          );

          if (read_status == OK)
          {
            printf("  Professors:\n");
            dll_map_for_all(professors, print_prof_wrapper);

            double_linked_list* tas = read_tas_from_input_file(
              test_file,
              courses,
              &read_status
            );

            if (tas)
            {
              printf("  Teaching assistants:\n");
              dll_map_for_all(tas, print_ta_wrapper);
              free_double_linked_list(tas);
            }

            switch (read_status)
            {
              case OK:
              {
                printf("OK\n\n");
                break;
              }
              case INVALID_STRUCTURE:
              {
                printf("INVALID_STRUCTURE\n\n");
                break;
              }
              case FILE_NOT_EXISTS:
              {
                printf("FILE_NOT_EXISTS\n\n");
                break;
              }
              case WRONG_FILE_NUMBER:
              {
                printf("WRONG_FILE_NUMBER\n\n");
                break;
              }
              default:
              {
                printf("Unknown error occurred\n\n");
              }
            }

            free_double_linked_list(professors);
          }
          else
          {
            printf("Invalid input at read professors stage\n\n");
          }

          free_double_linked_list(courses);
        }
        else
        {
          printf("Invalid input at read courses stage\n\n");
        }

        fclose(test_file);
      }
    }

    closedir(dir);
  }

  return 0;
}
