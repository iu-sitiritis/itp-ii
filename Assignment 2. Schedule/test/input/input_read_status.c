#include <stdio.h>
#include "../../src/input/input.h"

int main()
{
  file_read_status rfs_p;

  for (unsigned int i = 0; i <= MAX_NUM_FILES + 1; ++i)
  {
    read_input_file(i, &rfs_p);

    printf("[File #%u] ", i);
    switch (rfs_p)
    {
      case OK:
      {
        printf("OK\n");
        break;
      }
      case INVALID_STRUCTURE:
      {
        printf("INVALID_STRUCTURE\n");
        break;
      }
      case FILE_NOT_EXISTS:
      {
        printf("FILE_NOT_EXISTS\n");
        break;
      }
      case WRONG_FILE_NUMBER:
      {
        printf("WRONG_FILE_NUMBER\n");
        break;
      }
      default:
      {
        printf("Unknown error occurred\n");
      }
    }
  }

  return 0;
}

