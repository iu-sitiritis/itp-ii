#include <stdio.h>

#include "input/input.h"

#define NUM_TESTS 13

void print_course_wrapper(void* course_to_print, va_list no_args)
{
  course* cur_course = (course*)course_to_print;
  printf(
    "%s %u %u\n",
    cur_course->name,
    cur_course->req_labs,
    cur_course->max_students
  );
}

int main()
{
  const char test_files[NUM_TESTS][100] ={
    "../test/input/input_files/course/empty.txt",
    "../test/input/input_files/course/invalid_course_name.txt",
    "../test/input/input_files/course/invalid_course_terminator.txt",
    "../test/input/input_files/course/invalid_num_labs.txt",
    "../test/input/input_files/course/invalid_num_students.txt",
    "../test/input/input_files/course/new_line_issue.txt",
    "../test/input/input_files/course/no_course_labs.txt",
    "../test/input/input_files/course/no_course_name.txt",
    "../test/input/input_files/course/no_course_students.txt",
    "../test/input/input_files/course/no_courses.txt",
    "../test/input/input_files/course/same_courses.txt",
    "../test/input/input_files/course/unexpected_delimiter.txt",
    "../test/input/input_files/course/valid_input.txt"
  };

  for (int i = 0; i < NUM_TESTS; ++i)
  {
    file_read_status read_status;

    FILE* f = fopen(test_files[i], "r");
    double_linked_list* courses = read_courses_from_input_file(f, &read_status);
    fclose(f);

    printf("[File %s] ", test_files[i]);
    switch (read_status)
    {
      case OK:
      {
        printf("OK\n");
        break;
      }
      case INVALID_STRUCTURE:
      {
        printf("INVALID_STRUCTURE\n");
        break;
      }
      case FILE_NOT_EXISTS:
      {
        printf("FILE_NOT_EXISTS\n");
        break;
      }
      case WRONG_FILE_NUMBER:
      {
        printf("WRONG_FILE_NUMBER\n");
        break;
      }
      default:
      {
        printf("Unknown error occurred\n");
      }
    }

    if (courses)
    {
      dll_map_for_all(courses, print_course_wrapper);
      free_double_linked_list(courses);
    }
  }

  return 0;
}
