#include <dirent.h>
#include <unistd.h>

#include <stdio.h>

#include "input/input.h"
#include "logic/schedule_optimization.h"


#define TEST_FILES_DIR_STR "../test/input/input_files/all"
// "../test/input/input_files/all"
// "../test/logic/input_files/"


void print_ta_wrapper(void* ta_to_print, va_list no_args)
{
  teaching_assistant* ta = (teaching_assistant*)ta_to_print;

  printf(
    "  Name: %s %s; Labs left: %u\n",
    ta->name.first_name,
    ta->name.last_name,
    ta->labs_left
  );
}

void output_profs_tas_course_assignments_wrapper(void* in_ca, va_list no_args)
{
  course_assignment* ca = (course_assignment*)in_ca;

  printf(
    """Course: %s; Penalty cancellation: %u;\n"""
    """Professor: %s %s\n"""
    """TAs:\n",
    ca->course_obj->name,
    ca->penalty_cancellation,
    (ca->assigned_professor) ? ca->assigned_professor->name.first_name : "",
    (ca->assigned_professor) ? ca->assigned_professor->name.last_name : ""
  );

  if (ca->assigned_tas)
  {
    dll_map_for_all(
      ca->assigned_tas,
      print_ta_wrapper
    );
  }

  printf("\n");
}

void output_profs_tas_course_combinations_wrapper(
  void* course_assignments_list,
  va_list no_args
)
{
  dll_map_for_all(
    (double_linked_list*)course_assignments_list,
    output_profs_tas_course_assignments_wrapper
  );

  printf("-------------------------\n\n");
}


int main()
{
  chdir(TEST_FILES_DIR_STR);
  DIR* dir = opendir(".");

  FILE* test_file = fopen("valid_input1.txt", "r"); //"input12.txt", "r");

  file_read_status read_status = OK;
  schedule_input* input = read_input_file(test_file, &read_status);

  if (input)
  {
    double_linked_list* profs_tas_combs_for_each_course =
      get_combinations_of_tas_professors_for_all_courses(
        input->courses,
        input->professors,
        input->tas
      );

    dll_map_for_all(
      profs_tas_combs_for_each_course,
      output_profs_tas_course_combinations_wrapper
    );

    free_schedule_input(input);
    free_double_linked_list(profs_tas_combs_for_each_course);
  }

  fclose(test_file);
  closedir(dir);

  return 0;
}
