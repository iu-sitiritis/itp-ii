#include <dirent.h>
#include <unistd.h>

#include <stdio.h>

#include "input/input.h"
#include "logic/schedule_optimization.h"


#define TEST_FILES_DIR_STR "../test/logic/input_files/"
// "../test/logic/input_files/"
// "../test/input/input_files/all"

int main()
{
  chdir(TEST_FILES_DIR_STR);
  DIR* dir = opendir(".");

  FILE* test_file = fopen("input42.txt", "r"); //"valid_input1.txt", "r");

  file_read_status read_status = OK;
  schedule_input* input = read_input_file(test_file, &read_status);

  if (input)
  {
    assignment_distribution* distribution = find_first_most_optimal_schedule(
      input
    );

    print_assignment_distribution(distribution, stdout);
    print_distribution_errors(distribution, input, stdout);
    fprintf(stdout, "Total score is %u.", distribution->penalty);

    free_schedule_input(input);
    free_assignment_distribution(distribution);
  }

  fclose(test_file);
  closedir(dir);

  return 0;
}
