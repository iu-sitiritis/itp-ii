#include <dirent.h>
#include <unistd.h>

#include <stdio.h>

#include "logic/schedule_optimization.h"

void print_ta(void* ta_to_print, va_list no_args)
{
  teaching_assistant* ta = (teaching_assistant*)ta_to_print;

  printf(
    "First name: %s; Last name: %s; Num labs left: %u\n",
    ta->name.first_name,
    ta->name.last_name,
    ta->labs_left
  );
}

void print_ta_combinations(void* ta_combination, va_list no_args)
{
  static unsigned int call_counter = 0;
  ++call_counter;

  printf("Combination #%u:\n", call_counter);
  dll_map_for_all(
    ta_combination,
    print_ta
  );
  printf("\n");
}

int main()
{
  double_linked_list* ta1_courses =
    construct_double_linked_list_with_comp_destr(
      sizeof(course),
      course_string_comparator_wrapper,
      free_course_wrapper
    );

  double_linked_list* ta2_courses =
    construct_double_linked_list_with_comp_destr(
      sizeof(course),
      course_string_comparator_wrapper,
      free_course_wrapper
    );

  double_linked_list* ta3_courses =
    construct_double_linked_list_with_comp_destr(
      sizeof(course),
      course_string_comparator_wrapper,
      free_course_wrapper
    );

  double_linked_list* available_tas =
    construct_double_linked_list_with_comp_destr(
      sizeof(teaching_assistant),
      ta_name_comparator_wrapper,
      free_teaching_assistant_wrapper
    );

  dll_append_value(
    available_tas,
    construct_teaching_assistant(
      "1", "1", ta1_courses
    )
  );

  dll_append_value(
    available_tas,
    construct_teaching_assistant(
      "2", "2", ta2_courses
    )
  );

  dll_append_value(
    available_tas,
    construct_teaching_assistant(
      "3", "3", ta3_courses
    )
  );

  double_linked_list* tas_to_assign =
    construct_double_linked_list_with_comp_destr(
      sizeof(teaching_assistant),
      ta_name_comparator_wrapper,
      free_teaching_assistant_wrapper
    );

  double_linked_list* ta_combinations =
    construct_double_linked_list_with_destr(
      sizeof(double_linked_list*),
      free_double_linked_list_wrapper
    );

  get_combinations_of_tas_for_labs_backtrack(
    available_tas,
    tas_to_assign,
    5,
    4,
    ta_combinations
  );

  dll_map_for_all(
    ta_combinations,
    print_ta_combinations
  );

  free_double_linked_list(ta_combinations);
  free_double_linked_list(tas_to_assign);
  free_double_linked_list(available_tas);
  free_double_linked_list(ta1_courses);
  free_double_linked_list(ta2_courses);
  free_double_linked_list(ta3_courses);

  return 0;
}
