#include <dirent.h>
#include <unistd.h>

#include <stdio.h>

#include "input/input.h"
#include "logic/schedule_optimization.h"

#define TEST_FILES_DIR_STR "../test/input/input_files/all"
// "../test/logic/input_files"


void output_profs_course_assignments_wrapper(void* in_ca, va_list no_args)
{
  course_assignment* ca = (course_assignment*)in_ca;

  professor* assigned_prof = ca->assigned_professor;
  printf(
    "Course: %s; Assigned prof: %s %s\n",
    ca->course_obj->name,
    (assigned_prof) ? assigned_prof->name.first_name : "NULL",
    (assigned_prof) ? assigned_prof->name.last_name : "NULL"
  );
}

int main()
{
  chdir(TEST_FILES_DIR_STR);
  DIR* dir = opendir(".");

  FILE* test_file = fopen("valid_input1.txt", "r"); // input12.txt

  file_read_status read_status = OK;
  schedule_input* input = read_input_file(test_file, &read_status);

  if (input)
  {
    double_linked_list* course_assignments = assign_professors_to_course(
      input->courses->head->value,
      input->professors
    );

    dll_map_for_all(
      course_assignments,
      output_profs_course_assignments_wrapper
    );

    free_schedule_input(input);
    free_double_linked_list(course_assignments);
  }

  fclose(test_file);
  closedir(dir);

  return 0;
}
