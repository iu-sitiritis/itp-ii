#include <dirent.h>
#include <unistd.h>

#include <stdio.h>

#include "input/input.h"
#include "logic/schedule_optimization.h"


#define TEST_FILES_DIR_STR "../test/logic/input_files/"
// "../test/logic/input_files/"
// "../test/input/input_files/all"


void print_ta_wrapper(void* ta_to_print, va_list no_args)
{
  teaching_assistant* ta = (teaching_assistant*)ta_to_print;

  printf(
    "    Name: %s %s; Labs left: %u\n",
    ta->name.first_name,
    ta->name.last_name,
    ta->labs_left
  );
}

void print_assigned_students_wrapper(void* student_to_print, va_list no_args)
{
  student* stud = (student*)student_to_print;

  printf(
    "    %s %s\n",
    stud->name.first_name,
    stud->name.last_name
  );
}

void output_profs_tas_course_assignments_wrapper(void* in_ca, va_list no_args)
{
  course_assignment* ca = (course_assignment*)in_ca;

  printf(
    """Course: %s;\n"""
    """Penalty cancellation: %u;\n"""
    """  Professor: %s %s\n"""
    """  TAs:\n",
    ca->course_obj->name,
    ca->penalty_cancellation,
    (ca->assigned_professor) ? ca->assigned_professor->name.first_name : "",
    (ca->assigned_professor) ? ca->assigned_professor->name.last_name : ""
  );

  if (ca->assigned_tas)
  {
    dll_map_for_all(
      ca->assigned_tas,
      print_ta_wrapper
    );
  }

  printf("  Students:\n");

  if (ca->assigned_students)
  {
    dll_map_for_all(
      ca->assigned_students,
      print_assigned_students_wrapper
    );
  }

  printf("\n");
}

void print_distribution(assignment_distribution* distribution)
{
  printf("Penalty: %u\n\n", distribution->penalty);
  dll_map_for_all(
    distribution->assigned_courses,
    output_profs_tas_course_assignments_wrapper
  );
}

int main()
{
  chdir(TEST_FILES_DIR_STR);
  DIR* dir = opendir(".");

  FILE* test_file = fopen("input43.txt", "r");//"input43.txt", "r");//"valid_input1.txt", "r");

  file_read_status read_status = OK;
  schedule_input* input = read_input_file(test_file, &read_status);

  if (input)
  {
    assignment_distribution* distribution = find_first_most_optimal_schedule(
      input
    );

    print_distribution(distribution);

    free_schedule_input(input);
    free_assignment_distribution(distribution);
  }

  fclose(test_file);
  closedir(dir);

  return 0;
}
