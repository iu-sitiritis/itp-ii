#include <stdio.h>
#include "double_linked_list/double_linked_list.h"

#define LIST_SIZE (10)

void add(int* val, int value_to_add)
{
  *val += value_to_add;
}

void add_wrapper(void* value, va_list args)
{
  int value_to_add = va_arg(args, int);
  add((int*) value, value_to_add);
}

int main()
{
  double_linked_list* dll = construct_empty_double_linked_list(sizeof(int));

  for (int i = 0; i < LIST_SIZE; ++i)
  {
    dll_append(dll, &i);
  }

  double_linked_list* copy = copy_double_linked_list(dll);

  dll_map_for_all(dll, add_wrapper, 5);

  for (int i = 0; i < LIST_SIZE; ++i)
  {
    printf("%d\n", *((int*)dll_get_at(dll, i)));
  }

  for (int i = 0; i < LIST_SIZE; ++i)
  {
    dll_remove_first(dll);
  }

  for (int i = 0; i < LIST_SIZE; ++i)
  {
    printf("%d\n", *((int*)dll_get_at(copy, i)));
  }

  printf("Size of the first list after clean up = %u\n", dll->size);

  free_double_linked_list(dll);
  return 0;
}
