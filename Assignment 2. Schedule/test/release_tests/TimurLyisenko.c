#include <stdio.h>
#include <stdarg.h>
#include <memory.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>
#include <ctype.h>
#include <stddef.h>
#include <ctype.h>

#ifndef ASSIGNMENT_2_SCHEDULE_COMMON_H
#define ASSIGNMENT_2_SCHEDULE_COMMON_H

#define STR(tok) #tok
#define STR_VAL(tok) STR(tok)

#endif //ASSIGNMENT_2_SCHEDULE_COMMON_H

#ifndef DLL_NODE_H
#define DLL_NODE_H

/*!
 * @brief Represents a node in double linked list
 */
typedef
  struct dll_node
dll_node;

/*!
 * @brief Represents a node in double linked list
 */
struct dll_node
{
  /*!
   * @brief value Value of any type, stored within the current node
   */
  void* value;

  /*!
   * @brief left Pointer to a node on the left of the current
   */
  dll_node* left;
  dll_node* right;

  /*!
   * @brief Indicates if there was allocated memory for the value and value was
   * copied. Used to know if the value needs to be freed. Managed by the
   * functions, needs not to be modified by the user.
   */
  char value_allocated : 1;
};

/*!
 * @brief Creates a new dll_node with a memcopy of a value pointed by
 * <code>value</code>, left & right, which can be NULL
 * @param value Value stored within the created node
 * @param left Pointer to the node that is considered to be on the left from
 * the node being created
 * @param right Pointer to the node that is considered to be on the left from
 * the node being created
 * @return A newly created node with given assigned members
 */
dll_node* construct_dll_node_copy(
  void* value,
  const unsigned int type_size,
  dll_node* left,
  dll_node* right
);

/*!
 * @brief Creates a new dll_node with a memcopy of a value pointed by
 * <code>value</code>, that is not connected to
 * any other nodes
 * @param value Value stored within the created node
 * @return A newly created node with given assigned members
 */
dll_node* construct_single_dll_node_with_value_copy(
  void* value,
  const unsigned int type_size
);

/**
 * @brief Creates a new dll_node with a memcopy of a value pointed by
 * <code>value</code> & right, which can be NULL
 * @param value Value stored within the created node
 * @param right Pointer to the node that is considered to be on the left from
 * the node being created
 * @return A newly created node with given assigned members
 */
dll_node* construct_dll_node_with_value_right_copy(
  void* value,
  const unsigned int type_size,
  dll_node* right
);

/*!
 * @brief Creates a new dll_node with a memcopy of a value pointed by
 * <code>value</code> & left, which can be NULL
 * @param value Value stored within the created node
 * @param left Pointer to the node that is considered to be on the left from
 * the node being created
 * @return A newly created node with given assigned members
 */
dll_node* construct_dll_node_with_value_left_copy(
  void* value,
  const unsigned int type_size,
  dll_node* left
);

/*!
 * @brief Creates a new dll_node with a given value, left & right,
 * which can be NULL
 * @param value Value stored within the created node
 * @param left Pointer to the node that is considered to be on the left from
 * the node being created
 * @param right Pointer to the node that is considered to be on the left from
 * the node being created
 * @return A newly created node with given assigned members
 */
dll_node* construct_dll_node(
  void* value,
  dll_node* left,
  dll_node* right
);

/*!
 * @brief Creates a new dll_node with a given value, that is not connected to
 * any other nodes
 * @param value Value stored within the created node
 * @return A newly created node with given assigned members
 */
dll_node* construct_single_dll_node_with_value(
  void* value
);

/**
 * @brief Creates a new dll_node with a given value & right, which can be NULL
 * @param value Value stored within the created node
 * @param right Pointer to the node that is considered to be on the left from
 * the node being created
 * @return A newly created node with given assigned members
 */
dll_node* construct_dll_node_with_value_right(
  void* value,
  dll_node* right
);

/*!
 * @brief Creates a new dll_node with a given value & left, which can be NULL
 * @param value Value stored within the created node
 * @param left Pointer to the node that is considered to be on the left from
 * the node being created
 * @return A newly created node with given assigned members
 */
dll_node* construct_dll_node_with_value_left(
  void* value,
  dll_node* left
);


/*!
 * @brief Creates a new empty dll_node. It contains no value, left & right
 * (all are NULL-s)
 * @return A newly created empty node
 */
dll_node* construct_empty_dll_node(void);


/*!
 * @brief Frees memory allocated to a given dll_node
 * @param node_to_free Pointer to node which must be freed
 * @param destructor Function that frees memory for a value stored in the node.
 * If null, uses free function instead
 */
void free_dll_node(dll_node* node_to_free, void (*destructor)(void* value));

/*!
 * @brief Wrapper to <code>free_dll_node</code> to be used by
 * <code>dll_map_for_all_nodes</code> to release memory allocated to nodes of a
 * list.
 * @see <code>free_dll_node</code>
 * @see <code>dll_map_for_all_nodes</code>
 */
void free_dll_node_wrapper(dll_node* node_to_free, va_list args);

#endif // DLL_NODE_H

dll_node* construct_dll_node_copy(
  void* value,
  const unsigned int type_size,
  dll_node* left,
  dll_node* right
)
{
  dll_node* new_dll_node = malloc(sizeof(dll_node));
  new_dll_node->value = malloc(type_size);

  memcpy(new_dll_node->value, value, type_size);
  new_dll_node->left = left;
  new_dll_node->right = right;
  new_dll_node->value_allocated = 1;

  return new_dll_node;
}

dll_node* construct_single_dll_node_with_value_copy(
  void* value,
  const unsigned int type_size
)
{
  return construct_dll_node_copy(value, type_size, NULL, NULL);
}

dll_node* construct_dll_node_with_value_right_copy(
  void* value,
  const unsigned int type_size,
  dll_node* right
)
{
  return construct_dll_node_copy(value, type_size, NULL, right);
}

dll_node* construct_dll_node_with_value_left_copy(
  void* value,
  const unsigned int type_size,
  dll_node* left
)
{
  return construct_dll_node_copy(value, type_size, left, NULL);
}

dll_node* construct_dll_node(
  void* value,
  dll_node* left,
  dll_node* right
)
{
  dll_node* new_dll_node = malloc(sizeof(dll_node));

  new_dll_node->value = value;
  new_dll_node->left = left;
  new_dll_node->right = right;
  new_dll_node->value_allocated = 0;

  return new_dll_node;
}

dll_node* construct_single_dll_node_with_value(
  void* value
)
{
  construct_dll_node(value, NULL, NULL);
}

dll_node* construct_dll_node_with_value_right(
  void* value,
  dll_node* right
)
{
  construct_dll_node(value, NULL, right);
}

dll_node* construct_dll_node_with_value_left(
  void* value,
  dll_node* left
)
{
  construct_dll_node(value, left, NULL);
}

dll_node* construct_empty_dll_node(void)
{
  construct_dll_node(NULL, NULL, NULL);
}


void free_dll_node(
  dll_node* node_to_free,
  void (*destructor)(void* value)
)
{
  if (destructor)
  {
    destructor(node_to_free->value);
  }
  else if (node_to_free->value_allocated)
  {
    free(node_to_free->value);
  }

  free(node_to_free);
}

void free_dll_node_wrapper(dll_node* node_to_free, va_list args)
{
  void (*destructor)(void* value) = va_arg(
    args,
    void (*)(void* value)
  );

  free_dll_node(node_to_free, destructor);
}

#ifndef DOUBLE_LINKED_LIST_H
#define DOUBLE_LINKED_LIST_H

/*!
 * @brief Structure that implements a double linked list for any type
 * and utilizes an auxiliary pointer to its node - cursor, value of which
 * changes to point to a node that was accessed the last by different
 * operations on the list.
 */
typedef
  struct double_linked_list
  {
    /*!
     * @brief Number of elements in the list.
     */
    unsigned int size;
    /*!
     * @brief Size in bytes of a type stored in the list.
     */
    unsigned int type_size;

    #define dll_head_idx (0)
    #define dll_tail_idx(LIST_PTR) \
    ((LIST_PTR->size == 0) ? 0 : (LIST_PTR->size - 1))
    /*!
     * @brief Index of the node currently pointed by the cursor
     * in the list. If there are no elements in the list - cursor_idx == 0
     */
    unsigned int cursor_idx;

    /*!
     * @brief Points to the first node in the linked list.
     */
    dll_node* head;
    /*!
     * @brief Points to the last element in the linked list.
     */
    dll_node* tail;
    /*!
     * @brief Points to the last accessed element
     * by the operations performed on the list, such as add, get, etc.
     */
    dll_node* cursor;

    /*!
     * @brief Comparator for the elements stored in the list. Can be NULL.
     * If it is NULL, then all operations which require to compare elements
     * have no effect and behave as there is nothing to compare.
     */
    const int (*comp)(const void* left, const void* right);
    /*!
     * @brief Destructor for the pointer to the stored element. NULL, by
     * default, so usual free(value) is used.
     * @param value Pointer to the value to be freed.
     */
    void (*destr)(void* value);
  }
double_linked_list;


/*!
 * @brief Constructs a new empty double linked list, initialized with given
 * comparator and destructor.
 * @param type_size Size of a type that is stored in the list.
 * @param comp Comparator for elements stored in the list.
 * @param destr Destructor for elements stored in the list.
 * @return A pointer to the newly constructed double linked list.
 */
double_linked_list* construct_double_linked_list_with_comp_destr(
  const unsigned int type_size,
  const int (*comp)(const void* left, const void* right),
  void (*destr)(void* value)
);

/*!
 * @brief Constructs a new empty double linked list, initialized with given
 * comparator.
 * @param type_size Size of a type that is stored in the list.
 * @param comp Comparator for elements stored in the list.
 * @return A pointer to the newly constructed double linked list.
 */
double_linked_list* construct_double_linked_list_with_comp(
  const unsigned int type_size,
  const int (*comp)(const void* left, const void* right)
);

/*!
 * @brief Constructs a new empty double linked list, initialized with given
 * destructor.
 * @param type_size Size of a type that is stored in the list.
 * @param destr Destructor for elements stored in the list.
 * @return A pointer to the newly constructed double linked list.
 */
double_linked_list* construct_double_linked_list_with_destr(
  const unsigned int type_size,
  void (*destr)(void* value)
);

/*!
 * @brief Constructs a new empty double linked list,
 * initialized with default values.
 * @param type_size Size of a type that is stored in the list.
 * @return A pointer to the newly constructed double linked list.
 */
double_linked_list* construct_empty_double_linked_list(
  const unsigned int type_size
);

/*!
 * @brief Copies a given double linked list and returns a pointer to the double
 * linked list where it was copied.
 * @param list_to_copy List to copy from.
 * @return Pointer to a new double linked list with copied data.
 */
double_linked_list* copy_double_linked_list(double_linked_list* list_to_copy);


/*!
 * @brief Frees memory used by a double linked list.
 * @param dll List to free.
 */
void free_double_linked_list(double_linked_list* dll);

/*!
 * @brief Frees memory used by a double linked list.
 * @param dll List to free.
 */
void free_double_linked_list_wrapper(void* dll);


/*!
 * @brief Checks if a given double linked list is empty.
 * @param this List to perform the action for.
 * @return Returns a non-zero value if list is empty, otherwise - 0
 */
static inline const int dll_is_empty(const double_linked_list* this)
{
  return (this->size == 0);
}

/*!
 * @brief Checks if a given index is valid (that is, there is
 * an element at the index in the list) for a given double linked list.
 * @param this List to perform the action for.
 * @param idx Index to perform the action for.
 * @return Non-zero value if index is valid, 0 - otherwise.
 */
static inline const int dll_is_index_valid(
  const double_linked_list* this,
  const unsigned int idx
)
{
  return (idx < this->size);
}

/*!
 * @brief Checks if a given range [idx_from, idx_to] is,
 * valid (that is all values in range [idx_from, idx_to] are valid indexes and
 * idx_from <= idx_to) for a given double linked list.
 * @param this List to perform the action for.
 * @param idx_from The left value of a given range.
 * @param idx_to The right value of a given range.
 * @return Non-zero value if range is valid, 0 - otherwise.
 */
static inline const int dll_is_range_valid(
  const double_linked_list* this,
  const unsigned int idx_from,
  const unsigned int idx_to
)
{
  return dll_is_index_valid(this, idx_from) &&
         dll_is_index_valid(this, idx_to) &&
         (idx_from <= idx_to);
}


/*!
 * @brief Adds a copy of a given element (pointed by value) at the beginning of
 * a given double linked list.
 * @param this Double linked list @ which an element is to be inserted.
 * @param value Pointer to value to be copied into the list.
 * @return Pointer to the given list.
 */
double_linked_list* dll_prepend_copy(double_linked_list* this, void* value);

/*!
 * @brief Adds a given element at the beginning of a given double linked list.
 * @param this Double linked list @ which an element is to be inserted.
 * @param value Pointer to value, which (pointer) will be added into the list.
 * @return Pointer to the given list.
 */
double_linked_list* dll_prepend_value(double_linked_list* this, void* value);

/*!
 * @brief Adds a copy of a given element (pointed by value) at the end of
 * a given double linked list.
 * @param this Double linked list @ which an element is to be inserted.
 * @param value Pointer to value to be copied into the list.
 * @return Pointer to the given list.
 */
double_linked_list* dll_append_copy(double_linked_list* this, void* value);

/*!
 * @brief Adds a given element at the beginning of a given double linked list.
 * @param this Double linked list @ which an element is to be inserted.
 * @param value Pointer to value, which (pointer) will be added into the list.
 * @return Pointer to the given list.
 */
double_linked_list* dll_append_value(double_linked_list* this, void* value);

/*!
 * @brief Adds a copy of a given element (pointed by value) at a given index, so
 * that it is guaranteed for the element to reside at the index and the previous
 * element will have its index = index + 1. If index is invalid, then no value
 * is inserted.
 * @param this Double linked list @ which an element is to be inserted.
 * @param value Value to be inserted.
 * @param idx Index on which the operation is to be performed.
 * @return Pointer to the given list. If a given index <code>idx</code> is
 * invalid, NULL is returned.
 */
double_linked_list* dll_add_copy_at_idx(
  double_linked_list* this,
  void* value,
  const unsigned int idx
);

/*!
 * @brief Adds a copy of a given element (pointed by value) at a given index, so
 * that it is guaranteed for the element to reside at the index and the previous
 * element will have its index = index + 1. If index is invalid, then no value
 * is inserted.
 * @param this Double linked list @ which an element is to be inserted.
 * @param value Value to be inserted.
 * @param idx Index on which the operation is to be performed.
 * @return Pointer to the given list. If a given index <code>idx</code> is
 * invalid, NULL is returned.
 */
double_linked_list* dll_add_value_at_idx(
  double_linked_list* this,
  void* value,
  const unsigned int idx
);


/*!
 * @brief Removes all elements from a given double linked list and sets related
 * members to appropriate values.
 * @param this List from which the element is to be removed.
 * @return Always returns a pointer to the given list.
 */
double_linked_list* dll_clear(double_linked_list* this);

/*!
 * @brief Removes the very first element from a given double linked list. If the
 * list is empty - nothing happens.
 * @param this List from which the element is to be removed.
 * @return Always returns a pointer to the given list.
 */
double_linked_list* dll_remove_first(double_linked_list* this);

/*!
 * @brief Removes the very last element from a given double linked list. If the
 * list is empty - nothing happens.
 * @param this List from which the element is to be removed.
 * @return Always returns a pointer to the given list.
 */
double_linked_list* dll_remove_last(double_linked_list* this);

/*!
 * @brief Removes an element at a given index in a given list. If the
 * list is empty or index is not valid - nothing happens.
 * @param this List from which the element is to be removed.
 * @param idx Index on which the operation is to be performed.
 * @return Always returns a pointer to the given list.
 */
double_linked_list* dll_remove_at_idx(
  double_linked_list* this,
  const unsigned int idx
);

/*!
 * @brief Removes the first element, value of which matches with a given value
 * from a given list. If the list is empty or has no elements matched with a
 * given value - nothing happens. If such value was deleted - cursor and its
 * index will be set to the node before the deleted one and its index
 * respectively if it was not the head, otherwise, sets cursor and its index to
 * the head.
 * @param this List from which the element is to be removed.
 * @param value Value first entry of which is to be removed from the list.
 * @return Always returns a pointer to the given list.
 */
double_linked_list* dll_remove_first_found(
  double_linked_list* this,
  const void* value
);

/*!
 * @brief Removes all elements from a given list, value of which match with
 * a given value. If the list is empty or has no elements matched with a
 * given value - nothing happens. If such value was deleted - cursor and its
 * index will be set to the node before the first deleted one and its index
 * respectively if it was not the head, otherwise, sets cursor and its index to
 * the head.
 * @param this List from which the element is to be removed.
 * @param value Value to match elements against.
 * @return Always returns a pointer to the given list.
 */
double_linked_list* dll_remove_all_found(
  double_linked_list* this,
  const void* value
);


/*!
 * @brief Returns a void* to the element at a given index in a given list.
 * @param this List to perform the action for.
 * @param idx Index to perform the action for.
 * @return void* to an element at index <code>idx</code>. If <code>idx</code>
 * is not valid - returns NULL.
 */
void* dll_get_at(double_linked_list* this, const unsigned int idx);

/*!
 * @brief Traverses a given double linked list, until it finds an element with
 * value <code>value</code>. If such value was found - cursor and its index
 * will be set to the found node containing the element and its index
 * respectively.
 * @param this List to perform the action for.
 * @param value Value to search for.
 * @return Value stored in the list equal to the given. If no such element was
 * found or comp is NULL - returns NULL.
 */
void* dll_find_first(double_linked_list* this, const void* value);


/*!
 * @brief Applies a given function to value of each element of a given list.
 * @param this List to perform the action for.
 * @param func_wrapper Function to be applied to each element. The function is a
 * wrapper to the original one, taking its arguments packed in
 * <code>va_list</code>. Inside, the wrapper should unpack the list and pass
 * each unpacked argument to the function it wraps function.
 * @param ... Other arguments that will be passed to a given function. These
 * arguments are forwarded directly to a given function without being copied. If
 * their number does not match with one required by <code>func_wrapper</code>,
 * the behavior is undefined.
 */
void dll_map_for_all(
  double_linked_list* this,
  void (*const func_wrapper)(void* this, va_list args),
  ...
);

#endif // DOUBLE_LINKED_LIST_H

/*!
 * @brief Works the same as <code>dll_map_for_all</code> except that the
 * <code>func_wrapper</code> is applied to nodes of a given list.
 * @see <code>dll_map_for_all</code>
 */
static void dll_map_for_all_nodes(
  double_linked_list* this,
  void (*const func_wrapper)(dll_node* this, va_list args),
  ...
)
{
  dll_node* cur_node = this->head;

  while (cur_node)
  {
    dll_node* right_node = cur_node->right;

    va_list args;
    va_start(args, func_wrapper);

    func_wrapper(cur_node, args);

    va_end(args);

    cur_node = right_node;
  }
}


double_linked_list* construct_double_linked_list_with_comp_destr(
  const unsigned int type_size,
  const int (*comp)(const void* left, const void* right),
  void (*destr)(void* value)
)
{
  double_linked_list* result = malloc(sizeof(double_linked_list));

  result->size = 0;
  result->type_size = type_size;
  result->head = NULL;
  result->tail = NULL;
  result->cursor = NULL;
  result->cursor_idx = 0;
  result->comp = comp;
  result->destr = destr;

  return result;
}

double_linked_list* construct_double_linked_list_with_comp(
  unsigned int type_size,
  const int (*comp)(const void* left, const void* right)
)
{
  return construct_double_linked_list_with_comp_destr(type_size, comp, NULL);
}

double_linked_list* construct_double_linked_list_with_destr(
  const unsigned int type_size,
  void (*destr)(void* value)
)
{
  return construct_double_linked_list_with_comp_destr(type_size, NULL, destr);
}

double_linked_list* construct_empty_double_linked_list(
  const unsigned int type_size
)
{
  return construct_double_linked_list_with_comp_destr(type_size, NULL, NULL);
}

/*!
 * @brief Auxiliary function used to perform a copying of a list.
 * @param value Value to append to a given list.
 * @param list Double linked list to copy to.
 * @see <code>dll_append</code>
 * @see <code>copy_double_linked_list</code>
 */
static void dll_append_reversed_arg_wrapper(
  dll_node* node_to_copy,
  va_list list
)
{
  double_linked_list* list_to_copy_to = va_arg(list, double_linked_list*);

  if (node_to_copy->value_allocated)
  {
    dll_append_copy(list_to_copy_to, node_to_copy->value);
  }
  else
  {
    dll_append_value(list_to_copy_to, node_to_copy->value);
  }
}

double_linked_list* copy_double_linked_list(double_linked_list* list_to_copy)
{
  double_linked_list* result = construct_double_linked_list_with_comp_destr(
    list_to_copy->type_size,
    list_to_copy->comp,
    list_to_copy->destr
  );

  // copy value from list_to_copy to result
  dll_map_for_all_nodes(
    list_to_copy,
    dll_append_reversed_arg_wrapper,
    result
  );

  return result;
}


void free_double_linked_list(double_linked_list* dll)
{
  dll_map_for_all_nodes(dll, free_dll_node_wrapper, dll->destr);
  free(dll);
}

void free_double_linked_list_wrapper(void* dll)
{
  free_double_linked_list(dll);
}


/*!
 * @brief Traverses a list starting at <code>from_node</code> which has index
 * <code>from_node_idx</code> in the right direction until index
 * <code>to_node_idx</code> is reached. If range [<code>from_node_idx</code>,
 * <code>to_node_idx</code>] is invalid - returns NULL. Sets cursor to the
 * node at <code>to_node_idx</code> in the list.
 * @param this List to perform the action for.
 * @param from_node Node at which traversal is started.
 * @param from_node_idx Index of <code>from_node</code> in <code>this</code>.
 * @param to_node_idx Index at which traversal stops.
 * @return Node at index <code>to_node_idx</code>. If a given range is invalid -
 * returns NULL.
 */
static dll_node* dll_traverse_right_from_to(
  double_linked_list* this,
  dll_node* from_node,
  unsigned int from_node_idx,
  unsigned int to_node_idx
)
{
  dll_node* result = NULL;

  if (dll_is_range_valid(this, from_node_idx, to_node_idx))
  {
    while (from_node_idx < to_node_idx)
    {
      from_node = from_node->right;
      ++from_node_idx;
    }

    result = (this->cursor = from_node);
    this->cursor_idx = from_node_idx;
  }

  return result;
}

/*!
 * @brief Traverses a list starting at <code>from_node</code> which has index
 * <code>from_node_idx</code> in the left direction until index
 * <code>to_node_idx</code> is reached. If range [<code>to_node_idx</code>,
 * <code>from_node_idx</code>] is invalid - returns NULL. Sets cursor to the
 * node at <code>to_node_idx</code> in the list.
 * @param this List to perform the action for.
 * @param from_node Node at which traversal is started.
 * @param from_node_idx Index of <code>from_node</code> in <code>this</code>.
 * @param to_node_idx Index at which traversal stops.
 * @return Node at index <code>to_node_idx</code>. If a given range is invalid -
 * returns NULL.
 */
static dll_node* dll_traverse_left_from_to(
  double_linked_list* this,
  dll_node* from_node,
  unsigned int from_node_idx,
  unsigned int to_node_idx
)
{
  dll_node* result = NULL;

  if (dll_is_range_valid(this, to_node_idx, from_node_idx))
  {
    while (from_node_idx > to_node_idx)
    {
      from_node = from_node->left;
      --from_node_idx;
    }

    result = (this->cursor = from_node);
    this->cursor_idx = from_node_idx;
  }

  return result;
}

/*!
 * @brief Traverses a list starting from a given node, which has a given index
 * in the list until it reaches the target index. Depending on the
 * <code>from_node_idx</code> and <code>to_node_idx</code> the appropriate
 * traversal is performed (left to right or right to left).
 * @param this List to perform the action for.
 * @param from_node Node at which traversal is started.
 * @param from_node_idx Index of <code>from_node</code> in <code>this</code>.
 * @param to_node_idx Index at which traversal stops.
 * @return Node at index <code>to_node_idx</code>. If a given range is invalid -
 * returns NULL.
 */
static dll_node* dll_traverse_from_to(
  double_linked_list* this,
  dll_node* from_node,
  const unsigned int from_node_idx,
  const unsigned int to_node_idx
)
{
  dll_node* result = NULL;

  if (dll_is_range_valid(this, from_node_idx, to_node_idx))
  {
    result = dll_traverse_right_from_to(
      this,
      from_node,
      from_node_idx,
      to_node_idx
    );
  }
  else if (dll_is_range_valid(this, to_node_idx, from_node_idx))
  {
    result = dll_traverse_left_from_to(
      this,
      from_node,
      from_node_idx,
      to_node_idx
    );
  }

  return result;
}

/*!
 * @brief Works the same as <code>dll_get_at</code>, except that it returns
 * a node itself.
 * @return Node at index <code>idx</code>. If <code>idx</code> is not valid -
 * returns NULL.
 * @see <code>dll_get_at</code>
 */
static dll_node* dll_get_node_at(
  double_linked_list* this,
  const unsigned int idx
)
{
  dll_node* result_node = NULL;

  if (dll_is_index_valid(this, idx))
  {
    const unsigned int dist_from_head = idx;
    const unsigned int dist_from_cursor = labs(
      (long)(this->cursor_idx) - idx
    );
    const unsigned int dist_from_tail = dll_tail_idx(this) - idx;

    if (
      (dist_from_cursor <= dist_from_head) &&
      (dist_from_cursor <= dist_from_tail)
    )
    {
      result_node = dll_traverse_from_to(
        this,
        this->cursor,
        this->cursor_idx,
        idx
      );
    }
    else if (dist_from_head <= dist_from_tail)
    {
      result_node = dll_traverse_from_to(this, this->head, dll_head_idx, idx);
    }
    else
    {
      result_node = dll_traverse_from_to(
        this,
        this->tail,
        dll_tail_idx(this),
        idx
      );
    }
  }

  return result_node;
}

/*!
 * @brief Traverses a given double linked list from the beginning and returns
 * a node, such that its value is equal to the given, i. e.
 * comp(node->value, value) == 0. If such node was found - cursor and its index
 * will be set to the found element and its index respectively.
 * @param this List to perform the action for.
 * @param value Value to compare against.
 * @return Node with value stored equal to the given. If no such node was found
 * or comp is NULL - returns NULL.
 */
static dll_node* dll_find_first_node(
  double_linked_list* this,
  const void* value
)
{
  dll_node* cur_node = NULL;

  if (this->comp)
  {
    cur_node = this->head;
    unsigned int cur_node_idx = dll_head_idx;

    while (cur_node)
    {
      if ((this->comp(cur_node->value, value)) == 0)
      {
        this->cursor = cur_node;
        this->cursor_idx = cur_node_idx;
        break;
      }

      cur_node = cur_node->right;
      ++cur_node_idx;
    }
  }

  return cur_node;
}


double_linked_list* dll_prepend_copy(double_linked_list* this, void* value)
{
  this->cursor = construct_dll_node_with_value_right_copy(
    value,
    this->type_size,
    this->head
  );
  this->cursor_idx = dll_head_idx;

  if (dll_is_empty(this))
  {
    this->tail = this->cursor;
  }
  else
  {
    this->head->left = this->cursor;
  }

  this->head = this->cursor;
  ++(this->size);

  return this;
}

double_linked_list* dll_prepend_value(double_linked_list* this, void* value)
{
  this->cursor = construct_dll_node_with_value_right(
    value,
    this->head
  );
  this->cursor_idx = dll_head_idx;

  if (dll_is_empty(this))
  {
    this->tail = this->cursor;
  }
  else
  {
    this->head->left = this->cursor;
  }

  this->head = this->cursor;
  ++(this->size);

  return this;
}

double_linked_list* dll_append_copy(double_linked_list* this, void* value)
{
  this->cursor = construct_dll_node_with_value_left_copy(
    value,
    this->type_size,
    this->tail
  );
  this->cursor_idx = dll_tail_idx(this) + 1;

  if (dll_is_empty(this))
  {
    this->head = this->cursor;
  }
  else
  {
    this->tail->right = this->cursor;
  }

  this->tail = this->cursor;
  ++(this->size);

  return this;
}

double_linked_list* dll_append_value(double_linked_list* this, void* value)
{
  this->cursor = construct_dll_node_with_value_left(
    value,
    this->tail
  );
  this->cursor_idx = dll_tail_idx(this) + 1;

  if (dll_is_empty(this))
  {
    this->head = this->cursor;
  }
  else
  {
    this->tail->right = this->cursor;
  }

  this->tail = this->cursor;
  ++(this->size);

  return this;
}

double_linked_list* dll_add_copy_at_idx(
  double_linked_list* this,
  void* value,
  const unsigned int idx
)
{
  double_linked_list* result = NULL;
  dll_node* node_at_idx = dll_get_node_at(this, idx);

  if (node_at_idx)
  {
    if (idx == dll_head_idx)
    {
      dll_prepend_copy(this, value);
    }
    else if (idx == dll_tail_idx(this))
    {
      dll_append_copy(this, value);
    }
    else
    {
      dll_node* prev_node = node_at_idx->left;

      this->cursor = construct_dll_node_copy(
        value,
        this->type_size,
        prev_node,
        node_at_idx
      );
      // this->cursor_idx already has a correct index

      node_at_idx->left = (prev_node->right = this->cursor);
      ++(this->size);
    }

    result = this;
  }

  return result;
}

double_linked_list* dll_add_value_at_idx(
  double_linked_list* this,
  void* value,
  const unsigned int idx
)
{
  double_linked_list* result = NULL;
  dll_node* node_at_idx = dll_get_node_at(this, idx);

  if (node_at_idx)
  {
    if (idx == dll_head_idx)
    {
      dll_prepend_value(this, value);
    }
    else if (idx == dll_tail_idx(this))
    {
      dll_append_value(this, value);
    }
    else
    {
      dll_node* prev_node = node_at_idx->left;

      this->cursor = construct_dll_node(
        value,
        prev_node,
        node_at_idx
      );
      // this->cursor_idx already has a correct index

      node_at_idx->left = (prev_node->right = this->cursor);
      ++(this->size);
    }

    result = this;
  }

  return result;
}


double_linked_list* dll_clear(double_linked_list* this)
{
  dll_map_for_all_nodes(this, free_dll_node_wrapper, this->destr);
  this->size = this->cursor_idx = 0;
  this->cursor = this->head = this->tail = NULL;

  return this;
}

double_linked_list* dll_remove_first(double_linked_list* this)
{
  if (this->size == 1)
  {
    dll_clear(this);
  }
  else
  {
    dll_node* node_to_remove = this->head;

    this->head = (this->cursor = this->head->right);
    if (this->tail == this->head)
    {
      this->tail->left = NULL;
    }
    this->cursor_idx = dll_head_idx;

    --(this->size);

    free_dll_node(node_to_remove, this->destr);
  }

  return this;
}

double_linked_list* dll_remove_last(double_linked_list* this)
{
  if (this->size == 1)
  {
    dll_clear(this);
  }
  else
  {
    dll_node* node_to_remove = this->tail;

    this->tail = (this->cursor = this->tail->left);
    if (this->head == this->tail)
    {
      this->head->right = NULL;
    }
    --(this->size);
    this->cursor_idx = dll_tail_idx(this);

    free_dll_node(node_to_remove, this->destr);
  }

  return this;
}

double_linked_list* dll_remove_at_idx(
  double_linked_list* this,
  const unsigned int idx
)
{
    if (idx == dll_head_idx)
    {
      dll_remove_first(this);
    }
    else if (idx == dll_tail_idx(this))
    {
      dll_remove_last(this);
    }
    else
    {
      dll_node* node_to_remove = dll_get_node_at(this, idx);

      if (node_to_remove)
      {
        this->cursor = (node_to_remove->left->right = node_to_remove->right);
        node_to_remove->right->left = node_to_remove->left;
        --(this->size);

        free_dll_node(node_to_remove, this->destr);
      }
    }

  return this;
}

double_linked_list* dll_remove_first_found(
  double_linked_list* this,
  const void* value
)
{
  if (dll_find_first_node(this, value))
  {
    if (this->cursor == this->head)
    {
      dll_remove_first(this);
    }
    else if (this->cursor == this->tail)
    {
      dll_remove_last(this);
    }
    else
    {
      this->cursor->left->right = this->cursor->right;
      dll_node* new_cursor = (this->cursor->right->left = this->cursor->left);
      free_dll_node(this->cursor, this->destr);
      this->cursor = new_cursor;
      --(this->cursor_idx);
      --(this->size);
    }
  }

  return this;
}

double_linked_list* dll_remove_all_found(
  double_linked_list* this,
  const void* value
)
{
  if (this->comp)
  {
    dll_remove_first_found(this, value);

    dll_node* cur_node = this->cursor;

    while (cur_node == this->head)
    {
      dll_remove_first_found(this, value);
      cur_node = this->cursor;
    }

    cur_node = cur_node->right;

    while (cur_node)
    {
      dll_node* right = cur_node->right;

      if (this->comp(cur_node->value, value) == 0)
      {
        if (cur_node == this->tail)
        {
          dll_remove_last(this);
        }
        else
        {
          cur_node->left->right = right;
          right->left = cur_node->left;
          free_dll_node(cur_node, this->destr);
          --(this->size);
        }
      }

      cur_node = right;
    }
  }

  return this;
}


void* dll_get_at(double_linked_list* this, const unsigned int idx)
{
  dll_node* result_node = dll_get_node_at(this, idx);

  return (result_node) ? result_node->value : NULL;
}

void* dll_find_first(double_linked_list* this, const void* value)
{
  return (dll_find_first_node(this, value)) ? this->cursor->value : NULL;
}


void dll_map_for_all(
  double_linked_list* this,
  void (*const func_wrapper)(void* this, va_list args),
  ...
)
{
  dll_node* cur_node = this->head;

  while (cur_node)
  {
    dll_node* right_node = cur_node->right;

    va_list args;
    va_start(args, func_wrapper);

    func_wrapper(cur_node->value, args);

    va_end(args);

    cur_node = right_node;
  }
}

#ifndef ASSIGNMENT_2_SCHEDULE_BEING_NAME_H
#define ASSIGNMENT_2_SCHEDULE_BEING_NAME_H

#define FIRST_NAME_SIZE 64
#define LAST_NAME_SIZE 64


typedef
  struct being_name
  {
    char first_name[FIRST_NAME_SIZE];
    char last_name[LAST_NAME_SIZE];
  }
being_name;

#endif //ASSIGNMENT_2_SCHEDULE_BEING_NAME_H

#ifndef ASSIGNMENT_2_SCHEDULE_COURSE_H
#define ASSIGNMENT_2_SCHEDULE_COURSE_H

#define COURSE_NAME_SIZE 128

#define COURSE_LEAST_AMOUNT_STUDENTS 1
#define COURSE_LEAST_AMOUNT_LABS 1

#define COURSE_NOT_RUNNING_PENALTY 20


typedef
  struct course
  {
    char name[COURSE_NAME_SIZE];
    unsigned int req_labs;
    unsigned int max_students;
    unsigned int unassigned_labs;
  }
course;

course* construct_course(
  const char* name,
  unsigned int req_labs,
  unsigned int max_students
)
{
  course* result = malloc(sizeof(course));

  strncpy(result->name, name, COURSE_NAME_SIZE - 1);
  result->name[COURSE_NAME_SIZE - 1] = '\0';
  result->req_labs = req_labs;
  result->max_students = max_students;
  result->unassigned_labs = req_labs;

  return result;
}

void free_course(course* course_to_free)
{
  free(course_to_free);
}

void free_course_wrapper(void* course_to_free)
{
  free_course(course_to_free);
}


const int course_string_comparator_wrapper(
  const void* in_course,
  const void* name
)
{
  return strcmp(
    ((const course*)in_course)->name,
    (const char*)name
  );
}

const int course_ptr_string_comparator(const void* in_course, const void* name)
{
  return strcmp(
    (*((const course**)in_course))->name,
    (const char*)name
  );
}

#endif //ASSIGNMENT_2_SCHEDULE_COURSE_H

#ifndef ASSIGNMENT_2_SCHEDULE_PROFESSOR_H
#define ASSIGNMENT_2_SCHEDULE_PROFESSOR_H


#define PROFESSOR_MAX_ASSIGNED_TRAINED_COURSES 2
#define PROFESSOR_MAX_ASSIGNED_UNTRAINED_COURSES 1

#define PROFESSOR_LEAST_NUM_TRAINED_COURSES 1

#define PROFESSOR_NOT_RUNNING_COURSE_PENALTY 5

#define PROFESSOR_DELIM_TOK P


typedef
  struct professor
  {
    being_name name;
    double_linked_list* trained_courses;
  }
professor;


professor* construct_professor(
  const char* first_name,
  const char* last_name,
  double_linked_list* trained_courses
)
{
  professor* result = malloc(sizeof(professor));

  strncpy(result->name.first_name, first_name, FIRST_NAME_SIZE - 1);
  result->name.first_name[FIRST_NAME_SIZE - 1] = '\0';
  strncpy(result->name.last_name, last_name, LAST_NAME_SIZE - 1);
  result->name.last_name[LAST_NAME_SIZE - 1] = '\0';
  result->trained_courses = copy_double_linked_list(trained_courses);

  return result;
}

professor* construct_professor_from_name_courses(
  const being_name* name,
  double_linked_list* trained_courses
)
{
  return construct_professor(
    name->first_name,
    name->last_name,
    trained_courses
  );
}

void free_professor(professor* professor_to_free)
{
  free_double_linked_list(professor_to_free->trained_courses);
  free(professor_to_free);
}

void free_professor_wrapper(void* professor_to_free)
{
  free_professor(professor_to_free);
}


int professor_name_comparator(
  const professor* in_prof,
  const being_name* name_of_being
)
{
  int result = strcmp(
    in_prof->name.first_name,
    name_of_being->first_name
  );

  if (!result)
  {
    result = strcmp(
      in_prof->name.last_name,
      name_of_being->last_name
    );
  }

  return result;
}

int professor_comparator(
  const professor* left,
  const professor* right
)
{
  if (left == right)
  {
    return 0;
  }
  else if (!left)
  {
    return -1;
  }
  else if (!right)
  {
    return 1;
  }
  else
  {
    return professor_name_comparator(left, &(right->name));
  }
}

const int professor_name_comparator_wrapper(
  const void* in_prof,
  const void* name_of_being
)
{
  return professor_name_comparator(
    (const professor*)in_prof,
    (const being_name*)name_of_being
  );
}

#endif //ASSIGNMENT_2_SCHEDULE_PROFESSOR_H

#ifndef ASSIGNMENT_2_SCHEDULE_STUDENT_H
#define ASSIGNMENT_2_SCHEDULE_STUDENT_H


// including '\0', so ID itself is exactly 5 chars
#define STUDENT_ID_STR_SIZE 6

#define STUDENT_LEAST_AMOUNT_REQ_CLASS 1

#define STUDENT_LACK_COURSE_PENALTY 1

#define STUDENT_DELIM_TOK S


typedef
  struct student
  {
    being_name name;
    char id[STUDENT_ID_STR_SIZE];
    double_linked_list* requested_courses;
  }
student;


student* construct_student(
  const char* first_name,
  const char* last_name,
  const char id[STUDENT_ID_STR_SIZE],
  double_linked_list* requested_courses
)
{
  student* result = malloc(sizeof(student));

  strncpy(result->name.first_name, first_name, FIRST_NAME_SIZE - 1);
  result->name.first_name[FIRST_NAME_SIZE - 1] = '\0';
  strncpy(result->name.last_name, last_name, LAST_NAME_SIZE - 1);
  result->name.last_name[LAST_NAME_SIZE - 1] = '\0';
  strncpy(result->id, id, STUDENT_ID_STR_SIZE - 1);
  result->id[STUDENT_ID_STR_SIZE - 1] = '\0';
  result->requested_courses = copy_double_linked_list(requested_courses);

  return result;
}

student* construct_student_from_name_courses(
  const being_name* name,
  const char id[STUDENT_ID_STR_SIZE],
  double_linked_list* requested_courses
)
{
  return construct_student(
    name->first_name,
    name->last_name,
    id,
    requested_courses
  );
}

student* copy_student_dont_copy_courses(
  student* student_to_copy
)
{
  student* result = malloc(sizeof(student));

  strncpy(
    result->name.first_name,
    student_to_copy->name.first_name,
    FIRST_NAME_SIZE - 1
  );
  result->name.first_name[FIRST_NAME_SIZE - 1] = '\0';
  strncpy(
    result->name.last_name,
    student_to_copy->name.last_name,
    LAST_NAME_SIZE - 1
  );
  result->name.last_name[LAST_NAME_SIZE - 1] = '\0';
  strcpy(result->id, student_to_copy->id);
  result->requested_courses = student_to_copy->requested_courses;

  return result;
}


void free_student(student* student_to_free)
{
  free_double_linked_list(student_to_free->requested_courses);
  free(student_to_free);
}

void free_student_leave_courses(student* student_to_free)
{
  free(student_to_free);
}

void free_student_wrapper(void* student_to_free)
{
  free_student(student_to_free);
}

void free_student_leave_courses_wrapper(void* student_to_free)
{
  free_student_leave_courses(student_to_free);
}


const int student_id_comparator_wrapper(
  const void* in_student,
  const void* id
)
{
  return strcmp(
    ((student*)in_student)->id,
    id
  );
}


unsigned char is_valid_student_id(const char* id)
{
  size_t cur_char_i = 0;

  for (
    ;
    cur_char_i < STUDENT_ID_STR_SIZE &&
    *id;
    ++id, ++cur_char_i
  )
  {
    if (!isalnum(*id))
    {
      return 0;
    }
  }

  return (*id || cur_char_i != (STUDENT_ID_STR_SIZE - 1)) ? 0 : 1;
}


void add_penalty_for_student_wrapper(
  void* in_student,
  va_list accumulator_arg
)
{
  unsigned int* accumulator = va_arg(accumulator_arg, unsigned int*);
  *accumulator +=
    STUDENT_LACK_COURSE_PENALTY *
    ((student*)in_student)->requested_courses->size
  ;
}


void print_student(student* stud, FILE* file_ptr)
{
  fprintf(
    file_ptr,
    "%s %s %s\n",
    stud->name.first_name,
    stud->name.last_name,
    stud->id
  );
}

void print_student_wrapper(void* stud, va_list file_p)
{
  FILE* file_ptr = va_arg(file_p, FILE*);
  print_student(stud, file_ptr);
}

#endif //ASSIGNMENT_2_SCHEDULE_STUDENT_H

#ifndef ASSIGNMENT_2_SCHEDULE_TEACHING_ASSISTANT_H
#define ASSIGNMENT_2_SCHEDULE_TEACHING_ASSISTANT_H


#define TA_MAX_ASSIGNED_TRAINED_COURSES 4

#define TA_LEAST_NUM_TRAINED_COURSES 1

#define TA_LACK_LAB_PENALTY 2

#define TA_DELIM_TOK T


typedef
  struct teaching_assistant
  {
    being_name name;
    double_linked_list* trained_courses;
    unsigned int labs_left;
  }
teaching_assistant;


teaching_assistant* construct_teaching_assistant(
  const char* first_name,
  const char* last_name,
  double_linked_list* trained_courses
)
{
  teaching_assistant* result = malloc(sizeof(teaching_assistant));

  strncpy(result->name.first_name, first_name, FIRST_NAME_SIZE - 1);
  result->name.first_name[FIRST_NAME_SIZE - 1] = '\0';
  strncpy(result->name.last_name, last_name, LAST_NAME_SIZE - 1);
  result->name.last_name[LAST_NAME_SIZE - 1] = '\0';
  result->trained_courses = copy_double_linked_list(trained_courses);
  result->labs_left = TA_MAX_ASSIGNED_TRAINED_COURSES;

  return result;
}

teaching_assistant* construct_ta_from_name_courses(
  const being_name* name,
  double_linked_list* trained_courses
)
{
  return construct_teaching_assistant(
    name->first_name,
    name->last_name,
    trained_courses
  );
}

teaching_assistant* copy_teaching_assistant(
  teaching_assistant* ta_to_copy
)
{
  teaching_assistant* result =
    construct_ta_from_name_courses(
    &ta_to_copy->name,
    ta_to_copy->trained_courses
  );

  result->labs_left = ta_to_copy->labs_left;

  return result;
}

teaching_assistant* copy_teaching_assistant_dont_copy_courses(
  teaching_assistant* ta_to_copy
)
{
  teaching_assistant* result = malloc(sizeof(teaching_assistant));

  strncpy(
    result->name.first_name,
    ta_to_copy->name.first_name,
    FIRST_NAME_SIZE - 1
  );
  result->name.first_name[FIRST_NAME_SIZE - 1] = '\0';
  strncpy(
    result->name.last_name,
    ta_to_copy->name.last_name,
    LAST_NAME_SIZE - 1
  );
  result->name.last_name[LAST_NAME_SIZE - 1] = '\0';
  result->trained_courses = ta_to_copy->trained_courses;
  result->labs_left = TA_MAX_ASSIGNED_TRAINED_COURSES;

  return result;
}

void free_teaching_assistant(teaching_assistant* ta_to_free)
{
  free_double_linked_list(ta_to_free->trained_courses);
  free(ta_to_free);
}

void free_teaching_assistant_leave_courses(
  teaching_assistant* ta_to_free
)
{
  free(ta_to_free);
}

void free_teaching_assistant_wrapper(void* ta_to_free)
{
  free_teaching_assistant(ta_to_free);
}

void free_teaching_assistant_leave_courses_wrapper(
  void* ta_to_free
)
{
  free_teaching_assistant_leave_courses(ta_to_free);
}


const int ta_name_comparator_wrapper(
  const void* in_ta,
  const void* name_of_being
)
{
  int result = strcmp(
    ((teaching_assistant*)in_ta)->name.first_name,
    ((being_name*)name_of_being)->first_name
  );

  if (!result)
  {
    result = strcmp(
      ((teaching_assistant*)in_ta)->name.last_name,
      ((being_name*)name_of_being)->last_name
    );
  }

  return result;
}


void calculate_penalty_cancellation(
  teaching_assistant* ta,
  unsigned int* penalty_cancellation
)
{
  *penalty_cancellation +=
    (TA_MAX_ASSIGNED_TRAINED_COURSES - (ta->labs_left)) *
    TA_LACK_LAB_PENALTY;
}

void calculate_penalty_cancellation_wrapper(
  void* ta,
  va_list penalty_cancellation
)
{
  unsigned int* cancellation = va_arg(penalty_cancellation, unsigned int*);

  calculate_penalty_cancellation(ta, cancellation);
}


void print_ta(teaching_assistant* ta, FILE* file_ptr)
{
  for (
    unsigned int labs_occupied =
      TA_MAX_ASSIGNED_TRAINED_COURSES - ta->labs_left;
    labs_occupied > 0;
    --labs_occupied
  )
  {
    fprintf(
      file_ptr,
      "%s %s\n",
      ta->name.first_name,
      ta->name.last_name
    );
  }
}

void print_ta_wrapper(void* ta, va_list file_p)
{
  FILE* file_ptr = va_arg(file_p, FILE*);
  print_ta(ta, file_ptr);
}

#endif //ASSIGNMENT_2_SCHEDULE_TEACHING_ASSISTANT_H

#ifndef ASSIGNMENT_2_SCHEDULE_STR_UTILS_H
#define ASSIGNMENT_2_SCHEDULE_STR_UTILS_H

char is_word_of_letters(const char* str)
{
  if (*str)
  {
    while (*str)
    {
      if (!isalpha(*str))
      {
        return 0;
      }

      ++str;
    }

    return 1;
  }
  else
  {
    return 0;
  }
}

char is_word_of_digits(const char* str)
{
  while (*str)
  {
    if (!isdigit(*str))
    {
      return 0;
    }

    ++str;
  }

  return 1;
}

char is_word_of_digits_without_lead_zero(const char* str)
{
  char result = 0;

  if (*str && (*str != '0'))
  {
    result = is_word_of_digits(str);
  }

  return result;
}

/**
 * @brief Deletes whitespace characters from the right of a given
 * null-terminated string by putting '\0' after the last non-whitespace
 * character.
 * @param str Null-terminated string to trim from the right. Not null.
 */
void trim_right(char* restrict str)
{
  char* restrict last_nonws = str;

  while (*str)
  {
    if (!isspace(*str))
    {
      last_nonws = str;
    }

    ++str;
  }

  *last_nonws = '\0';
}

/**
 * @brief Deletes whitespace characters from the right of a given
 * null-terminated string by putting '\0' after the last non-whitespace
 * character. If the size of the string is known it is preferable to use
 * this function over trim_right(), because it will perform faster.
 * @param str Null-terminated string to trim from the right. Not null.
 * @param len Length of str until and including the first null-terminator.
 * @see trim_right()
 */
void trim_char_arr_right(char* restrict str, size_t len)
{
  if (len > 0)
  {
    str += len - 1;
    char* restrict pos = str;

    while (len > 0 && (isspace(*str) || (*str == '\0')))
    {
      pos = str;

      --str;
      --len;
    }

    *pos = '\0';
  }
}

#endif //ASSIGNMENT_2_SCHEDULE_STR_UTILS_H

#ifndef ASSIGNMENT_2_SCHEDULE_SCHEDULE_INPUT_H
#define ASSIGNMENT_2_SCHEDULE_SCHEDULE_INPUT_H


typedef
  struct schedule_input
  {
    double_linked_list* courses;
    double_linked_list* professors;
    double_linked_list* tas;
    double_linked_list* students;
    unsigned int total_penalty;
  }
schedule_input;

schedule_input* construct_schedule_input(
  double_linked_list* courses,
  double_linked_list* professors,
  double_linked_list* tas,
  double_linked_list* students
)
{
  schedule_input* result = malloc(sizeof(schedule_input));

  result->courses = courses;
  result->professors = professors;
  result->tas = tas;
  result->students = students;

  unsigned int students_penalty = 0;
  dll_map_for_all(
    students,
    add_penalty_for_student_wrapper,
    &students_penalty
  );

  result->total_penalty =
    (COURSE_NOT_RUNNING_PENALTY * courses->size) +
    (PROFESSOR_NOT_RUNNING_COURSE_PENALTY *
    PROFESSOR_MAX_ASSIGNED_TRAINED_COURSES * professors->size) +
    (TA_LACK_LAB_PENALTY * TA_MAX_ASSIGNED_TRAINED_COURSES * tas->size) +
    students_penalty
  ;

  return result;
}

void free_schedule_input(schedule_input* schedule_input_to_free)
{
  free_double_linked_list(schedule_input_to_free->courses);
  free_double_linked_list(schedule_input_to_free->professors);
  free_double_linked_list(schedule_input_to_free->tas);
  free_double_linked_list(schedule_input_to_free->students);
  free(schedule_input_to_free);
}

#endif //ASSIGNMENT_2_SCHEDULE_SCHEDULE_INPUT_H

#ifndef ASSIGNMENT_2_SCHEDULE_INPUT_H
#define ASSIGNMENT_2_SCHEDULE_INPUT_H


#define MAX_NUM_FILES 50
#define MIN_NUM_FILES 1
// With '\0', so maximum number of digits is 2
#define FILE_NUM_STR_LEN 3
// With '\0', so max file name length is 11
#define MAX_FILENAME_STR_LEN 12
#define INPUT_FILE_PREFIX input
#define INPUT_FILE_EXT .txt
// With '\0'
#define MAX_LINE_SIZE 1024


typedef
  enum file_read_status
  {
    /*!
     * @brief File read successfully.
     */
    OK = 0,
    /*!
     * @brief File has an invalid structure.
     */
    INVALID_STRUCTURE,
    /*!
     * @brief Indicates that file not exists.
     */
    FILE_NOT_EXISTS
  }
file_read_status;

static inline char is_invalid_name(const char* str)
{
  return !is_word_of_letters(str)
      || (strcmp(str, STR_VAL(PROFESSOR_DELIM_TOK)) == 0)
      || (strcmp(str, STR_VAL(STUDENT_DELIM_TOK)) == 0)
      || (strcmp(str, STR_VAL(TA_DELIM_TOK)) == 0)
  ;
}

double_linked_list* read_courses_from_input_file(
  FILE* file_ptr,
  file_read_status* restrict read_status
)
{
  double_linked_list* courses = construct_double_linked_list_with_comp_destr(
    sizeof(course),
    course_string_comparator_wrapper,
    free_course_wrapper
  );
  char* cur_read_seq = NULL;
  size_t cur_read_seq_size = 0;

  while (1)
  {
    char* tok = NULL;
    char* course_name = NULL;
    long req_labs;
    long max_students;

    // read current line
    getline(&cur_read_seq, &cur_read_seq_size, file_ptr);

    if (
      feof(file_ptr) ||
      // try to read a course name
      (tok = strtok(cur_read_seq, " \n")) == NULL ||
      !is_word_of_letters(tok) ||
      strcmp(tok, STR_VAL(TA_DELIM_TOK)) == 0 ||
      strcmp(tok, STR_VAL(STUDENT_DELIM_TOK)) == 0 ||
      dll_find_first(courses, tok)
    )
    {
      free_double_linked_list(courses);
      courses = NULL;
      *read_status = INVALID_STRUCTURE;
      break;
    }
    else
    {
      if (strcmp(tok, STR_VAL(PROFESSOR_DELIM_TOK)) == 0)
      {
        if (strtok(NULL, " \n"))
        {
          free_double_linked_list(courses);
          courses = NULL;
          *read_status = INVALID_STRUCTURE;
        }
        else
        {
          *read_status = OK;
        }

        break;
      }
      else
      // the currently read sequence is a valid course name
      {
        // copy currently read sequence to course_name
        course_name = calloc(sizeof(char), strlen(tok) + 1);
        strcpy(course_name, tok);

        // try to read a required number of labs
        tok = strtok(NULL, " ");

        if (
          tok == NULL ||
          !is_word_of_digits_without_lead_zero(tok)
        )
        {
          free(course_name);
          free_double_linked_list(courses);
          courses = NULL;
          *read_status = INVALID_STRUCTURE;
          break;
        }
        else
        // read sequence is indeed some unsigned integer without leading zeros
        {
          req_labs = strtol(tok, NULL, 10);

          // try to read a number of students
          tok = strtok(NULL, "\n");

          if (
            tok == NULL ||
            !is_word_of_digits_without_lead_zero(tok)
          )
          {
            free(course_name);
            free_double_linked_list(courses);
            courses = NULL;
            *read_status = INVALID_STRUCTURE;
            break;
          }
          else
          // read sequence is indeed some unsigned integer without leading zeros
          {
            max_students = strtol(tok, NULL, 10);

            if (strtok(NULL, " \n"))
            {
              free(course_name);
              free_double_linked_list(courses);
              courses = NULL;
              *read_status = INVALID_STRUCTURE;
              break;
            }

            dll_append_value(
              courses,
              construct_course(
                course_name,
                req_labs,
                max_students
              )
            );

            free(course_name);
          }
        }
      }
    }
  }

  free(cur_read_seq);
  return courses;
}

double_linked_list* read_list_of_courses(
  char* courses_str,
  double_linked_list* available_courses,
  file_read_status* restrict read_status
)
{
  double_linked_list* courses = construct_double_linked_list_with_comp(
    sizeof(course),
    course_string_comparator_wrapper
  );

  char* course_name = strtok(courses_str, " \n");

  if (course_name)
  {
    while (course_name)
    {
      if (is_invalid_name(course_name))
      {
        free_double_linked_list(courses);
        courses = NULL;
        *read_status = INVALID_STRUCTURE;
        break;
      }
      else
      // course has a valid name and is available in list of all courses
      {
        course* cur_course = dll_find_first(available_courses, course_name);

        if (
          cur_course &&
          !dll_find_first(courses, cur_course)
        )
        {
          dll_append_value(courses, cur_course);
          *read_status = OK;
        }
        else
        {
          free_double_linked_list(courses);
          courses = NULL;
          *read_status = INVALID_STRUCTURE;
          break;
        }
      }

      course_name = strtok(NULL, " \n");
    }
  }
  else
  // no courses on the line; invalid structure in terms of task and
  // implementation
  {
    free_double_linked_list(courses);
    courses = NULL;
    *read_status = INVALID_STRUCTURE;
  }

  return courses;
}

double_linked_list* read_professors_from_input_file(
  FILE* file_ptr,
  double_linked_list* available_courses,
  file_read_status* restrict read_status
)
{
  double_linked_list* professors = construct_double_linked_list_with_comp_destr(
    sizeof(professor),
    professor_name_comparator_wrapper,
    free_professor_wrapper
  );
  char* cur_read_seq = NULL;
  size_t cur_read_seq_size = 0;

  while (1)
  {
    char* tok = NULL;
    being_name professor_name;

    // try to read a professor's first name
    getdelim(&cur_read_seq, &cur_read_seq_size, '\n', file_ptr);

    if (
      feof(file_ptr) ||
      (tok = strtok(cur_read_seq, " \n")) == NULL ||
      !is_word_of_letters(tok) ||
      strcmp(tok, STR_VAL(PROFESSOR_DELIM_TOK)) == 0 ||
      strcmp(tok, STR_VAL(STUDENT_DELIM_TOK)) == 0
    )
    {
      free_double_linked_list(professors);
      professors = NULL;
      *read_status = INVALID_STRUCTURE;
      break;
    }
    else
    {
      if (strcmp(tok, STR_VAL(TA_DELIM_TOK)) == 0)
      // Is it a TA token?
      {
        if (strtok(NULL, " \n"))
        {
          free_double_linked_list(professors);
          professors = NULL;
          *read_status = INVALID_STRUCTURE;
        }
        else
        {
          *read_status = OK;
        }

        break;
      }
      else
      // the currently read sequence is a valid professor's first name
      {
        // copy currently read sequence to professor_name->first_name
        strncpy(
          professor_name.first_name,
          tok,
          FIRST_NAME_SIZE - 1
        );
        professor_name.first_name[FIRST_NAME_SIZE - 1] = '\0';

        // try to read professor's last name
        tok = strtok(NULL, " ");

        if (tok == NULL)
        {
          free_double_linked_list(professors);
          professors = NULL;
          *read_status = INVALID_STRUCTURE;
          break;
        }
        else
        {
          // copy currently read sequence to professor_name->last_name
          strncpy(
            professor_name.last_name,
            tok,
            LAST_NAME_SIZE - 1
          );
          professor_name.last_name[LAST_NAME_SIZE - 1] = '\0';

          if (
            is_invalid_name(tok) ||
            dll_find_first(professors, &professor_name)
          )
          {
            free_double_linked_list(professors);
            professors = NULL;
            *read_status = INVALID_STRUCTURE;
            break;
          }
          else
          // A new professor's first and last names are read and both are valid;
          // Try to read courses
          {
            tok = strtok(NULL, "\n");

            double_linked_list* trained_courses = read_list_of_courses(
              tok,
              available_courses,
              read_status
            );

            if (*read_status == INVALID_STRUCTURE)
            {
              // trained courses list does no need to be freed,
              // read_list_of_courses manages that; moreover it is null
              free_double_linked_list(professors);
              professors = NULL;
              break;
            }
            else
            {
              dll_append_value(
                professors,
                construct_professor_from_name_courses(
                  &professor_name,
                  trained_courses
                )
              );

              free_double_linked_list(trained_courses);
            }
          }
        }
      }
    }
  }

  free(cur_read_seq);
  return professors;
}

double_linked_list* read_tas_from_input_file(
  FILE* file_ptr,
  double_linked_list* available_courses,
  file_read_status* restrict read_status
)
{
  double_linked_list* tas = construct_double_linked_list_with_comp_destr(
    sizeof(teaching_assistant),
    ta_name_comparator_wrapper,
    free_teaching_assistant_wrapper
  );
  char* cur_read_seq = NULL;
  size_t cur_read_seq_size = 0;

  while (1)
  {
    char* tok = NULL;
    being_name ta_name;
    ssize_t num_read_chars = 0;

    // try to read a ta's first name
    num_read_chars = getdelim(
      &cur_read_seq,
      &cur_read_seq_size,
      '\n',
      file_ptr
    );

    if (
      (feof(file_ptr) && num_read_chars <= 0) ||
      (tok = strtok(cur_read_seq, " \n")) == NULL ||
      !is_word_of_letters(tok) ||
      strcmp(tok, STR_VAL(PROFESSOR_DELIM_TOK)) == 0 ||
      strcmp(tok, STR_VAL(TA_DELIM_TOK)) == 0
    )
    {
      free_double_linked_list(tas);
      tas = NULL;
      *read_status = INVALID_STRUCTURE;
      break;
    }
    else
    {
      if (strcmp(tok, STR_VAL(STUDENT_DELIM_TOK)) == 0)
      // Is it a student token?
      {
        if (strtok(NULL, " \n"))
        {
          free_double_linked_list(tas);
          tas = NULL;
          *read_status = INVALID_STRUCTURE;
        }
        else
        {
          *read_status = OK;
        }

        break;
      }
      else
      // the currently read sequence is a valid ta's first name
      {
        // copy currently read sequence to ta_name->first_name
        strncpy(
          ta_name.first_name,
          tok,
          FIRST_NAME_SIZE - 1
        );
        ta_name.first_name[FIRST_NAME_SIZE - 1] = '\0';

        // try to read ta's last name
        tok = strtok(NULL, " ");

        if (tok == NULL)
        {
          free_double_linked_list(tas);
          tas = NULL;
          *read_status = INVALID_STRUCTURE;
          break;
        }
        else
        {
          // copy currently read sequence to ta_name->last_name
          strncpy(
            ta_name.last_name,
            tok,
            LAST_NAME_SIZE - 1
          );
          ta_name.last_name[LAST_NAME_SIZE - 1] = '\0';

          if (
            is_invalid_name(tok) ||
            dll_find_first(tas, &ta_name)
          )
          {
            free_double_linked_list(tas);
            tas = NULL;
            *read_status = INVALID_STRUCTURE;
            break;
          }
          else
          // A new ta's first and last names are read and are both valid;
          // Try to read courses
          {
            tok = strtok(NULL, "\n");

            double_linked_list* trained_courses = read_list_of_courses(
              tok,
              available_courses,
              read_status
            );

            if (*read_status == INVALID_STRUCTURE)
            {
              // trained courses list does no need to be freed,
              // read_list_of_courses manages that; moreover it is null
              free_double_linked_list(tas);
              tas = NULL;
              break;
            }
            else
            {
              dll_append_value(
                tas,
                construct_ta_from_name_courses(
                  &ta_name,
                  trained_courses
                )
              );

              free_double_linked_list(trained_courses);
            }
          }
        }
      }
    }
  }

  free(cur_read_seq);
  return tas;
}

double_linked_list* read_students_from_input_file(
  FILE* file_ptr,
  double_linked_list* available_courses,
  file_read_status* restrict read_status
)
{
  double_linked_list* students = construct_double_linked_list_with_comp_destr(
    sizeof(student),
    student_id_comparator_wrapper,
    free_student_wrapper
  );
  char* cur_read_seq = NULL;
  size_t cur_read_seq_size = 0;

  while (1)
  {
    char* tok = NULL;
    being_name student_name;
    char id[STUDENT_ID_STR_SIZE];
    ssize_t read_chars = -1;

    // try to read a student's first name
    read_chars = getdelim(&cur_read_seq, &cur_read_seq_size, '\n', file_ptr);

    if (
      feof(file_ptr) &&
      read_chars == -1
    )
    {
      *read_status = OK;

      break;
    }
    else
    {
      tok = strtok(cur_read_seq, " \n");

      if (
        tok == NULL ||
        is_invalid_name(tok)
      )
      {
        free_double_linked_list(students);
        students = NULL;
        *read_status = INVALID_STRUCTURE;
        break;
      }
      else
      // the currently read sequence is a valid student's first name
      {
        // copy currently read sequence to student_name->first_name
        strncpy(
          student_name.first_name,
          tok,
          FIRST_NAME_SIZE - 1
        );
        student_name.first_name[FIRST_NAME_SIZE - 1] = '\0';

        // try to read ta's last name
        tok = strtok(NULL, " ");

        if (
          tok == NULL ||
          is_invalid_name(tok)
        )
        {
          free_double_linked_list(students);
          students = NULL;
          *read_status = INVALID_STRUCTURE;
          break;
        }
        else
        {
          // copy currently read sequence to student_name->last_name
          strncpy(
            student_name.last_name,
            tok,
            LAST_NAME_SIZE - 1
          );
          student_name.last_name[LAST_NAME_SIZE - 1] = '\0';

          // A new student's first and last names are read and are both valid;
          // Try to read ID
          tok = strtok(NULL, " ");

          if (tok == NULL || !is_valid_student_id(tok))
          {
            free_double_linked_list(students);
            students = NULL;
            *read_status = INVALID_STRUCTURE;
            break;
          }
          else
          {
            strcpy(id, tok);

            if (dll_find_first(students, id))
            {
              free_double_linked_list(students);
              students = NULL;
              *read_status = INVALID_STRUCTURE;
              break;
            }
            else
            {
              tok = strtok(NULL, "\n");

              double_linked_list* trained_courses = read_list_of_courses(
                tok,
                available_courses,
                read_status
              );

              if (*read_status == INVALID_STRUCTURE)
              {
                // trained courses list does no need to be freed,
                // read_list_of_courses manages that; moreover it is null
                free_double_linked_list(students);
                students = NULL;
                break;
              }
              else
              {
                dll_append_value(
                  students,
                  construct_student_from_name_courses(
                    &student_name,
                    id,
                    trained_courses
                  )
                );

                free_double_linked_list(trained_courses);
              }
            }
          }
        }
      }
    }
  }

  free(cur_read_seq);
  return students;
}

schedule_input* read_input_file(
  FILE* file_ptr,
  file_read_status* restrict read_status
)
{
  schedule_input* result = NULL;

  if (file_ptr)
  {
    double_linked_list* courses = read_courses_from_input_file(
      file_ptr,
      read_status
    );

    if (*read_status == OK)
    {
      double_linked_list* professors = read_professors_from_input_file(
        file_ptr,
        courses,
        read_status
      );

      if (*read_status == OK)
      {
        double_linked_list* tas = read_tas_from_input_file(
          file_ptr,
          courses,
          read_status
        );

        if (*read_status == OK)
        {
          double_linked_list* students = read_students_from_input_file(
            file_ptr,
            courses,
            read_status
          );

          if (*read_status == OK)
          {
            result = construct_schedule_input(
              courses,
              professors,
              tas,
              students
            );
          }
          else
          {
            free_double_linked_list(courses);
            free_double_linked_list(professors);
            free_double_linked_list(tas);
          }
        }
        else
        {
          free_double_linked_list(courses);
          free_double_linked_list(professors);
        }
      }
      else
      {
        free_double_linked_list(courses);
      }
    }
  }
  else
  {
    *read_status = FILE_NOT_EXISTS;
  }

  return result;
}

#endif //ASSIGNMENT_2_SCHEDULE_INPUT_H

#ifndef ASSIGNMENT_2_SCHEDULE_FINAL_ASSIGNMENT_STATE_H
#define ASSIGNMENT_2_SCHEDULE_FINAL_ASSIGNMENT_STATE_H


typedef
  struct course_assignment
  {
    course* course_obj;
    professor* assigned_professor;
    double_linked_list* assigned_tas;
    double_linked_list* assigned_students;

    unsigned int students_left;
    unsigned int penalty_cancellation;
  }
course_assignment;

course_assignment* construct_course_assignment_with_course_prof(
  course* course_obj,
  professor* assigned_professor
)
{
  course_assignment* result = malloc(sizeof(course_assignment));

  result->course_obj = course_obj;
  result->assigned_professor = assigned_professor;
  result->assigned_tas = NULL;
  result->assigned_students = NULL;
  result->students_left = course_obj->max_students;
  result->penalty_cancellation = PROFESSOR_NOT_RUNNING_COURSE_PENALTY;

  return result;
}

course_assignment* construct_course_assignment_with_course_tas(
  course* course_obj,
  double_linked_list* assigned_tas
)
{
  course_assignment* result = malloc(sizeof(course_assignment));

  result->course_obj = course_obj;
  result->assigned_professor = NULL;
  result->assigned_tas = assigned_tas;
  result->assigned_students = NULL;
//    construct_empty_double_linked_list(
//    sizeof(student)
//  );
  result->students_left = course_obj->max_students;
  result->penalty_cancellation = 0;
  dll_map_for_all(
    assigned_tas,
    calculate_penalty_cancellation_wrapper,
    &(result->penalty_cancellation)
  );

  return result;
}

course_assignment* subtle_copy_course_assignment(
  course_assignment* other
)
{
  course_assignment* result = malloc(sizeof(course_assignment));

  result->course_obj = other->course_obj;
  result->assigned_professor = other->assigned_professor;
  result->assigned_tas = other->assigned_tas;
  result->assigned_students = other->assigned_students;
  result->students_left = other->students_left;
  result->penalty_cancellation = other->penalty_cancellation;

  return result;
}

course_assignment* deep_copy_course_assignment_lists(
  course_assignment* other
)
{
  course_assignment* result = malloc(sizeof(course_assignment));

  result->course_obj = other->course_obj;
  result->assigned_professor = other->assigned_professor;
  result->assigned_tas = (other->assigned_tas) ?
                         copy_double_linked_list(other->assigned_tas):
                         NULL;
  result->assigned_students = (other->assigned_students) ?
                              copy_double_linked_list(other->assigned_students):
                              NULL;
  result->students_left = other->students_left;
  result->penalty_cancellation = other->penalty_cancellation;

  return result;
}


void free_course_assignment_leave_course_prof(course_assignment* obj_to_free)
{
  if (obj_to_free->assigned_tas)
  {
    free_double_linked_list(obj_to_free->assigned_tas);
  }

  if (obj_to_free->assigned_students)
  {
    free_double_linked_list(obj_to_free->assigned_students);
  }

  free(obj_to_free);
}

void free_course_assignment_leave_course_prof_wrapper(
  void* course_assignment_to_free
)
{
  free_course_assignment_leave_course_prof(course_assignment_to_free);
}


void print_course_assignment(course_assignment* assignment, FILE* file_ptr)
{
  if (assignment->assigned_professor)
  {
    fprintf(file_ptr, "%s\n", assignment->course_obj->name);

    fprintf(
      file_ptr,
      "%s %s\n",
      assignment->assigned_professor->name.first_name,
      assignment->assigned_professor->name.last_name
    );

    dll_map_for_all(
      assignment->assigned_tas,
      print_ta_wrapper,
      file_ptr
    );

    dll_map_for_all(
      assignment->assigned_students,
      print_student_wrapper,
      file_ptr
    );

    fprintf(file_ptr, "\n");
  }
}

void print_course_assignment_wrapper(void* assignment, va_list file_p)
{
  FILE* file_ptr = va_arg(file_p, FILE*);
  print_course_assignment(assignment, file_ptr);
}


typedef
  struct assignment_distribution
  {
    double_linked_list* assigned_courses;
    unsigned int penalty;
  }
assignment_distribution;

assignment_distribution* construct_empty_assignment_distribution_with_penalty(
  unsigned int initial_penalty
)
{
  assignment_distribution* result = malloc(sizeof(assignment_distribution));

  result->assigned_courses = construct_double_linked_list_with_destr(
    sizeof(course_assignment),
    free_course_assignment_leave_course_prof_wrapper
  );
  result->penalty = initial_penalty;

  return result;
}

void deep_copy_assigned_course_into_list(
  course_assignment* assignment,
  double_linked_list* course_assignments
)
{
  dll_append_value(
    course_assignments,
    deep_copy_course_assignment_lists(assignment)
  );
}

void deep_copy_assigned_course_into_list_wrapper(
  void* assignment,
  va_list course_assigs
)
{
  double_linked_list* course_assignments = va_arg(
    course_assigs,
    double_linked_list*
  );

  deep_copy_assigned_course_into_list(assignment, course_assignments);
}

assignment_distribution* deep_copy_assignment_distribution(
  assignment_distribution* other
)
{
  assignment_distribution* result = malloc(sizeof(assignment_distribution));

  result->assigned_courses = construct_double_linked_list_with_destr(
    sizeof(course_assignment),
    free_course_assignment_leave_course_prof_wrapper
  );
  dll_map_for_all(
    other->assigned_courses,
    deep_copy_assigned_course_into_list_wrapper,
    result->assigned_courses
  );
  result->penalty = other->penalty;

  return result;
}


void free_assignment_distribution(assignment_distribution* obj_to_free)
{
  free_double_linked_list(obj_to_free->assigned_courses);
  free(obj_to_free);
}

void free_assignment_distribution_wrapper(void* assignment_distribution_to_free)
{
  free_assignment_distribution(assignment_distribution_to_free);
}


void print_assignment_distribution(
  assignment_distribution* distribution,
  FILE* file_ptr
)
{
  dll_map_for_all(
    distribution->assigned_courses,
    print_course_assignment_wrapper,
    file_ptr
  );
}

void print_course_assignment_course_error(
  course_assignment* ca,
  FILE* file_ptr
)
{
  if (!(ca->assigned_professor))
  {
    fprintf(
      file_ptr,
      "%s cannot be run.\n",
      ca->course_obj->name
    );
  }
}

void print_course_assignment_course_error_wrapper(
  void* ca,
  va_list file_ptr_arg
)
{
  FILE* file_ptr = va_arg(file_ptr_arg, FILE*);

  print_course_assignment_course_error(ca, file_ptr);
}

void print_distribution_courses_errors(
  assignment_distribution* distribution,
  FILE* file_ptr
)
{
  dll_map_for_all(
    distribution->assigned_courses,
    print_course_assignment_course_error_wrapper,
    file_ptr
  );
}

void print_distribution_professor_error(
  professor* prof,
  assignment_distribution* distribution,
  FILE* file_ptr
)
{
  unsigned char classes_running = 0;
  char* result_error = "is unassigned";

  dll_node* iter_node = distribution->assigned_courses->head;
  while (iter_node)
  {
    course_assignment* cur_assignment = (course_assignment*)iter_node->value;

    if (professor_comparator(prof, cur_assignment->assigned_professor) == 0)
    {
      if (
        dll_find_first(prof->trained_courses, cur_assignment->course_obj->name)
      )
      {
        ++classes_running;

        if (classes_running < PROFESSOR_MAX_ASSIGNED_TRAINED_COURSES)
        {
          result_error = "is lacking class";
        }
        else
        {
          result_error = NULL;
          break;
        }
      }
      else
      {
        fprintf(
          file_ptr,
          "%s %s is not trained for %s.\n",
          prof->name.first_name,
          prof->name.last_name,
          cur_assignment->course_obj->name
        );
        return;
      }
    }

    iter_node = iter_node->right;
  }

  if (result_error)
  {
    fprintf(
      file_ptr,
      "%s %s %s.\n",
      prof->name.first_name,
      prof->name.last_name,
      result_error
    );
  }
}

void print_distribution_professor_error_wrapper(
  void* prof,
  va_list distribution_filePtr
)
{
  assignment_distribution* distribution =
    va_arg(distribution_filePtr, assignment_distribution*);
  FILE* file_ptr =
    va_arg(distribution_filePtr, FILE*);

  print_distribution_professor_error(prof, distribution, file_ptr);
}

void print_distribution_professors_errors(
  double_linked_list* all_professors,
  assignment_distribution* distribution,
  FILE* file_ptr
)
{
  dll_map_for_all(
    all_professors,
    print_distribution_professor_error_wrapper,
    distribution,
    file_ptr
  );
}

void calculate_number_occupied_labs_for_ta_in_distribution(
  course_assignment* course_assig,
  teaching_assistant* ta,
  unsigned int* labs_occupied
)
{
  if (course_assig->assigned_tas)
  {
    teaching_assistant* found_ta = dll_find_first(
      course_assig->assigned_tas,
      &(ta->name)
    );

    if (found_ta)
    {
      *labs_occupied += TA_MAX_ASSIGNED_TRAINED_COURSES - found_ta->labs_left;
    }
  }
}

void calculate_number_occupied_labs_for_ta_in_distribution_wrapper(
  void* course_assig,
  va_list ta_labsOccup
)
{
  teaching_assistant* ta = va_arg(ta_labsOccup, teaching_assistant*);
  unsigned int* labs_occupied = va_arg(ta_labsOccup, unsigned int*);

  calculate_number_occupied_labs_for_ta_in_distribution(
    course_assig,
    ta,
    labs_occupied
  );
}

void print_distribution_ta_error(
  teaching_assistant* ta,
  assignment_distribution* distribution,
  FILE* file_ptr
)
{
  unsigned int labs_occupied = 0;

  dll_map_for_all(
    distribution->assigned_courses,
    calculate_number_occupied_labs_for_ta_in_distribution_wrapper,
    ta,
    &labs_occupied
  );

  unsigned int labs_left = TA_MAX_ASSIGNED_TRAINED_COURSES - labs_occupied;

  if (labs_left)
  {
    fprintf(
      file_ptr,
      "%s %s is lacking %u lab(s).\n",
      ta->name.first_name,
      ta->name.last_name,
      labs_left
    );
  }
}

void print_distribution_ta_error_wrapper(
  void* ta,
  va_list distrib_filePtr
)
{
  assignment_distribution* distribution =
    va_arg(distrib_filePtr, assignment_distribution*);
  FILE* file_ptr =
    va_arg(distrib_filePtr, FILE*);

  print_distribution_ta_error(ta, distribution, file_ptr);
}

void print_distribution_tas_errors(
  double_linked_list* tas,
  assignment_distribution* distribution,
  FILE* file_ptr
)
{
  dll_map_for_all(
    tas,
    print_distribution_ta_error_wrapper,
    distribution,
    file_ptr
  );
}

void remove_assigned_course_from_list_if_student_assigned(
  course_assignment* ca,
  student* stud,
  double_linked_list* result
)
{
  if (dll_find_first(ca->assigned_students, stud->id))
  {
    dll_remove_first_found(result, ca->course_obj->name);
  }
}

void remove_assigned_course_from_list_if_student_assigned_wrapper(
  void* ca,
  va_list stud_result
)
{
  student* stud = va_arg(stud_result, student*);
  double_linked_list* result = va_arg(stud_result, double_linked_list*);

  remove_assigned_course_from_list_if_student_assigned(
    ca,
    stud,
    result
  );
}

void print_student_lack_course_error(
  course* course_to_print,
  student* stud,
  FILE* file_ptr
)
{
  fprintf(
    file_ptr,
    "%s %s is lacking %s.\n",
    stud->name.first_name,
    stud->name.last_name,
    course_to_print->name
  );
}

void print_student_lack_course_error_wrapper(
  void* course_to_print,
  va_list stud_filePtr
)
{
  student* stud = va_arg(stud_filePtr, student*);
  FILE* file_ptr = va_arg(stud_filePtr, FILE*);

  print_student_lack_course_error(course_to_print, stud, file_ptr);
}

void print_distribution_student_error(
  student* stud,
  assignment_distribution* distribution,
  FILE* file_ptr
)
{
  double_linked_list* student_unassigned_courses =
    copy_double_linked_list(
      stud->requested_courses
    );

  dll_map_for_all(
    distribution->assigned_courses,
    remove_assigned_course_from_list_if_student_assigned_wrapper,
    stud,
    student_unassigned_courses
  );

  dll_map_for_all(
    student_unassigned_courses,
    print_student_lack_course_error_wrapper,
    stud,
    file_ptr
  );

  free_double_linked_list(student_unassigned_courses);
}

void print_distribution_student_error_wrapper(
  void* stud,
  va_list distribution_filePtr
)
{
  assignment_distribution* distribution =
    va_arg(distribution_filePtr, assignment_distribution*);
  FILE* file_ptr =
    va_arg(distribution_filePtr, FILE*);

  print_distribution_student_error(stud, distribution, file_ptr);
}

void print_distribution_students_errors(
  double_linked_list* students,
  assignment_distribution* distribution,
  FILE* file_ptr
)
{
  dll_map_for_all(
    students,
    print_distribution_student_error_wrapper,
    distribution,
    file_ptr
  );
}

void print_distribution_errors(
  assignment_distribution* distribution,
  schedule_input* input,
  FILE* file_ptr
)
{
  print_distribution_courses_errors(distribution, file_ptr);

  print_distribution_professors_errors(
    input->professors,
    distribution,
    file_ptr
  );

  print_distribution_tas_errors(
    input->tas,
    distribution,
    file_ptr
  );

  print_distribution_students_errors(
    input->students,
    distribution,
    file_ptr
  );
}

#endif //ASSIGNMENT_2_SCHEDULE_FINAL_ASSIGNMENT_STATE_H

#ifndef ASSIGNMENT_2_SCHEDULE_SCHEDULE_OPTIMIZATION_H
#define ASSIGNMENT_2_SCHEDULE_SCHEDULE_OPTIMIZATION_H


// -----------------------------------------------------------------------------
// Professors to a course
// -----------------------------------------------------------------------------

void assign_professor_to_course(
  professor* professor_to_assign,
  course* course_to_assign,
  double_linked_list* course_assignments_list_to_append
)
{
  dll_append_value(
    course_assignments_list_to_append,
    construct_course_assignment_with_course_prof(
      course_to_assign,
      professor_to_assign
    )
  );
}

void assign_professor_to_course_wrapper(
  void* passed_professor,
  va_list course_assiglist
)
{
  course* course_to_assign =
    va_arg(course_assiglist, course*);
  double_linked_list* course_assignments_list_to_append =
    va_arg(course_assiglist, double_linked_list*);

  assign_professor_to_course(
    (professor*)passed_professor,
    course_to_assign,
    course_assignments_list_to_append
  );
}

/**
 * @return Double linked list of course_assignments. Each entry contains
 * a unique combination of each professor from the universe of professors with
 * a given course, plus case, when a course lacks a professor with calculated
 * corresponding penalty cancellation for the assignment. Size of the resulting
 * lists is |professors| + 1.
 */
double_linked_list* assign_professors_to_course(
  course* course_to_assign,
  double_linked_list* professors
)
{
  double_linked_list* result = construct_double_linked_list_with_destr(
    sizeof(course_assignment),
    free_course_assignment_leave_course_prof_wrapper
  );

  dll_map_for_all(
    professors,
    assign_professor_to_course_wrapper,
    course_to_assign,
    result
  );

  return result;
}

// -----------------------------------------------------------------------------
// TAs to a course
// -----------------------------------------------------------------------------

void add_assignable_to_course_tas_to_list(
  teaching_assistant* ta,
  course* course_to_test,
  double_linked_list* list_to_append
)
{
  if (dll_find_first(ta->trained_courses, course_to_test->name))
  // if ta is trained for the course
  {
    dll_append_value(list_to_append, ta);
  }
}

void add_assignable_to_course_tas_to_list_wrapper(
  void* ta,
  va_list course_list_to_append
)
{
  course* course_to_test =
    va_arg(course_list_to_append, course*);
  double_linked_list* list_to_append =
    va_arg(course_list_to_append, double_linked_list*);

  add_assignable_to_course_tas_to_list(ta, course_to_test, list_to_append);
}

void insert_ta_copy_into_list(teaching_assistant* ta, double_linked_list* list)
{
  dll_append_copy(list, ta);
}

void insert_ta_copy_into_list_wrapper(void* ta, va_list dll)
{
  double_linked_list* list = va_arg(dll, double_linked_list*);

  insert_ta_copy_into_list(ta, list);
}

void get_combinations_of_tas_for_labs_backtrack(
  double_linked_list* tas_available,
  double_linked_list* tas_to_assign,
  unsigned int course_labs_left,
  unsigned int first_ta_labs_left,
  double_linked_list* result_list
)
{
  double_linked_list* copy_tas_to_assign =
    construct_double_linked_list_with_comp_destr(
      sizeof(teaching_assistant),
      ta_name_comparator_wrapper,
      free_teaching_assistant_leave_courses_wrapper
    );
  dll_map_for_all(
    tas_to_assign,
    insert_ta_copy_into_list_wrapper,
    copy_tas_to_assign
  );

  double_linked_list* copy_tas_available =
    construct_double_linked_list_with_comp_destr(
      sizeof(teaching_assistant),
      ta_name_comparator_wrapper,
      free_teaching_assistant_leave_courses_wrapper
    );
  dll_map_for_all(
    tas_available,
    insert_ta_copy_into_list_wrapper,
    copy_tas_available
  );

  while (!dll_is_empty(copy_tas_available))
  {
    if (course_labs_left == 0)
    // If no more labs left, then we are at the bottom of a backtracking tree
    // and a copy of tas_to_assign needs to be added to the result_list
    {
      dll_append_value(
        result_list,
        copy_double_linked_list(copy_tas_to_assign)
      );
      break;
    }
    else
    {
      // Copy tas to assign again
      dll_clear(copy_tas_to_assign);
      dll_map_for_all(
        tas_to_assign,
        insert_ta_copy_into_list_wrapper,
        copy_tas_to_assign
      );

      if (first_ta_labs_left == 0)
      // if the first TA ran out of his labs, delete him from the list and continue
      // with other TAs
      {
        dll_remove_first(copy_tas_available);
      }

      if (!dll_is_empty(copy_tas_available))
      {
        // search for TA by name in the list of TAs to assign
        teaching_assistant* cur_ta =
          (teaching_assistant*)copy_tas_available->head->value;

        teaching_assistant* found_ta = dll_find_first(
          copy_tas_to_assign,
          &(cur_ta->name)
        );

        if (found_ta)
        {
          --(found_ta->labs_left);
        }
        else
        {
          found_ta = copy_teaching_assistant_dont_copy_courses(cur_ta);
          first_ta_labs_left = found_ta->labs_left;
          --(found_ta->labs_left);

          dll_append_value(
            copy_tas_to_assign,
            found_ta
          );
        }

        get_combinations_of_tas_for_labs_backtrack(
          copy_tas_available,
          copy_tas_to_assign,
          course_labs_left - 1,
          first_ta_labs_left - 1,
          result_list
        );

        dll_remove_first(copy_tas_available);
      }
    }
  }

  free_double_linked_list(copy_tas_available);
  free_double_linked_list(copy_tas_to_assign);
}

void assign_ta_combination_to_course(
  double_linked_list* ta_combination,
  course* course_to_assign,
  double_linked_list* list_to_add
)
{
//  course_assignment* value_to_add =
//    construct_course_assignment_with_course_tas(
//      course_to_assign,
//      ta_combination
//    );
  dll_append_value(
    list_to_add,
    construct_course_assignment_with_course_tas(
      course_to_assign,
      ta_combination
    )
  );
}

void assign_ta_combination_to_course_wrapper(
  void* ta_combination,
  va_list course_listtoadd
)
{
  course* course_to_assign = va_arg(
    course_listtoadd,
    course*
  );
  double_linked_list* list_to_add = va_arg(
    course_listtoadd,
    double_linked_list*
  );

  assign_ta_combination_to_course(
    ta_combination,
    course_to_assign,
    list_to_add
  );
}

double_linked_list* assign_tas_to_course(
  course* course_to_assign,
  double_linked_list* all_tas
)
{
  double_linked_list* assignable_tas = construct_double_linked_list_with_comp(
    sizeof(teaching_assistant),
    ta_name_comparator_wrapper
  );

  // Find all assignable to a given course tas
  dll_map_for_all(
    all_tas,
    add_assignable_to_course_tas_to_list_wrapper,
    course_to_assign,
    assignable_tas
  );

  double_linked_list* ta_combinations =
    construct_empty_double_linked_list(
      sizeof(double_linked_list*)
    );

  double_linked_list* tas_to_assign =
    construct_double_linked_list_with_comp_destr(
      sizeof(teaching_assistant),
      ta_name_comparator_wrapper,
      free_teaching_assistant_wrapper
    );

  get_combinations_of_tas_for_labs_backtrack(
    assignable_tas,
    tas_to_assign,
    course_to_assign->req_labs,
    TA_MAX_ASSIGNED_TRAINED_COURSES,
    ta_combinations
  );
  free_double_linked_list(tas_to_assign);

  free_double_linked_list(assignable_tas);

  double_linked_list* result = construct_double_linked_list_with_destr(
    sizeof(course_assignment),
    free_course_assignment_leave_course_prof_wrapper
  );

  dll_map_for_all(
    ta_combinations,
    assign_ta_combination_to_course_wrapper,
    course_to_assign,
    result
  );

  free_double_linked_list(ta_combinations);

  return result;
}

// -----------------------------------------------------------------------------
// Mix TAs & Profs for a single course
// -----------------------------------------------------------------------------

void combine_tas_with_prof(
  course_assignment* course_assignment_tas,
  course_assignment* professor_course_assignment,
  double_linked_list* result_course_assignment
)
{
  course_assignment* result_assignment = subtle_copy_course_assignment(
    professor_course_assignment
  );
  result_assignment->assigned_tas = copy_double_linked_list(
    course_assignment_tas->assigned_tas
  );
  result_assignment->penalty_cancellation +=
    course_assignment_tas->penalty_cancellation +
    COURSE_NOT_RUNNING_PENALTY
    // since course is now running
  ;

  dll_append_value(
    result_course_assignment,
    result_assignment
  );
}

void combine_tas_with_prof_wrapper(
  void* course_assignment_tas,
  va_list profCa_result
)
{
  course_assignment* professor_course_assignment =
    va_arg(profCa_result, course_assignment*);
  double_linked_list* result_course_assignment =
    va_arg(profCa_result, double_linked_list*);

  combine_tas_with_prof(
    course_assignment_tas,
    professor_course_assignment,
    result_course_assignment
  );
}

void combine_tas_prof_course_assignment(
  course_assignment* prof_course_assignment,
  double_linked_list* tas_course_assignments,
  double_linked_list* result_course_assignment
)
{
  dll_map_for_all(
    tas_course_assignments,
    combine_tas_with_prof_wrapper,
    prof_course_assignment,
    result_course_assignment
  );
}

void combine_tas_prof_course_assignment_wrapper(
  void* prof_course_assignment,
  va_list taCa_result
)
{
  double_linked_list* tas_course_assignments =
    va_arg(taCa_result, double_linked_list*);
  double_linked_list* result_course_assignment =
    va_arg(taCa_result, double_linked_list*);

  combine_tas_prof_course_assignment(
    prof_course_assignment,
    tas_course_assignments,
    result_course_assignment
  );
}

double_linked_list* get_combinations_of_professors_tas_for_course(
  course* course_obj,
  double_linked_list* all_profs,
  double_linked_list* all_tas
)
{
  double_linked_list* result = construct_double_linked_list_with_destr(
    sizeof(course_assignment),
    free_course_assignment_leave_course_prof_wrapper
  );

  double_linked_list* profs_assigned = assign_professors_to_course(
    course_obj,
    all_profs
  );

  double_linked_list* tas_assigned = assign_tas_to_course(
    course_obj,
    all_tas
  );
//  tas_assigned->destr = NULL;

  dll_map_for_all(
    profs_assigned,
    combine_tas_prof_course_assignment_wrapper,
    tas_assigned,
    result
  );

  course_assignment* no_profs_tas =
    construct_course_assignment_with_course_prof(
      course_obj,
      NULL
    );
  no_profs_tas->penalty_cancellation = 0;

  dll_append_value(
    result,
    no_profs_tas
  );

  free_double_linked_list(profs_assigned);
  free_double_linked_list(tas_assigned);

  return result;
}

// -----------------------------------------------------------------------------
// Mix TAs & Profs for all courses
// -----------------------------------------------------------------------------

void insert_combinations_of_profs_tas_for_course_into_list(
  course* cur_course,
  double_linked_list* all_profs,
  double_linked_list* all_tas,
  double_linked_list* list_to_insert
)
{
  dll_append_value(list_to_insert,
    get_combinations_of_professors_tas_for_course(
      cur_course,
      all_profs,
      all_tas
    )
  );
}

void insert_combinations_of_profs_tas_for_course_into_list_wrapper(
  void* cur_course,
  va_list profs_tas_resultList
)
{
  double_linked_list* all_profs = va_arg(
    profs_tas_resultList,
    double_linked_list*
  );
  double_linked_list* all_tas = va_arg(
    profs_tas_resultList,
    double_linked_list*
  );
  double_linked_list* list_to_insert = va_arg(
    profs_tas_resultList,
    double_linked_list*
  );

  insert_combinations_of_profs_tas_for_course_into_list(
    cur_course,
    all_profs,
    all_tas,
    list_to_insert
  );
}

double_linked_list* get_combinations_of_tas_professors_for_all_courses(
  double_linked_list* all_courses,
  double_linked_list* all_profs,
  double_linked_list* all_tas
)
{
  // list, where each entry is a list of course_assignments for a particular
  // course
  double_linked_list* result = construct_double_linked_list_with_destr(
    sizeof(double_linked_list*),
    free_double_linked_list_wrapper
  );

  dll_map_for_all(
    all_courses,
    insert_combinations_of_profs_tas_for_course_into_list_wrapper,
    all_profs,
    all_tas,
    result
  );

  return result;
}

// -----------------------------------------------------------------------------
// Produce all possible combinations of assignments of Profs, TAs, students for
// all courses from all combinations for each course and find a combination
// with a minimal penalty
// -----------------------------------------------------------------------------

char professor_with_course_assignable_to_distribution(
  course_assignment* prof_course,
  assignment_distribution* distribution
)
{
  char result = 1;

  if (prof_course->assigned_professor)
  {
    unsigned char num_courses_assigned = 0;
    dll_node* cur_node = distribution->assigned_courses->head;

    while (cur_node)
    {
      const professor* cur_prof =
        ((course_assignment*)cur_node->value)->assigned_professor;

      if (professor_comparator(prof_course->assigned_professor, cur_prof) == 0)
      {
        if (
          dll_find_first(
            prof_course->assigned_professor->trained_courses,
            ((course_assignment*)cur_node->value)->course_obj->name
          )
        )
        {
          ++num_courses_assigned;

          if (num_courses_assigned >= PROFESSOR_MAX_ASSIGNED_TRAINED_COURSES)
          {
            result = 0;
            break;
          }
          else if (
            !dll_find_first(
              prof_course->assigned_professor->trained_courses,
              prof_course->course_obj->name
            )
          )
          {
            result = 0;
            break;
          }
        }
        else
        // professor already has an assignment for a course he is not trained
        {
          result = 0;
          break;
        }
      }

      cur_node = cur_node->right;
    }
  }

  return result;
}

char ta_assignable_to_distribution(
  teaching_assistant* ta,
  assignment_distribution* distribution
)
{
  unsigned int labs_occupied_in_distribution = 0;

  dll_map_for_all(
    distribution->assigned_courses,
    calculate_number_occupied_labs_for_ta_in_distribution_wrapper,
    ta,
    &labs_occupied_in_distribution
  );

  return (ta->labs_left >= labs_occupied_in_distribution);
}

char all_tas_assignable_to_distribution(
  double_linked_list* tas,
  assignment_distribution* distribution
)
{
  char result = 1;

  if (tas)
  {
    dll_node* cur_node = tas->head;

    while (result && cur_node)
    {
      result = ta_assignable_to_distribution(cur_node->value, distribution);

      cur_node = cur_node->right;
    }
  }

  return result;
}

char is_valid_combination(
  assignment_distribution* distribution_so_far,
  course_assignment* assignment_to_add
)
{
  return
    professor_with_course_assignable_to_distribution(
      assignment_to_add,
      distribution_so_far
    ) &&
    all_tas_assignable_to_distribution(
      assignment_to_add->assigned_tas,
      distribution_so_far
    )
  ;
}

void try_add_course_assignment_to_distribution(
  assignment_distribution* distribution,
  course_assignment* assignment,
  double_linked_list* result
)
{
  if (is_valid_combination(distribution, assignment))
  {
    assignment_distribution* distribution_copy =
      deep_copy_assignment_distribution(
        distribution
      );

    dll_append_value(
      distribution_copy->assigned_courses,
      deep_copy_course_assignment_lists(
        assignment
      )
    );

    distribution_copy->penalty -= assignment->penalty_cancellation;

    dll_append_value(
      result,
      distribution_copy
    );
  }
}

void try_add_course_assignment_to_distribution_wrapper(
  void* distribution,
  va_list assignment_result
)
{
  course_assignment* assignment = va_arg(assignment_result, course_assignment*);
  double_linked_list* result = va_arg(assignment_result, double_linked_list*);

  try_add_course_assignment_to_distribution(
    distribution,
    assignment,
    result
  );
}

void distribute_course_assignment_among_distributions(
  course_assignment* assignment,
  double_linked_list* distributions_so_far,
  double_linked_list* result,
  unsigned int total_penalty
)
{
  if (dll_is_empty(distributions_so_far))
  {
    assignment_distribution* distribution_to_add =
      construct_empty_assignment_distribution_with_penalty(
        total_penalty
      );

    // TODO: might be memory issue
    dll_append_value(
      distribution_to_add->assigned_courses,
      deep_copy_course_assignment_lists(
        assignment
      )
    );

    distribution_to_add->penalty -= assignment->penalty_cancellation;

    dll_append_value(
      result,
      distribution_to_add
    );
  }
  else
  {
    dll_map_for_all(
      distributions_so_far,
      try_add_course_assignment_to_distribution_wrapper,
      assignment,
      result
    );
  }
}

void distribute_course_assignment_among_distributions_wrapper(
  void* assignment,
  va_list distrbsSoFar_result_penalty
)
{
  double_linked_list* distributions_so_far =
    va_arg(
      distrbsSoFar_result_penalty,
      double_linked_list*
    );
  double_linked_list* result =
    va_arg(
      distrbsSoFar_result_penalty,
      double_linked_list*
    );
  unsigned int total_penalty =
    va_arg(
      distrbsSoFar_result_penalty,
      unsigned int
    );

  distribute_course_assignment_among_distributions(
    assignment,
    distributions_so_far,
    result,
    total_penalty
  );
}

void try_add_student_to_course_assignment(
  student* student_to_add,
  course_assignment* assignment,
  unsigned int* penalty
)
{
  if (
    dll_find_first(
      student_to_add->requested_courses,
      assignment->course_obj->name
    ) &&
    assignment->students_left &&
    assignment->assigned_professor
  )
  {
    dll_append_value(assignment->assigned_students, student_to_add);
    --(assignment->students_left);
    assignment->penalty_cancellation += STUDENT_LACK_COURSE_PENALTY;
    *penalty -= STUDENT_LACK_COURSE_PENALTY;
  }
}

void try_add_student_to_course_assignment_wrapper(
  void* student_to_add,
  va_list assignment_penalty
)
{
  course_assignment* assignment =
    va_arg(assignment_penalty, course_assignment*);
  unsigned int* penalty =
    va_arg(assignment_penalty, unsigned int*);

  try_add_student_to_course_assignment(student_to_add, assignment, penalty);
}

void assign_students_for_course(
  course_assignment* assignment,
  double_linked_list* all_students,
  unsigned int* penalty
)
{
  if (!(assignment->assigned_students))
  {
    assignment->assigned_students = construct_double_linked_list_with_comp(
      sizeof(student),
      student_id_comparator_wrapper
    );
  }

  dll_map_for_all(
    all_students,
    try_add_student_to_course_assignment_wrapper,
    assignment,
    penalty
  );
}

void assign_students_for_course_wrapper(
  void* assignment,
  va_list allStudents_penalty
)
{
  double_linked_list* all_students = va_arg(
    allStudents_penalty,
    double_linked_list*
  );
  unsigned int* penalty = va_arg(allStudents_penalty, unsigned int*);

  assign_students_for_course(assignment, all_students, penalty);
}

void assign_students_to_distribution(
  assignment_distribution* distribution,
  double_linked_list* all_students
)
{
  dll_map_for_all(
    distribution->assigned_courses,
    assign_students_for_course_wrapper,
    all_students,
    &(distribution->penalty)
  );
}

void assign_students_to_distribution_wrapper(
  void* distribution,
  va_list all_students_arg
)
{
  double_linked_list* all_students = va_arg(
    all_students_arg,
    double_linked_list*
  );

  assign_students_to_distribution(distribution, all_students);
}

double_linked_list* get_all_courses_professors_tas_students_distributions(
  schedule_input* input
)
{
  double_linked_list* tas_profs_courses_combinations =
    get_combinations_of_tas_professors_for_all_courses(
      input->courses,
      input->professors,
      input->tas
    );

  double_linked_list* result = construct_double_linked_list_with_destr(
    sizeof(assignment_distribution),
    free_assignment_distribution_wrapper
  );

  dll_node* cur_course_combinations_node = tas_profs_courses_combinations->head;

  while (cur_course_combinations_node)
  {
    double_linked_list* temp_list = construct_double_linked_list_with_destr(
      sizeof(assignment_distribution),
      free_assignment_distribution_wrapper
    );

    dll_map_for_all(
      cur_course_combinations_node->value,
      distribute_course_assignment_among_distributions_wrapper,
      result,
      temp_list,
      input->total_penalty
    );

    free_double_linked_list(result);
    result = temp_list;
    cur_course_combinations_node = cur_course_combinations_node->right;
  }

  dll_map_for_all(
    result,
    assign_students_to_distribution_wrapper,
    input->students
  );

  free_double_linked_list(tas_profs_courses_combinations);
  return result;
}

void assign_optimal_schedule_to_result(
  assignment_distribution* distribution,
  assignment_distribution** optimal_distribution
)
{
  if (
    (distribution->penalty < (*optimal_distribution)->penalty) ||
    dll_is_empty((*optimal_distribution)->assigned_courses)
  )
  {
    free_assignment_distribution(*optimal_distribution);
    *optimal_distribution = deep_copy_assignment_distribution(distribution);
  }
}

void assign_optimal_schedule_to_result_wrapper(
  void* distribution,
  va_list optimal_distribution
)
{
  assignment_distribution** optimal_distrib = va_arg(
    optimal_distribution,
    assignment_distribution**
  );

  assign_optimal_schedule_to_result(distribution, optimal_distrib);
}

assignment_distribution* find_first_most_optimal_schedule(schedule_input* input)
{
  assignment_distribution* result =
    construct_empty_assignment_distribution_with_penalty(
      input->total_penalty
    )
  ;

  double_linked_list* all_combinations =
    get_all_courses_professors_tas_students_distributions(
      input
    );

  dll_map_for_all(
    all_combinations,
    assign_optimal_schedule_to_result_wrapper,
    &result
  );

  free_double_linked_list(all_combinations);
  return result;
}

#endif //ASSIGNMENT_2_SCHEDULE_SCHEDULE_OPTIMIZATION_H

void print_email()
{
  FILE* email_file = fopen("TimurLyisenkoEmail.txt", "w");

  fprintf(
    email_file,
    "t.lyisenko@innopolis.university"
  );

  fclose(email_file);
}

int main()
{
  print_email();

  char consider_file = 0;

  for (unsigned char i = MAX_NUM_FILES; i > 0; --i)
  {
    char input_file_name[12];
    sprintf(input_file_name, "input%u.txt", i);
    FILE* input_file = fopen(input_file_name, "r");

    if (input_file)
    {
      consider_file = 1;
    }

    if (consider_file)
    {
      char output_file_name[12];
      sprintf(output_file_name, "output%u.txt", i);
      FILE* output_file = fopen(output_file_name, "w");

      if (input_file)
      {
        file_read_status read_status = OK;
        schedule_input* input = read_input_file(input_file, &read_status);

        if (read_status == OK)
        {
          assignment_distribution* solution =
            find_first_most_optimal_schedule(
              input
            );

          print_assignment_distribution(solution, output_file);
          print_distribution_errors(solution, input, output_file);
          fprintf(output_file, "Total score is %u.", solution->penalty);

          free_assignment_distribution(solution);
          free_schedule_input(input);
        }
        else
        {
          fprintf(output_file, "Invalid input.");
        }

        fclose(input_file);
      }
      else
      {
        fprintf(output_file, "Invalid input.");
      }

      fclose(output_file);
    }
  }

  return 0;
}
