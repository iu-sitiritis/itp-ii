#include <stdio.h>

int main()
{
  printf("Current file: %s\n", __FILE__);
  printf("Current date: %s\n", __DATE__);
  printf("Current time: %s\n", __TIME__);
  printf("Current line number: %d\n", __LINE__);

  return 0;
}
