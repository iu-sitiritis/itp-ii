#include <stdio.h>

int main()
{
  int LIMIT = 5;

  printf("%d\n", LIMIT);

  #define LIMIT 1000
  printf("%d\n", LIMIT);
  #undef LIMIT

  printf("%d\n", LIMIT);

  return 0;
}
