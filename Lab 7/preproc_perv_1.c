#include <stdio.h>

#define printValue(value) printf(#value"=%d\n", value)
#define paste(l, r) l##r

void printValueFunc(int value)
{
  printf("%d\n", value);
}

int main()
{
  int paste(a, b) = 3;

  printValueFunc(ab);
  printValueFunc(ab);

  printValueFunc(paste(a, b));
  printValue(paste(a, b));

  printValueFunc(ab = 5);
  printValue(ab = 5);

  // int paste(paste(a, b), a) = 5; // () - are not allowed in an identifier's name
}

