package edu.innopolis;

import java.util.List;

public class Student extends UniversityEntity
{
  public Student(String name, List<Course> trainedCourses)
  {
    super(name, trainedCourses);
  }
}
