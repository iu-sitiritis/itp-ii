package edu.innopolis;

import java.util.*;

public class Course
{
  public Course(String name, int numLabs, int maxNumStudents)
  {
    this.name = name;
    this.requiredNumLabs = numLabs;
    this.maxNumStudents = maxNumStudents;
    professor = null;
    teachingAssistantLabs = new HashMap<>();
    students = new LinkedList<>();
  }

  boolean hasProfessor()
  {
    return professor != null;
  }

  boolean enoughTAs()
  {
    int labsOccupied = 0;

    for (Integer labPerTA : teachingAssistantLabs.values())
    {
      labsOccupied += labPerTA;
    }

    return labsOccupied == requiredNumLabs;
  }

  boolean isRunnable()
  {
    return hasProfessor() && enoughTAs();
  }

  public final String name;
  public final int requiredNumLabs;
  public final int maxNumStudents;
  public Professor professor;
  public Map<TeachingAssistant, Integer> teachingAssistantLabs;
  public List<Student> students;
}
