package edu.innopolis;

import java.util.*;

public final class UniversityScheduler
{
  private UniversityScheduler() {}

  public static int optimizeSchedule(
    List<Course> courses,
    List<Professor> professors,
    List<TeachingAssistant> teachingAssistants //,
//    Set<Student> students
  )
  {
    int penalty = calculateMaximalPenalty(
      courses,
      professors,
      teachingAssistants //,
//      students
    );

    Collections.sort(professors);
    Collections.sort(teachingAssistants);
    List<Course> availableCourses = new ArrayList<>(courses);
    List<Professor> availableProfessors = new ArrayList<>(professors);
    List<TeachingAssistant> availableTAs = new ArrayList<>(teachingAssistants);

    for (Professor professor : professors)
    {
      Iterator<Course> trainedCoursesIter = professor.trainedCourses.iterator();

      while (
        trainedCoursesIter.hasNext() &&
        professor.assignedCourses.size() < 2
      )
      {
        Course curCourse = trainedCoursesIter.next();

        if (availableCourses.contains(curCourse))
        // is course available for the assignment?
        {
          Map<TeachingAssistant, Integer> optimalTAs = findOptimalTAsForCourse(availableTAs, curCourse);
          int numOccupiedLabs = 0;

          for (Integer numLabsForTA : optimalTAs.values())
          {
            numOccupiedLabs += numLabsForTA;
          }

          if (numOccupiedLabs == curCourse.requiredNumLabs)
          // are there enough TAs for the course?
          {
          // if there are - make the course running
            curCourse.professor = professor;
            curCourse.teachingAssistantLabs = optimalTAs;
            penalty -= 20;

            professor.assignedCourses.add(curCourse);
            availableProfessors.remove(professor);
            penalty -= 5;

            for (Map.Entry<TeachingAssistant, Integer> ta : optimalTAs.entrySet())
            {
              ta.getKey().addToLab(curCourse, ta.getValue());
              if (ta.getKey().availableLabs == 0)
              {
                availableTAs.remove(ta.getKey());
              }

              penalty -= 2 * ta.getValue();
            }
          }

          // regardless of whether course is running or not - delete it form the list of available courses, since:
          // if course is already running there is no sense in checking it for other professors;
          // if course cannot be run - it cannot be run for other professors as well, since there are not enough TAs
          availableCourses.remove(curCourse);
        }
      }
    }

    // Deal with the rest of the courses (those none of the professors are trained for)
    Iterator<Course> availableCoursesIter = availableCourses.iterator();
    Iterator<Professor> availableProfIter = availableProfessors.iterator();

    while (
      availableCoursesIter.hasNext() &&
      availableProfIter.hasNext()
    )
    {
      Course curCourse = availableCoursesIter.next();

      Map<TeachingAssistant, Integer> optimalTAs = findOptimalTAsForCourse(availableTAs, curCourse);
      int numOccupiedLabs = 0;

      for (Integer numLabsForTA : optimalTAs.values())
      {
        numOccupiedLabs += numLabsForTA;
      }

      if (numOccupiedLabs == curCourse.requiredNumLabs)
      // are there enough TAs for the course?
      {
        // run the course
        Professor curProfessor = availableProfIter.next();

        curCourse.professor = curProfessor;
        curCourse.teachingAssistantLabs = optimalTAs;
        penalty -= 20;

        curProfessor .assignedCourses.add(curCourse);
//        availableProfessors.remove(curProfessor);
        penalty -= 5;

        for (Map.Entry<TeachingAssistant, Integer> ta : optimalTAs.entrySet())
        {
          ta.getKey().addToLab(curCourse, ta.getValue());
          if (ta.getKey().availableLabs == 0)
          {
            availableTAs.remove(ta.getKey());
          }

          penalty -= 2 * ta.getValue();
        }
      }
    }

    return penalty;
  }

  public static Map<TeachingAssistant, Integer> findOptimalTAsForCourse(
    List<TeachingAssistant> teachingAssistants,
    Course course
  )
  {
    Map<TeachingAssistant, Integer> optimalTeachingAssistants = new HashMap<>();
    int labsFilled = 0;

    Iterator<TeachingAssistant> teachingAssistantIterator = teachingAssistants.iterator();
    while (
      teachingAssistantIterator.hasNext() &&
      course.requiredNumLabs > labsFilled
    )
    {
      TeachingAssistant curTA = teachingAssistantIterator.next();

      if (curTA.trainedCourses.contains(course))
      {
        int numOccupiedLabs = Math.min(curTA.availableLabs, course.requiredNumLabs);
        optimalTeachingAssistants.put(curTA, numOccupiedLabs);
        labsFilled += numOccupiedLabs;
      }
    }

    return optimalTeachingAssistants;
  }

  public static int calculateMaximalPenalty(
    final List<Course> courses,
    final List<Professor> professors,
    final List<TeachingAssistant> teachingAssistants //,
//    final Set<Student> students
  )
  {
    return (20 * courses.size()) + (10 * professors.size()) + (2 * 4 * teachingAssistants.size());
  }
}
