package edu.innopolis;

import java.util.LinkedList;
import java.util.List;

public abstract class UniversityEntity
{
  public UniversityEntity(String name, List<Course> trainedCourses)
  {
    this.name = name;
    this.trainedCourses = trainedCourses;
    this.assignedCourses = new LinkedList<>();
  }

  public final String name;
  public final List<Course> trainedCourses;
  public List<Course> assignedCourses;
}
