package edu.innopolis;

import java.util.List;

public class Professor extends UniversityEntity implements Comparable<Professor>
{
  public Professor(String name, List<Course> trainedCourses)
  {
    super(name, trainedCourses);
  }

  @Override
  public int compareTo(Professor professor)
  {
    if (trainedCourses.size() < 2)
    {
      return 1;
    }
    else if (professor.trainedCourses.size() < 2)
    {
      return -1;
    }
    else
    {
      return Integer.compare(trainedCourses.size(), professor.trainedCourses.size());
    }
  }
}
