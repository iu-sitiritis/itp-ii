package edu.innopolis;

import java.util.*;

public class Main
{
  public static void main(String[] args)
  {
    Course math = new Course("Math", 4, 100);
    Course programming = new Course("Programming", 4, 100);
    Course philosophy = new Course("Philosophy", 4, 100);

    Professor brown = new Professor(
      "Joseph Brown",
      Arrays.asList(math, programming)
    );
    Professor succi = new Professor(
      "Giancarlo Succi",
      Arrays.asList(programming)
    );

    TeachingAssistant munir = new TeachingAssistant(
      "Munir",
      Arrays.asList(programming)
    );
    TeachingAssistant shilov = new TeachingAssistant(
      "Shilov",
      Arrays.asList(philosophy, math)
    );
    TeachingAssistant konukhov = new TeachingAssistant(
      "Konukhov",
      Arrays.asList(math)
    );

    List<Course> courses = Arrays.asList(math, programming, philosophy);
    List<Professor> professors = Arrays.asList(brown, succi);
    List<TeachingAssistant> teachingAssistants = Arrays.asList(munir, shilov, konukhov);

    int totalPenalty = UniversityScheduler.optimizeSchedule(
      courses,
      professors,
      teachingAssistants
    );

    System.out.println(totalPenalty);
  }
}
