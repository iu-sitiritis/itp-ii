package edu.innopolis;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TeachingAssistant extends UniversityEntity implements Comparable<TeachingAssistant>
{
  public TeachingAssistant(String name, List<Course> trainedCourses)
  {
    super(name, trainedCourses);
    availableLabs = 4;
    courseLabs = new HashMap<>();
  }

  void addToLab(Course course, int numLabs)
  {
    int resultNumLabs = availableLabs - numLabs;

    if (resultNumLabs < 0)
    {
      throw new IllegalArgumentException();
    }

    courseLabs.put(course, numLabs);
    availableLabs = resultNumLabs;
  }

  @Override
  public int compareTo(TeachingAssistant teachingAssistant)
  {
    return Integer.compare(trainedCourses.size(), teachingAssistant.trainedCourses.size());
  }

  public Map<Course, Integer> courseLabs;
  public int availableLabs;
}
