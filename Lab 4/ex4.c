#include <stdio.h>
#include <stdlib.h>

#define MAX_NUM_INGREDIENTS 10
#define NUM_RECIPES 3

struct ingredient_record
{
  char* name;
  unsigned int quantity;
};

struct recipe
{
  char* name;
  struct ingredient_record ingredients[MAX_NUM_INGREDIENTS];
  unsigned int num_ingredients;
};

struct cookbook
{
  struct recipe* recipes;
  unsigned int num_recipes;
};

void print_cookbook(struct cookbook* cb)
{
  printf("Cookbook:\n");

  for (int i = 0; i < cb->num_recipes; ++i)
  {
    printf("\t%s:\n", cb->recipes[i].name);

    for (int j = 0; j < cb->recipes[i].num_ingredients; ++j)
    {
      printf("\t\t%s - %u\n", cb->recipes[i].ingredients[j].name, cb->recipes[i].ingredients[j].quantity);
    }
  }
}

int main()
{
  struct cookbook cb = {
    calloc(NUM_RECIPES, sizeof(struct recipe)),
    NUM_RECIPES
  };
  
  cb.recipes[0].name = "Scrambled eggs";
  cb.recipes[0].ingredients[0].name = "Eggs";
  cb.recipes[0].ingredients[0].quantity = 3;
  cb.recipes[0].ingredients[1].name = "Bacon";
  cb.recipes[0].ingredients[1].quantity = 2;
  cb.recipes[0].num_ingredients = 2;

  cb.recipes[1].name = "Shaurma";
  cb.recipes[1].ingredients[0].name = "Pita";
  cb.recipes[1].ingredients[0].quantity = 1;
  cb.recipes[1].ingredients[1].name = "Chicken";
  cb.recipes[1].ingredients[1].quantity = 300;
  cb.recipes[1].ingredients[2].name = "Salad";
  cb.recipes[1].ingredients[2].quantity = 2;
  cb.recipes[1].ingredients[3].name = "Cheese";
  cb.recipes[1].ingredients[3].quantity = 50;
  cb.recipes[1].ingredients[4].name = "Mashrooms";
  cb.recipes[1].ingredients[4].quantity = 4;
  cb.recipes[1].ingredients[5].name = "Сucumber";
  cb.recipes[1].ingredients[5].quantity = 8;
  cb.recipes[1].ingredients[6].name = "Tomato";
  cb.recipes[1].ingredients[6].quantity = 6;
  cb.recipes[1].ingredients[7].name = "Sauce";
  cb.recipes[1].ingredients[7].quantity = 100;
  cb.recipes[1].num_ingredients = 8;

  cb.recipes[2].name = "Echpochmak";
  cb.recipes[2].ingredients[0].name = "Dough";
  cb.recipes[2].ingredients[0].quantity = 100;
  cb.recipes[2].ingredients[1].name = "Chicken";
  cb.recipes[2].ingredients[1].quantity = 50;
  cb.recipes[2].num_ingredients = 2;

  print_cookbook(&cb);

  return 0;
}
