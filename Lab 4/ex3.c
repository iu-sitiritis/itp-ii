#include <stdio.h>

struct bday
{
  unsigned short day : 3;
  unsigned short month : 3;
  unsigned short year : 8;
};

int main()
{
  struct bday birth_day = {5, 7, 255};

  printf("Day: %u\nMonth: %u\nYear: %u\n",
    birth_day.day, birth_day.month , birth_day.year + 1744
  );
  printf("struct size = %u\n", sizeof(birth_day));

  return 0;
}
