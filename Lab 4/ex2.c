#include <stdio.h>

struct message
{
  enum msg_state
  {
    DEC = 0,
    ENC = 1
  } state : 1;

  union mes
  {
    unsigned long long int decoded;
    unsigned long long int encoded;
  } message;
};

void encode(struct message* msg)
{
  unsigned long long int result;
  unsigned int ulli_size = sizeof(unsigned long long int);

  for (unsigned int i = 0; i < ulli_size; i += 2)
  {
    ((char *)(&result))[i] = ((char *)(&(msg->message)))[i + 1];
    ((char *)(&result))[i + 1] = ((char *)(&(msg->message)))[i];
  }

  msg->message.encoded = result;
  msg->state = ENC;
}

unsigned long long int getMessage(const struct message* msg)
{
  return (msg->state) ? msg->message.encoded : msg->message.decoded;
}

int main()
{
  struct message msg;
  msg.state = DEC;

  printf("Enter a number:\n");
  scanf("%llu", &(msg.message));

  printf("%llx\n", getMessage(&msg));
  encode(&msg);
  printf("%llx\n", getMessage(&msg));

  return 0;
}
