#include <stdio.h>
#include <stdlib.h>

#define NAME_SIZE 51
#define MONTH_SIZE 16

struct student
{
  char name[NAME_SIZE];
  char surname[NAME_SIZE];
  unsigned int groupNO;
  struct exam_day* exam;
};

struct exam_day
{
  unsigned int day,
               year;
  char month[MONTH_SIZE];
};

void write_to_file(const struct student* stud)
{
  FILE* data_file = fopen("./data/students.txt", "a+");

  fputs("Name: ", data_file);
  fputs(stud->name, data_file);
  putc('\n', data_file);

  fputs("Surname: ", data_file);
  fputs(stud->surname, data_file);
  putc('\n', data_file);

  fputs("GroupNO: ", data_file);
  fprintf(data_file, "%u", stud->groupNO);
  putc('\n', data_file);

  fputs("\tExam:\n", data_file);

  fputs("\t\tExam day: ", data_file);
  fprintf(data_file, "%u", stud->exam->day);
  putc('\n', data_file);

  fputs("\t\tExam month: ", data_file);
  fprintf(data_file, "%s", stud->exam->month);
  putc('\n', data_file);

  fputs("\t\tExam year: ", data_file);
  fprintf(data_file, "%u\n", stud->exam->year);

  fclose(data_file);
}

int main()
{
  struct student stud;
  struct exam_day* exam = (struct exam_day*) malloc(sizeof(struct exam_day));
  stud.exam = exam;
  char c = EOF;

  printf("Enter student's name:\n");

  int i = 0;
  while (((c = getchar()) != EOF) && (c != '\n') && (i < (NAME_SIZE - 1)))
  {
    stud.name[i++] = c;
  }
  stud.name[i] = '\0';

  if (c == EOF)
  {
    putchar('\n');
  }


  printf("Enter student's surname:\n");

  i = 0;
  while (((c = getchar()) != EOF) && (c != '\n') && (i < (NAME_SIZE - 1)))
  {
    stud.surname[i++] = c;
  }
  stud.surname[i] = '\0';

  if (c == EOF)
  {
    putchar('\n');
  }


  printf("Enter the student's group #:\n");
  scanf("%u", &(stud.groupNO));


  printf("Enter exam day:\n");
  scanf("%u", &(exam->day));


  printf("Enter exam month:\n");
  scanf("%s", &(exam->month));


  printf("Enter exam year:\n");
  scanf("%d", &(exam->year));

  write_to_file(&stud);

  return 0;
}
