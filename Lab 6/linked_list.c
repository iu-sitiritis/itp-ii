#include <stdlib.h>
#include <string.h>

typedef 
  struct cell
cell;

struct cell
{
  unsigned int num_occur;

  char* str;
  cell *next;
};

typedef
  struct linked_list
  {
    unsigned int size;

    cell* head;
  }
linked_list;

cell* construct_cell(char* str)
{
  cell* result = malloc(sizeof(cell));
  result->num_occur = 1;

  return result;
}

// frees cell and memory of allocated to its string
void free_cell(cell* cl)
{
  free(cl->str);
  free(cl);
}

// Construct a new linked_list and initialize it with zeros
linked_list* construct_linked_list()
{
  linked_list* result = calloc(1, sizeof(linked_list));

  return result;
}

void free_list(linked_list* ll)
{
  cell* iter = ll->head;

  while(iter)
  {
    cell* next = iter->next;
    free_cell(iter);
    iter = next;
  }
  
  free(ll);
}

// Adds a given cell at the beginning of a linked list, assigns its next, the previous head
// and increases a size of the list
// Returns an inserted cell
const cell* add_first(linked_list* ll, cell* cell_to_insert)
{
  ++(ll->size);
  cell_to_insert->next = ll->head;

  return ll->head = cell_to_insert;
}

// Returns a cell with result->str == str. If no such cell is found - the result is null
const cell* get_cell_by_str(const linked_list* ll, const char* str)
{
  cell* iter = ll->head;

  while (
    iter &&
    strcmp(iter->str, str)
  )
  {
    iter = iter->next;
  }

  return (const cell*) iter;
}


