#include <stdio.h>

int main(int argc, char* argv[])
{
  unsigned int page_count = 1;

  for (unsigned int i = 1; i < argc; ++i)
  {
    FILE* f = fopen(argv[i], "r");

    if (f)
    {
      for (unsigned int l = 0; !feof(f); ++l)
      {
        if (l % 9 == 0)
        {
          if (page_count > 1)
          {
            putchar('\f');
          }

          printf("Filename: %s\nPage: %d\n", argv[i], page_count);
          ++page_count;
        }

        char c = EOF;
        while ((c = fgetc(f)) != '\n' && c != EOF)
        {
          putchar(c);
        }

        if (c == '\n')
        {
          putchar(c);
        }
      }
    }
    else
    {
      if (page_count > 1)
      {
        putchar('\f');
      }

      printf("File %s not exists\n", argv[i]);
    }
  }
}
