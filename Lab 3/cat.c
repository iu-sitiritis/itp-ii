#include <stdio.h>

int main()
{
  FILE* f = fopen("./data/data_file.txt", "r");

  if (f)
  {
    while (!feof(f))
    {
      putchar(fgetc(f));
    }

    fclose(f);
  }
  else
  {
    printf("File not exists\n");
  }

  return 0;
}
