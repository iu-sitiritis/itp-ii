#include <stdio.h>

int main()
{
  FILE* left = fopen("./data/left.txt", "r");
  FILE* right = fopen("./data/right.txt", "r");

  if (!left)
  {
    printf("./data/left.txt does not exist");
  }

  if (!right)
  {
    printf("./data/right.txt does not exist");
  }

  if (left && right)
  {
    char cl = EOF, cr = EOF; // left and right characters
    int num_same_chars = 0, num_chars_cur_line = 0;
    int line_num = 1;

    while (((cl = getc(left)) == (cr = getc(right))) && (cl != EOF))
    {
      ++num_same_chars;

      if (cl == '\n')
      {
        num_chars_cur_line = 0;
        ++line_num;
      }
      else
      {
       ++num_chars_cur_line;
      }
    }

    if (cl == EOF && cr == EOF)
    {
      printf("Files are the same\n");
    }
    else
    {
      printf("Line #%d\n", line_num);

      printf("Left file:\n");
      fseek(left,  num_same_chars - num_chars_cur_line, SEEK_SET);

      char c = EOF;
      while (((c = getc(left)) != '\n') && c != EOF)
      {
        putchar(c);
      }

      printf("\n\nRight file:\n");
      fseek(right, num_same_chars - num_chars_cur_line, SEEK_SET);

      while (((c = getc(right)) != '\n') && c != EOF)
      {
        putchar(c);
      }
      putchar('\n');
    }
    
  }

  return 0;
}
