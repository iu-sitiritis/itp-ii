#include <stdio.h>
#include <ctype.h>
#include <stdlib.h>
#include <string.h>

#define LEN(a) sizeof(a)/sizeof(typeof(a[0]))

char* remove_nonalphanum_tolower(char str[], unsigned int size)
{
  char* res_str = malloc(size);

  unsigned int i, j;
  i = j = 0;

  while (str[i] != '\0')
  {
    if(isalpha(str[i]) || isdigit(str[i]))
      res_str[j++] = tolower(str[i]);

    ++i;
  }

  res_str[j] = '\0';

  return res_str;
}

int is_palindrome(char str[], unsigned int size)
{
  char* processed_str = remove_nonalphanum_tolower(str, size);

  int i = 0, j = strlen(processed_str) - 1;
  while (i <= j && processed_str[i++] == processed_str[j--]);

  return (j - i <= 0);
}

int main()
{
  char str[] = "sitiritis";
  printf("%s\n", remove_nonalphanum_tolower(str, LEN(str)));
  printf("Is palindrome = %d\n", is_palindrome(str, LEN(str)));
}
