#include <stdio.h>

void addition(int x, int y);

int multiplication(int x, int y);

void charCodeSum(char x, char y);

int main()
{
  int x, y;
  x = 3;
  y = 5;

  char a, b;
  a = 'a';
  b = 'b';

  addition(x, y);
  printf("%d + %d = %d\n", x, y, multiplication(x, y));
  charCodeSum(a, b);
}

void addition(int x, int y)
{
  printf("%d + %d = %d\n", x, y, x + y);
}

int multiplication(int x, int y)
{
  return x * y;
}

void charCodeSum(char x, char y)
{
  printf("%c + %c = %d\n", x, y, x + y);
}
