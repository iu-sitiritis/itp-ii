#include <stdio.h>

int main()
{
  int a = 255;

  printf("d = %5i\n", a);
  printf("h = %5x\n", a);
  printf("o = %05o\n", a);
}
