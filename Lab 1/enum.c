#include <stdio.h>

enum season
{
  WINTER = 0,
  SPRING = 1,
  SUMMER = 2,
  AUTUMN = 3
};

void printMessage(enum season s)
{
  switch (s)
  {
    case WINTER: printf("Brrr, it's cold...\n"); break;
    case SPRING: printf("The nature is so beautiful...\n"); break;
    case SUMMER: printf("I'm dying from the heat...\n"); break;
    case AUTUMN: printf("Everything is orange...\n"); break;
  }
}

int main()
{
  enum season s = WINTER;

  printMessage(s);
}
