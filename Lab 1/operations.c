#include <stdio.h>

int main()
{
  int x = 10, y = 5;

  printf("int:\n");
  printf("x + y = %d\n", x + y);
  printf("x - y = %d\n", x - y);
  printf("x * y = %d\n", x * y);
  printf("x / y = %d\n", x / y);
  printf("x %% y = %d\n", x % y);

  float xf = 10.3, yf = 5.6;

  printf("\nfloat:\n");
  printf("xf + yf = %f\n", xf + yf);
  printf("xf - yf = %f\n", xf - yf);
  printf("xf * yf = %f\n", xf * yf);
  printf("xf / yf = %f\n", xf / yf);
  // printf("xf %% yf = %d\n", xf % yf); // no % for float

  printf("\n(int)float:\n");
  printf("xf + yf = %d\n", (int)(xf + yf));
  printf("xf - yf = %d\n", (int)(xf - yf));
  printf("xf * yf = %d\n", (int)(xf * yf));
  printf("xf / yf = %d\n", (int)(xf / yf));

  long long int xl = -2147483648,
                yl = -2147483649;

  printf("\nlong long int:\n");
  printf("xl + yl = %lld\n", (xl + yl));
  printf("xl - yl = %lld\n", (xl - yl));

  printf("\n(int)(long long int):\n");
  printf("xl + yl = %lld\n", (int)(xl + yl));
  printf("xl - yl = %lld\n", (int)(xl - yl));
}
